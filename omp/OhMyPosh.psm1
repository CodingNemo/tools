$initializationScript = "oh-my-posh --init --shell pwsh --config '$PSScriptRoot/theme.omp.json' | Invoke-Expression"
$additionalModules = 'if ($host.Name -eq "ConsoleHost") {
        Import-Module PSReadLine

        Set-PSReadLineOption -PredictionSource History
        Set-PSReadLineOption -PredictionViewStyle ListView
        Set-PSReadLineOption -EditMode Windows

        Import-Module -Name Terminal-Icons
    }'

function Add-OhMyPoshInitToProfile {
    $profileContent = Get-Content $PROFILE -Raw

    if ($profileContent.Contains("# Init OhMyPosh")) {
        Write-Info "Profile already contains OhMyPosh initialization"
    }
    else {
        if(-not(Test-Path $PROFILE)) {
            New-Item -Type File $PROFILE
        }

        Add-Content $PROFILE "# Init OhMyPosh"
        Add-Content $PROFILE $initializationScript
        Add-Content $PROFILE $additionalModules
        Add-Content $PROFILE "# End OhMyPosh init"
        Add-Content $PROFILE ""
        Write-Success "Profile updated"
        Write-Host ""
        Write-Host ""
    }
}

function Test-OhMyPosh {
    $ohMyPosh = Get-Command oh-my-posh -ErrorAction SilentlyContinue
    return $null -ne $ohMyPosh
}


function Get-CaskaydiaCoveNerdFont {
    Write-Pending "Downloading Nerd Font"
    $zipPath = "$env:TEMP\CascadiaCode.zip"
    $zipUrl = "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/CascadiaCode.zip"
    Invoke-WebRequest -Uri $zipUrl -OutFile $zipPath   
    Write-Success "Nerd Font downloaded here : $zipPath (Install it manually)"
}

function Install-TerminalIcons {
    Install-Module -Name Terminal-Icons
}
function Install-PSReadLine {
    Install-Module -Name PSReadLine -AllowClobber -Force
}

function Install-OhMyPosh {
    if (-not(Test-OhMyPosh)) {
        Write-Pending "Installing OhMyPosh"
        winget install JanDeDobbeleer.OhMyPosh -s winget
        Write-Success "OhMyPosh installed"
    }
    else {
        Write-Info "OhMyPosh already installed"
    }

    Write-Pending "Setuping OhMyPosh"
    
    Install-TerminalIcons
    Install-PSReadLine
    Add-OhMyPoshInitToProfile
    Get-CaskaydiaCoveNerdFont

    Write-Success "OhMyPosh ready"
}

Export-ModuleMember -Function Install-OhMyPosh