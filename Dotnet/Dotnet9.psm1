function Test-Dotnet9 {
    $dotnet = Get-Command dotnet -ErrorAction SilentlyContinue

    if ($null -ne $dotnet) {
        $version = dotnet --version
        if ($version.StartsWith("9")) {
            return $true
        }
    }

    return $null -ne $dotnet
}

function Install-Dotnet9 {
    if (-not(Test-Dotnet9)) {
        Write-Pending "Installing dotnet 9"
        winget install --id Microsoft.DotNet.SDK.9 -e
        Write-Success "Dotnet 9 installed"
    }
    else {
        Write-Info "Dotnet 9 already installed"
    }
}

Export-ModuleMember -Function Install-Dotnet9