
function Test-WindowsTerminal {
    if (Get-Command winget -ErrorAction SilentlyContinue) {
        return $true
    }
    return $false
}


function Install-WindowsTerminal {
    if (Test-WindowsTerminal) {
        Write-Info "Windows Terminal already installed"
        return
    }

    Write-Pending "Installing Windows Terminal"
    winget install --id Microsoft.WindowsTerminal -e
    Write-Success "Windows Terminal installed"
}

Export-ModuleMember -Function Install-WindowsTerminal