param (
    [string]$TargetVersion = '8'
)

function Confirm {
    param (
        [string]$Message
    )

    while ($true) {
        $userInput = Read-Host "$Message (y/n)"
        $userInput = $userInput.ToLower()
        if ($userInput -eq 'y') {
            return $true
        }
        if ($userInput -eq 'n') {
            return $false
        }
    }
    return $false
}

function Parse-Version {
    param (
        [Parameter(Position = 0)]
        [string]$Version
    )

    if ($Version -match "^(\d+)$") {
        return @{ Major = [int]$matches[1]; }
    }

    if ($Version -match "^(\d+)\.(\d+)$") {
        return @{ Major = [int]$matches[1]; Minor = [int]$matches[2] }
    }
    elseif ($Version -match "^(\d+)\.(\d+)\.(\d+)$") {
        return @{ Major = [int]$matches[1]; Minor = [int]$matches[2]; Patch = [int]$matches[3] }
    }
    else {
        return $null
    }
}

function Display-Version {
    param (
        [Parameter(ValueFromPipeline = $true)][hashtable]$Version
    )

    $versionString = "$($Version.Major)"
    if ($Version.ContainsKey('Minor')) {
        $versionString = "$versionString.$($Version.Minor)"
    }

    if ($Version.ContainsKey('Patch')) {
        $versionString = "$versionString.$($Version.Patch)"
    }
    return $versionString
}

function Contains-Version {
    param (
        [Parameter(ValueFromPipeline = $true)]$Versions,
        [hashtable]$Version
    )

    foreach ($v in $Versions) {
        if ($v.Major -eq $Version.Major -and $v.Minor -eq $Version.Minor -and $v.Patch -eq $Version.Patch) {
            return $true
        }
    }

    return $false
}

function Get-SdkVersionToInstall {
    param (
        [hashtable]$Reference
    )

    $InstalledSdkVersions = Get-InstalledSdkVersions -Reference $Reference

    if ($Reference.ContainsKey('Patch')) {
        if ($InstalledSdkVersions | Contains-Version -Version $Reference) {
            Write-Host ".NET SDK $($Reference | Display-Version) is already installed."
            return $null    
        }
        else {
            $LatestSdkVersion = Get-LatestSdkVersion -Reference $Version
            if ($LatestSdkVersion | Is-Greater-Than $Reference) {
                Write-Host ".NET SDK $($Reference | Display-Version) is prior to the latest sdk version $($LatestSdkVersion | Display-Version)."
                if (Confirm "Would you like to install the latest sdk version now?") {
                    return $LatestSdkVersion
                }
                else {
                    return $Reference
                }
            }
        }
    }
    else {
        $LatestSdkVersion = Get-LatestSdkVersion -Reference $Reference
        if ($null -eq $LatestSdkVersion) {
            Write-Host "No .NET SDK Version found for $($Reference | Display-Version)"
            return $null
        }
        else {
            if ($InstalledSdkVersions -and $InstalledSdkVersions | Contains-Version -Version $LatestSdkVersion) {
                Write-Host ".NET SDK $($Reference | Display-Version) with the latest sdk version $($LatestSdkVersion | Display-Version) is already installed."
                return $null
            }
            else {
                Write-Host "Latest available sdk version for .NET SDK $($Reference | Display-Version): $($LatestSdkVersion | Display-Version) is not installed"
                if ($InstalledSdkVersions) {
                    Write-Host "Currently installed versions:"
                    foreach ($installedSdkVersion in $InstalledSdkVersions) {
                        Write-Host "- $($installedSdkVersion | Display-Version)"
                    }
                }
                else {
                    Write-Host "No versions are installed for $($Reference | Display-Version)"
                }
                return $LatestSdkVersion
            }
            return $LatestSdkVersion
        }
    }
}

function Ensure-DotnetSdk {
    param (
        [string]$Version
    )

    $referenceVersion = Parse-Version -Version $Version
    if ($null -eq $referenceVersion) {
        Write-Host "Invalid version format. Please use a format like '8', '8.0' or '8.0.404'."
        return
    }

    $sdkVersionsToInstall = Get-SdkVersionToInstall -Reference $referenceVersion

    if ($null -eq $sdkVersionsToInstall) {
        return
    }

    if (Confirm "Would you like to install .NET SDK version $($sdkVersionsToInstall | Display-Version)") {
        Install-DotnetSdk -Version $sdkVersionsToInstall
    }
    else {
        Write-Host "Installation aborted by user."
    }
}

function Match-Reference {
    param (
        [Parameter(ValueFromPipeline = $true)][hashtable]$Version,
        [Parameter(Position = 0)]
        [hashtable]$Reference
    )

    if ($Reference.Major -ne $Version.Major) {
        return $false
    }

    if ($Reference.ContainsKey("Minor") -and $Reference.Minor -ne $Version.Minor) {
        return $false
    }

    if ($Reference.ContainsKey('Patch') -and $Reference.Patch -ne $Version.Patch) {
        return $false
    }
    
    return $true
}

function Is-Greater-Than {
    param (
        [Parameter(ValueFromPipeline = $true)][hashtable]$Version,
        [Parameter(Position = 0)]
        [hashtable]$Reference
    )

    if ($Version.Major -gt $Reference.Major) {
        return $true
    }

    if ($Version.Major -eq $Reference.Major -and $Version.Minor -gt $Reference.Minor) {
        return $true
    }

    if ($Version.Major -eq $Reference.Major -and $Version.Minor -eq $Reference.Minor -and $Version.Patch -gt $Reference.Patch) {
        return $true
    }

    return $false
}

function Get-InstalledSdkVersions {
    param(
        [hashtable]$Reference
    )

    $dotnetInfo = &dotnet --list-sdks 2>$null
    $installedSdkVersions = @()
    foreach ($sdk in $dotnetInfo) {
        
        $split = $sdk -split " \["
        $rawVersion = $split[0]

        $parsedSdkVersion = Parse-Version $rawVersion
        
        if ($parsedSdkVersion | Match-Reference $Reference) {
            $installedSdkVersions += $parsedSdkVersion            
        }
    }
    return $installedSdkVersions
}

function Get-LatestSdkVersion {
    param (
        [hashtable]$Reference
    )

    $url = "https://dotnetcli.blob.core.windows.net/dotnet/release-metadata/releases-index.json"
    $releaseIndex = Invoke-RestMethod -Uri $url -UseBasicParsing

    $latestSdkVersion = $null

    foreach ($release in $releaseIndex.'releases-index') {
        $latestSdkVersionPerRelease = Parse-Version $release.'latest-sdk'

        if ($latestSdkVersionPerRelease | Match-Reference $Reference) {
            if ($null -eq $latestSdkVersion) {
                $latestSdkVersion = $latestSdkVersionPerRelease
            }
            else {
                if ($latestSdkVersionPerRelease | Is-Greater-Than $latestSdkVersion) {
                    $latestSdkVersion = $latestSdkVersionPerRelease
                }
            }
        }
    }
    
    return $latestSdkVersion
}

function Install-DotnetSdk {
    param (
        [hashtable]$Version
    )

    $VersionString = $Version | Display-Version
    
    Write-Host "Installing .NET SDK version $VersionString..."
    
    # Download and execute the dotnet-install script
    $installerUrl = "https://dotnet.microsoft.com/fr-fr/download/dotnet/thank-you/sdk-$VersionString-windows-x64-installer"
    $installerPath = "dotnet-sdk-$VersionString-win-x64.exe"

    Invoke-WebRequest -Uri $installerUrl -OutFile $installerPath
    Start-Process -FilePath $installerPath -ArgumentList "/quiet /install" -Wait

    # Remove the installation script after use
    Remove-Item $installerPath
}

Ensure-DotnetSdk -Version $TargetVersion
