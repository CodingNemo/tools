﻿module Console

open Fake.IO
open Spectre.Console


let writeBlankLine () =
    AnsiConsole.WriteLine("")

let writeTitle title =
    AnsiConsole.Write(FigletText(title).Centered())
    writeBlankLine()

let writeWarning message =
    AnsiConsole.MarkupLine $" :warning: [yellow]{message}.[/]"

let writeSuccess message =
    AnsiConsole.MarkupLine $" :check_mark_button: [green]{message}.[/]"

let writePending message =
    AnsiConsole.MarkupLine $" :counterclockwise_arrows_button: [cyan]{message}...[/]"

let writeError message =
    AnsiConsole.MarkupLine $" :cross_mark: [red]{message}.[/]"

let writeException (exception':exn) =
    AnsiConsole.WriteException(exception',ExceptionFormats.Default)

let writeInfo message =
    AnsiConsole.MarkupLine $" :notebook: {message}."

let confirm (question:string) =
    let prompt = ConfirmationPrompt($" :white_question_mark: {question}")
    AnsiConsole.Prompt(prompt)

let promptUser question =
    writeBlankLine()
    let prompt = TextPrompt<string>($" :writing_hand: {question}")
    AnsiConsole.Prompt(prompt)

let promptUserOrDefault question defaultValue =
    writeBlankLine()
    let prompt = TextPrompt<string>($" :writing_hand: {question}")
                    .AllowEmpty()
                    .DefaultValue(defaultValue)
                    .ShowDefaultValue()
    AnsiConsole.Prompt(prompt)

let rec promptUserForExistingPath title =
    writeBlankLine()
    let path = promptUser $"Please provide the path to {title}"
        
    if System.IO.Directory.Exists path then
        path
    else
        writeWarning $"Path '{path}' does not exist"
        if confirm "Do you want to create it ?" then
            Directory.ensure path
            path
        else
            writeWarning "Try again"
            promptUserForExistingPath title

let readKey() =
    System.Console.ReadKey() |> ignore
    
let choice<'a> (title:string) (choices: 'a list) =
    writeBlankLine()
    let prompt = SelectionPrompt<'a>()
                    .Title($" :abacus: {title}")
                    .PageSize(10)
                    .AddChoices(choices)
                    .UseConverter(_.ToString())
                    .EnableSearch()
    AnsiConsole.Prompt(prompt)

let choices<'a> (title:string) (choices: 'a list) =
    writeBlankLine()
    let prompt = MultiSelectionPrompt<'a>()
                    .Title($" :abacus: {title}")
                    .Required()
                    .PageSize(10)
                    .AddChoices(choices)
                    .UseConverter(_.ToString())
    AnsiConsole.Prompt(prompt)

