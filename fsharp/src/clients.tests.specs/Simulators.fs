﻿namespace clients.tests.specs

module Simulators = 
    
    open System.IO
    open System.Text
    open clients.tests.Specs

    let createMissionsFileFrom (clientFolder:string) (missions:MissionSpec list) =
        
        let missionsFilePath = Path.Combine(clientFolder, "missions.csv")
        
        let fileContent = StringBuilder("Id,Title,StartDate,EndDate,Tj")
        
        missions |> Seq.iter (fun mission ->
            
            fileContent.AppendLine()
            |> ignore
            
            fileContent.AppendFormat("{0},{1},{2},{3},{4}",
                mission.id,
                mission.title |> Option.defaultValue "",
                mission.startDate.ToString("yyyy-MM-dd"),
                mission.endDate |> Option.map _.ToString("yyyy-MM-dd")
                                |> Option.defaultValue "",
                mission.tj)
            |> ignore
            
            )
        
        File.WriteAllText(missionsFilePath, fileContent.ToString())
        
    
    let createClientsFileFrom
        (workingDirectory:string)
        (source:ClientsSourceSpec)
        (clients:ClientSpec list) =
        
        let clientsFolder = Path.Combine(workingDirectory, source.folder)
        
        if not(Directory.Exists(clientsFolder)) then
            Directory.CreateDirectory(clientsFolder) |> ignore
        
        let clientsFilePath = Path.Combine(clientsFolder, source.file)
        
        let fileContent = StringBuilder("Id,Slug,Name,Email,ArchiveDate")
            
        clients |> Seq.iter (fun client ->
            let clientFolder = Path.Combine(clientsFolder, client.slug)
            
            if not(Directory.Exists(clientFolder)) then
                Directory.CreateDirectory(clientFolder) |> ignore
            
            fileContent.AppendLine()
            |> ignore
            fileContent.AppendFormat("{0},{1},\"{2}\",{3},{4}",
                client.id, client.slug, client.name, client.email, client.archiveDate |> Option.map _.ToString("yyyy-MM-dd") |> Option.defaultValue "")
            |> ignore
            
            if client.missions.Length > 0 then
                createMissionsFileFrom clientFolder client.missions
        )
        
        use file = File.Create(clientsFilePath)
        use streamWriter = new StreamWriter(file)
        
        streamWriter.Write(fileContent.ToString())

