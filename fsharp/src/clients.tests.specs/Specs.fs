﻿namespace clients.tests.Specs

open System

open backbone
open backbone.for'.tests

module Tj =
    open Bogus
    let generate (faker:Faker) =
        faker.Random.Int(650, 1000)

module MissionTitle =
    open Bogus
    let generate (faker:Faker) =
        let isNone = faker.Random.Bool()
        if isNone then
            None
        else 
            Some (faker.PickRandom(["ScrumMaster";"Dev";"TechLead";"Coach";"Cacib - Dev"]))

    
type MissionUpdateSpec =
    | List of DateOnly
    | Start of DateOnly * int * string option
    | End of DateOnly
    | Tj of int * DateOnly * string option

module MissionUpdateSpec =
    let list asOfDate =
        List asOfDate
    
    let start startDate tj =
        Start (startDate, tj, None)
        
    let startWithTitle startDate tj title =
        Start (startDate, tj, Some title)
    
    let end' endDate =
        End endDate
        
    let tj tj asOfDate =
        Tj (tj, asOfDate, None)
        
    let tjWithNewTitle tj asOfDate newTitle =
        Tj (tj, asOfDate, Some newTitle)

type ClientsSourceSpec = {
    folder: string
    file: string
}
    with
        static member Default = {
            folder= "clients"
            file="clients.csv"
        }

type ClientInfoSpec =
| Email of string
| SourceCodePath of string option

module ClientInfoSpec =
    let email (email:string) : ClientInfoSpec =
        Email email
    
    let sourceCodePath (path:string option) : ClientInfoSpec =
        SourceCodePath path
    
type MissionSpec = {
    id:string
    title:string option
    startDate:DateOnly
    endDate:DateOnly option
    tj:int
}

type Setup<'a> = 'a -> 'a 

module MissionSpec =
    open Bogus
    
    
    type Builder(faker:Faker) =
        let faker = Faker.from faker        

        let mutable tj = faker |> Tj.generate 
        
        let mutable title = faker |> MissionTitle.generate
            
        let mutable startDate = faker.Date.RecentDateOnly(30)
        let mutable endDate = None
        
        member this.StartedOn (setup:DateOnlySetup) =
            startDate <- setup faker
            this
        
        member this.EndedOn (setup:DateOnlySetup) =
            endDate <- Some(setup faker)
            this
        
        member this.WithoutTitle ()=
            title <- None
            this
        
        member this.Build(id:string) =
            {
                id = id
                title = title
                startDate = startDate
                endDate = endDate
                tj = tj
            }
        
    let build id faker setup : MissionSpec =
        let builder = Builder(faker)
        
        let builder = match setup with
                      | Some setup -> setup builder
                      | None -> builder
        
        let mission = builder.Build(id)
        mission
    
type ClientSpec = {
    id: string
    slug: string
    name: string
    email: string
    missions: MissionSpec list
    archiveDate: DateOnly option
}

module ClientSpec =
    open Bogus
    
    type Builder(faker:Faker) =
        let faker = Faker.from faker
        
        let generateId()= faker.Random.Guid().ToString()

        let mutable id = generateId()
        let mutable name = faker.Company.CompanyName()

        let mutable slug = None
        let mutable email = ""
        
        let mutable archiveDate : DateOnly option = None
        
        let mutable missions : Setup<MissionSpec.Builder> option list = []
        
        member this.WithId(newId: byref<string | null>) =
            newId <- match newId with
                       | null -> id
                       | id -> id
                    
            id <- newId
            this
        
        member this.WithSlug () =
            let mutable slug : string = null 
            this.WithSlug(&slug)
        
        member this.WithSlug (newSlug: byref<string | null>) =
            newSlug <- match newSlug with
                       | null -> Slug.GenerateFrom(name).value
                       | s -> s
                    
            slug <- Some(newSlug)
            this
        
        member this.WithName x =
            name <- x
            this
        
        member this.IsArchivedOn (setup:DateOnlySetup) =
            archiveDate <- Some(setup faker)
            this
        
        member this.WithEmail(?x:string) =
            email <- x |> Option.defaultWith (fun () -> faker.Internet.Email())
            this
         
        member this.ClearMissions() =
            missions <- []
            this   
         
        member this.AddMission (?setup: Setup<MissionSpec.Builder>) =
            missions <- missions @ [setup]
            this
        
        member this.Build() =
            let slug = slug |> Option.defaultWith (fun () -> Slug.GenerateFrom(name).value)
            
            let missions = missions |> List.mapi (fun i setup ->
                                                    let id = $"{slug}#{i}"
                                                    MissionSpec.build id faker setup
                                                 )
            {
                id=id
                slug=slug
                name=name
                email=email
                missions=missions
                archiveDate=archiveDate
            }
            
            
    let build faker setup : ClientSpec =
        let builder = Builder(faker)
        
        let builder = match setup with
                      | Some setup -> setup builder
                      | None -> builder
        
        let client = builder.Build()
        client

