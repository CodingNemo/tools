﻿namespace schedule.tests.Specs

open System

type MonthlyScheduleLineSpec = {
    header : string
    days : string list
}

type MonthlyScheduleSpec = {
    reference  : string
    firstDay : DateOnly
    lastDay : DateOnly
    numberOfDays : int
    lines : MonthlyScheduleLineSpec list
    off: string list
    holiday: Map<DateOnly, string>
}
    with
        static member InitFromDate(date:DateOnly) =
            let countDaysInMonth (date:DateOnly) =
                let date = date.ToDateTime(TimeOnly.MinValue)
                let days = DateTime.DaysInMonth(date.Year, date.Month)
                days
            
            let month = match date.Month with
                        | 1 -> "Jan"
                        | 2 -> "Feb"
                        | 3 -> "Mar"
                        | 4 -> "Apr"
                        | 5 -> "May"
                        | 6 -> "Jun"
                        | 7 -> "Jul"
                        | 8 -> "Aug"
                        | 9 -> "Sep"
                        | 10 -> "Oct"
                        | 11 -> "Nov"
                        | 12 -> "Dec"
                        | _ -> failwith "invalid month number"
            
            let year = $"{date.Year}".Substring(2,2)
            let name = $"{month}{year}"
            
            let numberOfDays = date |> countDaysInMonth

            {
                reference = name
                firstDay = DateOnly(date.Year, date.Month, 1)
                lastDay = DateOnly(date.Year, date.Month, numberOfDays)
                numberOfDays = numberOfDays
                lines = []
                off = List.init numberOfDays (fun _ -> " ")
                holiday = Map.empty
            }
            
        member this.AddFullMissionLine (mission:string) =
            let mutable days = []
            
            let isWeekend (date:DateOnly) =
                date.DayOfWeek = DayOfWeek.Saturday || date.DayOfWeek = DayOfWeek.Sunday
            
            let isHoliday (date:DateOnly) =
                this.holiday |> Map.containsKey(date)

            for i in 1..this.numberOfDays do
                let date = DateOnly(this.firstDay.Year, this.firstDay.Month, i)
                if isWeekend date || isHoliday date then
                    days <- days @ [ " " ]
                else
                    days <- days @ [ "X" ]
            
            let line = {
                header = $"mission:{mission}"
                days = days
            }
            
            { this with lines = this.lines @ [ line ] }
            
        member this.AddEmptyMissionLine (mission:string) =
            
            let mutable days = []
            
            for i in 1..this.numberOfDays do
                days <- days @ [ " " ]
            
            let line = {
                header = $"mission:{mission}"
                days = days
            }
            
            { this with lines = this.lines @ [ line ] }
            
        member this.AddOffDate (date:DateOnly, span:string) =
            let index = date.Day - 1
            let off = this.off |> List.mapi (fun i x -> if i = index then span else x)
            { this with off = off }
            
        member this.AddHoliday (date:DateOnly, ?name:string) =
            let name = match name with
                       | Some name -> name
                       | None -> "holiday"
                       
            { this with holiday = this.holiday |> Map.add date name }

type ScheduleSpec() =
    let mutable monthlySchedules = [] : MonthlyScheduleSpec list
    
    member this.MonthlySchedules = monthlySchedules
    
    member this.AddMonthlySchedule (date:DateOnly, setup:MonthlyScheduleSpec -> MonthlyScheduleSpec) =
        let monthlySchedule = MonthlyScheduleSpec.InitFromDate date |> setup
        monthlySchedules <- monthlySchedules @ [ monthlySchedule ]
        monthlySchedule.reference
        
    member this.AddQuarterlySchedule (date:DateOnly, setup:MonthlyScheduleSpec -> MonthlyScheduleSpec) =
        let startOfQuarter = DateOnly(date.Year, (date.Month - 1) / 3 * 3 + 1, 1)
        
        for i in 0..2 do
            let date = startOfQuarter.AddMonths i
            this.AddMonthlySchedule (date, setup) |> ignore
        
        let numberOfQuarter = (date.Month - 1) / 3 + 1
        let shortYear = $"{date.Year}".Substring(2,2)
        $"Q{numberOfQuarter}{shortYear}"
        
    member this.AddYearlySchedule (date:DateOnly, setup:MonthlyScheduleSpec -> MonthlyScheduleSpec) =
        let startOfYear = DateOnly(date.Year, 1, 1)
        
        for i in 0..11 do
            let date = startOfYear.AddMonths i
            this.AddMonthlySchedule (date, setup) |> ignore
           
        let shortYear = $"{date.Year}".Substring(2,2)
        $"Y{shortYear}"

type ScheduleSourceSpec = {
    folder : string
}
    with
        static member Default = { folder = "schedule" }