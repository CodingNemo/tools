﻿namespace schedule.tests.Specs

open System.IO
open System.Text
open schedule.tests.Specs

module Simulators = 
    let createMonthlySchedule (workingDirectory:string) (scheduleSource:ScheduleSourceSpec) (monthlySchedule: MonthlyScheduleSpec) =
        let scheduleFolder = Path.Combine(workingDirectory, scheduleSource.folder)
        if not(Directory.Exists(scheduleFolder)) then
            Directory.CreateDirectory(scheduleFolder) |> ignore

        let scheduleYearFolder = Path.Combine(scheduleFolder, $"{monthlySchedule.firstDay.Year}")
        if not(Directory.Exists(scheduleYearFolder)) then
            Directory.CreateDirectory(scheduleYearFolder) |> ignore
        
        let monthlyScheduleFilePath = Path.Combine(scheduleYearFolder, $"{monthlySchedule.reference}.schedule")
         
        let header = $"{monthlySchedule.reference} | " + (Seq.init monthlySchedule.numberOfDays (fun i -> $"{(i+1).ToString().PadLeft(2,'0')}") |> String.concat " | ")
            
        let fileContent = StringBuilder()
        fileContent.AppendLine(header) |> ignore
        
        for line in monthlySchedule.lines do
            fileContent.AppendLine($"{line.header} | " + (line.days |> String.concat " | ")) |> ignore

        for holiday in monthlySchedule.holiday do
            let date, name = (holiday.Key, holiday.Value)
            
            let daysBefore = (fun _ -> " ")
                                |> List.init (date.Day - 1)
            let day = "X"                   
            let daysAfter = (fun _ -> " ")
                                |> List.init (monthlySchedule.numberOfDays - date.Day) 

            let days = daysBefore @ [day] @ daysAfter                                
            fileContent.AppendLine($"holiday:{name} | " + (days |> String.concat " | ")) |> ignore
            
                        
        fileContent.AppendLine("off | " + (monthlySchedule.off |> String.concat " | ")) |> ignore
        
        use file = File.Create(monthlyScheduleFilePath)
        use streamWriter = new StreamWriter(file)
        
        streamWriter.Write(fileContent.ToString())