module DateTests

open FsUnit
open Xunit
open backbone.Dates

open backbone.Result

module Year = 

    [<Fact>]
    let ``Should parse a 4 digit year`` () =
        let r = "2024" |> Year.Parse
        
        r |> value
          |> should equal { value = 2024 }
        

    [<Fact>]
    let ``Should parse a 2 digit year`` () =
        let r = "24" |> Year.Parse
        
        r |> value
          |> should equal { value = 2024 }

module Month =
    
    [<Fact>]
    let ``Should parse a 3 chars month`` () =
        let r = "Feb" |> Month.Parse
        
        r |> value
          |> should equal Month.February
          
    [<Fact>]
    let ``Should parse a 2 digits month`` () =
        let r = "03" |> Month.Parse
        
        r |> value
          |> should equal Month.March
          
module Day =
    
    [<Fact>]
    let ``Should parse a 2 digits day`` () =
        let r = "03" |> Day.Parse
        
        r |> value
          |> should equal { number = 3 }
          
module Date =
    
    [<Fact>]
    let ``Should parse a long format date`` () =
        let date = "2026-Sep-03"
                    |> Date.Parse
                    |> value
          
        date.day.number |> should equal 3
        date.month |> should equal Month.September
        date.year.value |> should equal 2026

    [<Fact>]
    let ``Should parse a short format date`` () =
        let date = "2023-08-10"
                   |> Date.Parse
                   |> value
        
        date.day.number |> should equal 10
        date.month |> should equal Month.August
        date.year.value |> should equal 2023
