﻿namespace clients.tests

open System
open clients

module Exe =
    
    open backbone.for'.tests
    open backbone.for'.tests.Exe
    open backbone.for'.tests.Asserts.Exe

    open clients.tests.Specs
    
    open FsUnit.Xunit
    
    type ClientExe =
        {
            Exe:Exe
            ClientsSource:ClientsSourceSpec
        }
        member this.ClientsSourceFullPath =
            System.IO.Path.Combine(this.Exe.Context.workingFolder, this.ClientsSource.folder)
            
        member this.FullPathInClientsSource(relativePath) =
            System.IO.Path.Combine(this.ClientsSourceFullPath, relativePath)
        
        member this.FullPathInWorkingDirectory(relativePath) =
            System.IO.Path.Combine(this.Exe.Context.workingFolder, relativePath)
        
        interface IDisposable with
            member this.Dispose() =
                (this.Exe :> IDisposable).Dispose()
    
    module ClientExe =
        let run (args:string) (exe:ClientExe) =
            let args = args |> Args.defaulting (Map.empty
                |> Map.add "--clients-location" (fun _ -> $"folder:{System.IO.Path.Combine(exe.Exe.Context.workingFolder, exe.ClientsSource.folder)}")
            )
            
            exe.Exe |> run args
            
    let inline clientsExe<'scenario
            when ^scenario : (member ClientsSource:ClientsSourceSpec)
            and 'scenario :> Scenario
            > simulators (scenario:'scenario) =
        {
            Exe = new Exe("clients", scenario, simulators)
            ClientsSource = scenario.ClientsSource
        }
    
    open ClientExe
    
    let add (client:ClientSpec) (exe:ClientExe) =
        exe |> run $"add \"{client.name}\" --id {client.id}"
    
    let addWithEmail (client:ClientSpec) (exe:ClientExe) =
        exe |> run $"add \"{client.name}\" --id {client.id} --email {client.email}"
    
    let addWithSlug (client:ClientSpec) (exe:ClientExe) =
        exe |> run $"add \"{client.name}\" --id {client.id} --slug {client.slug}"
    
    let archiveById (client:ClientSpec) (exe:ClientExe) =
        exe |> run $"archive {client.id}"
    
    let archiveBySlug (client:ClientSpec) (exe:ClientExe) =
        exe |> run $"archive {client.slug}"
    
    let archiveByPartialSlug (client:ClientSpec) (exe:ClientExe) =
        exe |> run $"archive {client.slug.Substring(0,4)}"
    
    let list (exe:ClientExe) =
        exe |> run "list"
        
    let listAll (exe:ClientExe) =
        exe |> run "list --all"

    let current (asOfDate:DateOnly) (exe:ClientExe) =
        let asOfDate = asOfDate.ToString("yyyy-MM-dd")
        exe |> run $"current --as-of {asOfDate}"
    
    let set (client:ClientSpec) (info:ClientInfoSpec) (exe:ClientExe)=
        let setArgs = match info with
                      | Email email -> $"email {email}"
                      | SourceCodePath (Some path) -> $"source {path}"
                      | SourceCodePath None ->
                          let sourceRootFolder = System.IO.Path.Combine(exe.Exe.Context.workingFolder, "source")
                          $"source --root-folder {sourceRootFolder}"
        
        exe |> run $"set {client.id} $..$ {setArgs}"
        
        
    let mission (client:ClientSpec) (mission:MissionUpdateSpec) (exe:ClientExe) =
        
        let missionUpdate = match mission with
                            | List asOfDate->
                                let asOfDate = asOfDate.ToString("yyyy-MM-dd")
                                $"list --as-of {asOfDate}"
                            | Start (startDate, tj, None) ->
                                let startDate = startDate.ToString("yyyy-MM-dd")
                                $"start {tj} {startDate}"
                            | Start (startDate, tj, Some title) ->
                                let startDate = startDate.ToString("yyyy-MM-dd")
                                $"start {tj} {startDate} --title {title}"
                            | End endDate ->
                                let endDate = endDate.ToString("yyyy-MM-dd")
                                $"end {endDate}"
                            | Tj (newTj,asOfDate, None) ->
                                let asOfDate = asOfDate.ToString("yyyy-MM-dd")
                                $"tj {newTj} --as-of {asOfDate}"
                            | Tj (newTj,asOfDate, Some newTitle) ->
                                let asOfDate = asOfDate.ToString("yyyy-MM-dd")
                                $"tj {newTj} --as-of {asOfDate} --new-title {newTitle}"
        
        exe |> run $"mission {client.id} $..$ {missionUpdate}"
        
    let withSuccess (run:Run) =
        run |> should be successful
        run
        
    let withError (errorCode:ErrorCodes) (run:Run) =
        run |> should be (inError (int errorCode))
        run