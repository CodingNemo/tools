﻿namespace clients.tests.AddClient

open Xunit

open backbone.for'.tests
open backbone.for'.tests.Asserts.IO

open FsUnit.Xunit
open VerifyXunit

open clients
open clients.tests.Exe
open clients.tests.AddClient.Scenario
open clients.tests.AddClient.Simulators

type VerifyAddClient = { Add: Run; List: Run }

type Should() =

    let clientsExeFor = clientsExe [ simulateClientsSource ]

    [<Fact>]
    member _.``Add a client``() =
        task {
            let scenario = AddClient(12345)
            use clientsExe = clientsExeFor scenario

            let addClientRun = clientsExe |> add scenario.NewClient |> withSuccess

            clientsExe.FullPathInClientsSource(scenario.NewClient.slug)
            |> should be anExistingFolder

            let listClientsRun = clientsExe |> list |> withSuccess

            let! _ =
                Verifier.Verify
                    { Add = addClientRun
                      List = listClientsRun }

            ()
        }

    [<Fact>]
    member _.``Add a client with an email``() =
        task {
            let scenario = AddClient(12345).WithNewClient(_.WithEmail())

            use clientsExe = clientsExeFor scenario

            let addClientRun = clientsExe |> addWithEmail scenario.NewClient |> withSuccess

            clientsExe.FullPathInClientsSource(scenario.NewClient.slug)
            |> should be anExistingFolder

            let listClientsRun = clientsExe |> list |> withSuccess

            let! _ =
                Verifier.Verify
                    { Add = addClientRun
                      List = listClientsRun }

            ()
        }

    [<Fact>]
    member _.``Fail to add a client with an existing id``() =
        task {
            let mutable id: string | null = null

            let scenario =
                AddClient(67801)
                    .AddExistingClient(_.WithId(&id))
                    .WithNewClient(_.WithId(&id))

            use clientsExe = clientsExeFor scenario

            let addClientRun =
                clientsExe |> add scenario.NewClient |> withError ErrorCodes.Client

            let! _ = Verifier.Verify addClientRun
            ()
        }

    [<Fact>]
    member _.``Fail to add a client with an existing slug``() =
        task {
            let mutable slug: string | null = null

            let scenario =
                AddClient(67801)
                    .AddExistingClient(_.WithSlug(&slug))
                    .WithNewClient(_.WithSlug(&slug))

            use clientsExe = clientsExeFor scenario

            let addClientRun =
                clientsExe |> addWithSlug scenario.NewClient |> withError ErrorCodes.Client

            let! _ = Verifier.Verify addClientRun
            ()
        }

    [<Fact>]
    member _.``Fail to add a client with an existing partial slug``() =
        task {

            let mutable slug1: string = "slug-1"
            let mutable slug2: string = "slug-2"
            let mutable slug3: string = "slug-3"
            let mutable slug4: string = "slug-4"
            let mutable slug: string = "slug"

            let scenario =
                AddClient(67802)
                    .AddExistingClient(_.WithSlug(&slug1))
                    .AddExistingClient(_.WithSlug(&slug2))
                    .AddExistingClient(_.WithSlug(&slug3))
                    .AddExistingClient(_.WithSlug(&slug4))
                    .WithNewClient(_.WithSlug(&slug))

            use clientsExe = clientsExeFor scenario

            let addClientRun =
                clientsExe |> addWithSlug scenario.NewClient |> withError ErrorCodes.Client

            let! _ = Verifier.Verify addClientRun
            ()
        }
