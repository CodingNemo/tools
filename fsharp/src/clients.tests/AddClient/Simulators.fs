﻿namespace clients.tests.AddClient

open backbone.for'.tests
open clients.tests.AddClient.Scenario
open clients.tests.Exe
open clients.tests.specs.Simulators

module Simulators = 
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? AddClient as addClient -> 
            addClient.Clients |> createClientsFileFrom context.workingFolder addClient.ClientsSource
        | _ -> ()

