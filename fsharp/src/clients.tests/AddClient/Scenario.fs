﻿namespace clients.tests.AddClient

open backbone.for'.tests

module Scenario = 
    
    open clients.tests.Specs

    type AddClient(?seed:int) =
        let faker = Faker.create(seed)
        let mutable clients : ClientSpec list = []
        let buildClient = ClientSpec.build faker

        member val ClientsSource : ClientsSourceSpec = ClientsSourceSpec.Default
        
        
        member this.Clients: ClientSpec list = clients
        
        member val NewClient : ClientSpec = buildClient None with get,set
        
        member this.AddExistingClient(?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            clients <- clients @ [buildClient setup]
            this
        
        member this.WithNewClient (?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            this.NewClient <- buildClient setup
            this
        
        interface Scenario
