namespace clients.tests.ListClients


open Xunit

open backbone.for'.tests
open backbone.for'.tests.Asserts.Exe

open FsUnit.Xunit
open VerifyXunit

open clients.tests.Exe
open clients.tests.ListClients.Scenario
open clients.tests.ListClients.Simulators

type Should() =

    let clientsExeFor = clientsExe [simulateClientsSource]
    
    [<Fact>]
    member _.``List all non archived clients`` () =
        task {
            let scenario = ListClients(67890)
                            .AddClients(setup = _.IsArchivedOn(DateOnly.yesterday))
                            .AddClients(setup = _.AddMission(_.StartedOn(DateOnly.pastDayFromNow(6<days>))))
                            .AddClients()
                            
            use clientsExe = clientsExeFor scenario
        
            let listClientsRun =
                clientsExe |> list

            listClientsRun
            |> should be successful
            
            let! _ = Verifier.Verify listClientsRun
            ()
        }
        
    [<Fact>]
    member _.``List all clients`` () =
        task {
            let scenario = ListClients(45121)
                            .AddClients(setup = _.IsArchivedOn(DateOnly.yesterday))
                            .AddClients(setup = _.AddMission(_.StartedOn(DateOnly.pastDayFromNow(6<days>))))
                            .AddClients()
                            
            use clientsExe = clientsExeFor scenario
        
            let listClientsRun =
                clientsExe |> listAll

            listClientsRun
            |> should be successful
            
            let! _ = Verifier.Verify listClientsRun
            ()
        }

