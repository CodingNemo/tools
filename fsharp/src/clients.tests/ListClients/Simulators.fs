﻿namespace clients.tests.ListClients

open backbone.for'.tests

module Simulators =
    
    open clients.tests.specs.Simulators
    open clients.tests.ListClients.Scenario
    
    let simulateClientsSource (context:ExeContext) (scenario:Scenario)  =
        match scenario with
        | :? ListClients as listClients -> 
            listClients.Clients |> createClientsFileFrom context.workingFolder listClients.ClientsSource
        | _ ->
            ()
