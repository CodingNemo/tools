﻿namespace clients.tests.ListClients

open clients.tests.Specs


module Scenario =
    open backbone.for'.tests
    
    type ListClients(?seed:int) =
        
        let faker = Faker.create(seed)
        
        member val ClientsSource : ClientsSourceSpec = ClientsSourceSpec.Default
        member val Clients : ClientSpec list = [] with get,set
        
        member this.AddClient (?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            let builder = ClientSpec.Builder(faker)
            
            let builder = match setup with
                          | Some setup -> setup builder
                          | None -> builder
            
            let client = builder.Build()
            this.Clients <- client :: this.Clients
            
            this
        
        member this.AddClients(?count:int, ?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            let count = count |> Option.defaultWith (fun _ -> faker.Random.Int(3,13))
            
            for _ in 1..count do
                match setup with
                | Some setup -> this.AddClient(setup) |> ignore
                | None -> this.AddClient() |> ignore
                
            this
        
        interface Scenario


