﻿namespace clients.tests.Missions.End

open backbone.for'.tests
open clients.tests.Missions.End.Scenario
open clients.tests.specs.Simulators

module Simulators = 
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? EndMission as endMission ->
            let client = endMission.Client
            let clients = [client]
            clients |> createClientsFileFrom context.workingFolder endMission.ClientsSource
        | _ -> ()


