﻿namespace clients.tests.Missions.End

open backbone.for'.tests
open clients.tests.Specs

module Scenario = 

    type EndMission(?seed:int) =
        let faker = Faker.create(seed)
        let mutable endDate = DateOnly.tomorrow faker
        let clientSource = ClientsSourceSpec.Default
        
        let buildClient = ClientSpec.build faker
        let mutable client = buildClient (Some (_.AddMission()))
        
        member this.Client = client
        
        member this.ClientsSource = clientSource
        
        member val AsOfDate = DateOnly.today faker
        
        member this.EndDate = endDate
        
        member this.WithEndDate (setup:DateOnlySetup) =
            endDate <- setup faker
            this
                
        member this.WithClient (?setup:Setup<ClientSpec.Builder>) =
            client <- buildClient setup
            this

        interface Scenario
