﻿namespace clients.tests.Missions.End

open VerifyXunit
open Xunit
open backbone.for'.tests

open clients
open clients.tests.Missions.End.Scenario
open clients.tests.Missions.End.Simulators
open clients.tests.Exe
open clients.tests.Specs.MissionUpdateSpec

type VerifyEndMission = {
    End: Run
    AsOf: string
    List: Run
}

type Should() =
    let clientsExeFor = clientsExe [simulateClientsSource]
    
    [<Fact>]
    member _.``End a mission when a mission already started`` () =
        task {
            let scenario = EndMission(25333)
            
            use clientsExe = clientsExeFor scenario
            
            let endMissionRun = clientsExe |> mission scenario.Client (end' scenario.EndDate) 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                End = endMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd")
                List = listMissionRun
            }
            ()
        }
        
    [<Fact>]
    member _.``End a mission in the past`` () =
        task {
            let scenario = EndMission(25343)
                            .WithClient(_.AddMission(_.StartedOn(DateOnly.pastDayBetween -3<days> -35<days>)))
                            .WithEndDate DateOnly.yesterday
            
            use clientsExe = clientsExeFor scenario
            
            let endMissionRun = clientsExe |> mission scenario.Client (end' scenario.EndDate) 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                End = endMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd")
                List = listMissionRun
            }
            ()
        }
        
    [<Fact>]
    member _.``End a mission in the future`` () =
        task {
            let scenario = EndMission(25343)
                            .WithEndDate (DateOnly.futureDayFromNow 10<days>)
            
            use clientsExe = clientsExeFor scenario
            
            let endMissionRun = clientsExe |> mission scenario.Client (end' scenario.EndDate) 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                End = endMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd")
                List = listMissionRun
            }
            ()
        }
        
    [<Fact>]
    member _.``Fail to end a mission when no started mission`` () =
        task {
            let scenario = EndMission(25363)
                            .WithClient(
                                _.AddMission(
                                    _.StartedOn(DateOnly.pastDayBetween -5<days> -10<days>)
                                     .EndedOn(DateOnly.pastDayFromNow -5<days>)
                                    ))
            
            use clientsExe = clientsExeFor scenario
            
            let endMissionRun = clientsExe |> mission scenario.Client (end' scenario.EndDate) 
                                           |> withError ErrorCodes.Mission
                                           
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
                                             
            let! _ = Verifier.Verify {
                End = endMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd")
                List = listMissionRun
            }
            ()
        }
        
    