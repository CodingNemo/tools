﻿namespace clients.tests.Missions.Start

open backbone.for'.tests
open clients.tests.Missions.Start.Scenario
open clients.tests.specs.Simulators

module Simulators = 
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? StartMission as startNewMission -> 
            let client = startNewMission.Client
            let clients = [client]
            clients |> createClientsFileFrom context.workingFolder startNewMission.ClientsSource
        | _ -> ()


