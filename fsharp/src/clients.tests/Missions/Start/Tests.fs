﻿namespace clients.tests.Missions.Start

open VerifyXunit
open Xunit
open backbone.for'.tests
open backbone.for'.tests.DateOnly
open clients
open clients.tests.Missions.Start.Scenario
open clients.tests.Missions.Start.Simulators
open clients.tests.Exe
open clients.tests.Specs.MissionUpdateSpec

type VerifyStartMission = {
    Start: Run
    AsOf: string
    List: Run
}

type VerifyStartFailureMission = {
    Start: Run
    AsOf: string
}

type Should() =
    
    let clientsExeFor = clientsExe [simulateClientsSource]
    
    [<Fact>]
    member _.``Start a mission when no mission already started`` () =
        task {
            let scenario = StartMission(25323)
            
            use clientsExe = clientsExeFor scenario 
            
            let startMissionRun = clientsExe |> mission scenario.Client (start scenario.StartDate scenario.Tj) 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                Start = startMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd") 
                List = listMissionRun
            }
            ()
        }
        
    [<Fact>]
    member _.``Start a mission with a title`` () =
        task {
            let scenario = StartMission(25431)
            
            use clientsExe = clientsExeFor scenario
            
            let startMissionRun = clientsExe |> mission scenario.Client (startWithTitle scenario.StartDate scenario.Tj "Coach") 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                Start = startMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd") 
                List = listMissionRun
            }
            ()
        }

    [<Fact>]
    member _.``Start a mission yesterday when no mission already started`` () =
        task {
            let scenario = StartMission(25325)
                                .WithStartDateOn yesterday
            
            use clientsExe = clientsExeFor scenario
            
            let startMissionRun = clientsExe |> mission scenario.Client (start scenario.StartDate scenario.Tj) 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                Start = startMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd") 
                List = listMissionRun
            }
            ()
        }
    
    [<Fact>]
    member _.``Start a mission in the future when no mission already started`` () =
        task {
            let scenario = StartMission(25325)
                            .WithStartDateOn (futureDayFromNow 3<days>)
            
            use clientsExe = clientsExeFor scenario
            
            let startMissionRun = clientsExe |> mission scenario.Client (start scenario.StartDate scenario.Tj) 
                                             |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify {
                Start = startMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd")
                List = listMissionRun
            }
            ()
        }
    
    [<Fact>]
    member _.``Fail when starting a mission while another mission has already started`` () =
        task {
            let scenario = StartMission(25326)
                            .WithClient (_.AddMission())
            
            use clientsExe = clientsExeFor scenario
            
            let startMissionRun = clientsExe |> mission scenario.Client (start scenario.StartDate scenario.Tj) 
                                             |> withError ErrorCodes.Mission
            
            let! _ = Verifier.Verify {
                Start = startMissionRun
                AsOf = scenario.AsOfDate.ToString("yyyy-MM-dd")
            }
            ()
        }
