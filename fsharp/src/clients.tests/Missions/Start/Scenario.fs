﻿namespace clients.tests.Missions.Start

open backbone.for'.tests
open clients.tests.Specs

module Scenario = 

    type StartMission(?seed:int) =
        let faker = Faker.create(seed)
        
        let buildClient = ClientSpec.build faker
        
        let mutable client = buildClient None
        
        let mutable startDate = faker |> DateOnly.pastDayFromNow -3<days>
        
        member this.Client = client
        
        member val ClientsSource = ClientsSourceSpec.Default with get,set

        member this.StartDate = startDate
        member this.AsOfDate = faker |> DateOnly.today
        member this.Tj = faker |> Tj.generate
        
        member this.WithStartDateOn (setup:DateOnlySetup) =
            startDate <- setup faker
            this
                
        member this.WithClient (?setup:Setup<ClientSpec.Builder>) =
            client <- buildClient setup
            this

        interface Scenario
