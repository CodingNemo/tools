﻿namespace clients.tests.Missions.UpdateTj

open backbone.for'.tests
open clients.tests.Specs

module Scenario = 

    type UpdateTj(?seed:int) =
        let faker = Faker.create(seed)
        
        let mutable asOfDate = faker |> DateOnly.today
        let clientSource = ClientsSourceSpec.Default
        
        let buildClient = ClientSpec.build faker
        let mutable client = buildClient (Some (_.AddMission()))
        
        member this.Client = client
        
        member this.ClientsSource = clientSource
        
        member this.AsOfDate = asOfDate
                
        member this.NewTj = faker |> Tj.generate
                
        member this.AsOf (setup:DateOnlySetup) =
            asOfDate <- setup faker
            this
                
        member this.WithClient (?setup:Setup<ClientSpec.Builder>) =
            client <- buildClient setup
            this

        interface Scenario
