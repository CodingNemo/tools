﻿namespace clients.tests.Missions.UpdateTj

open backbone.for'.tests
open clients.tests.Missions.UpdateTj.Scenario
open clients.tests.specs.Simulators

module Simulators = 
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? UpdateTj as endMission ->
            let client = endMission.Client
            let clients = [client]
            clients |> createClientsFileFrom context.workingFolder endMission.ClientsSource
        | _ -> ()


