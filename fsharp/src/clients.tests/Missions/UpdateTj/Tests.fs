﻿namespace clients.tests.Missions.UpdateTj

open VerifyXunit
open Xunit
open backbone.for'.tests

open clients.tests.Missions.UpdateTj.Simulators
open clients.tests.Exe
open clients.tests.Missions.UpdateTj.Scenario
open clients.tests.Specs.MissionUpdateSpec

type VerifyUpdateTj = {
    Update: Run
    AsOfDate:string
    List: Run
}

type Should() =
    
    let clientsExeFor = clientsExe [simulateClientsSource]
    
    [<Fact>]
    member _.``End current mission and create a new one with the new TJ`` () =
        task {
            let scenario = UpdateTj(65333)
            
            use clientsExe = clientsExeFor scenario 
            
            let updateTjRun = clientsExe |> mission scenario.Client (tj scenario.NewTj scenario.AsOfDate) 
                                         |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify { Update = updateTjRun
                                       AsOfDate = scenario.AsOfDate.ToString("yyyy-MM-dd") 
                                       List = listMissionRun }
            ()
        }
    
    [<Fact>]
    member _.``End current mission and create a new one with the new TJ and a new title`` () =
        task {
            let scenario = UpdateTj(65777)
            
            use clientsExe = clientsExeFor scenario
            
            let updateTjRun = clientsExe |> mission scenario.Client (tjWithNewTitle scenario.NewTj scenario.AsOfDate "DevOps") 
                                         |> withSuccess
                                             
            let listMissionRun = clientsExe |> mission scenario.Client (list scenario.AsOfDate)
                                            |> withSuccess
            
            let! _ = Verifier.Verify { Update = updateTjRun
                                       AsOfDate = scenario.AsOfDate.ToString("yyyy-MM-dd") 
                                       List = listMissionRun }
            ()
        }
   
    