﻿namespace clients.tests.SetClientInfo.Email

open VerifyXunit
open Xunit

open backbone.for'.tests
open clients.tests.Exe
open clients.tests.SetClientInfo.Email.Scenario
open clients.tests.SetClientInfo.Email.Simulators
open clients.tests.Specs.ClientInfoSpec

type VerifySetClientEmail = {
    Set: Run
    List: Run
}

type Should() =
    
    let clientsExeFor = clientsExe [simulateClientsSource]
    
    [<Fact>]
    member _.``Set client email`` () =
        task {
            let scenario = SetClientEmail(33456)
            
            use clientsExe = clientsExeFor scenario
            
            let setEmailRun = clientsExe |> set scenario.Client (email scenario.NewEmail)
                                         |> withSuccess
            
            let listClientsRun = clientsExe |> list
                                            |> withSuccess
            
            let! _ = Verifier.Verify { Set=setEmailRun; List=listClientsRun }
            ()
        }

