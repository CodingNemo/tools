﻿namespace clients.tests.SetClientInfo.Email

open backbone.for'.tests
open clients.tests.Specs

module Scenario =
    
    type SetClientEmail(?seed:int) =
        let faker = Faker.create(seed)
        
        let buildClient = ClientSpec.build faker
        
        member val Client = buildClient None with get,set
        
        member val ClientsSource = ClientsSourceSpec.Default with get,set
        
        member val NewEmail = faker.Internet.Email() with get,set
        
        member this.WithClient setup =
            this.Client <- buildClient setup
            this
        
        interface Scenario
        

