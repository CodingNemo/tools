﻿namespace clients.tests.SetClientInfo.Email

open backbone.for'.tests
open clients.tests.SetClientInfo.Email.Scenario

module Simulators =
    
    open clients.tests.specs.Simulators
    
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? SetClientEmail as setClientEmail -> 
            let client = setClientEmail.Client
            let clients = [client]
            clients |> createClientsFileFrom context.workingFolder setClientEmail.ClientsSource
        | _ -> ()
