﻿namespace clients.tests.GetCurrentClient

open backbone.for'.tests
open clients.tests.GetCurrentClient.Scenario
open clients.tests.specs.Simulators

module Simulators = 
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? GetCurrentClient as addClient -> 
            addClient.Clients |> createClientsFileFrom context.workingFolder addClient.ClientsSource
        | _ -> ()

