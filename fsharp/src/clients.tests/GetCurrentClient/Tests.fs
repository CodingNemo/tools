﻿namespace clients.tests.GetCurrentClient 
open Xunit

open backbone.for'.tests
open backbone.for'.tests.Asserts.IO

open FsUnit.Xunit
open VerifyXunit

open clients
open clients.tests.Exe

open clients.tests.GetCurrentClient.Simulators
open clients.tests.GetCurrentClient.Scenario

type VerifyAddClient = {
    Add: Run
    List: Run
}

type Should() =
    
    let clientsExeFor = clientsExe [simulateClientsSource]

    [<Fact>]
    member _.``Display the currently active client`` () =
        task {
            let scenario = GetCurrentClient(12345)
                            .AddClients(setup = _.AddMission(
                                _.StartedOn(DateOnly.pastDayBetween 30<days> 15<days>).EndedOn(DateOnly.pastDayBetween 10<days> 2<days>)
                            ))
                            .AddClient(_.AddMission(
                                _.StartedOn(DateOnly.pastDayBetween 30<days> 15<days>)
                            ))
                            
            use clientsExe = clientsExeFor scenario
        
            let getCurrentClientRun =
                clientsExe |> current scenario.AsOfDate 
                           |> withSuccess

            
            let! _ = Verifier.Verify getCurrentClientRun
            ()
        }


    [<Fact>]
    member _.``Fail when there is no currently active client`` () =
        task {
            let scenario = GetCurrentClient(12345)
                            .AddClients(setup = _.AddMission(
                                _.StartedOn(DateOnly.pastDayBetween 30<days> 15<days>).EndedOn(DateOnly.pastDayBetween 10<days> 2<days>)
                            ))
                            
            use clientsExe = clientsExeFor scenario
        
            let getCurrentClientRun =
                clientsExe |> current scenario.AsOfDate
                           |> withError ErrorCodes.Client
            
            let! _ = Verifier.Verify getCurrentClientRun
            ()
        }
