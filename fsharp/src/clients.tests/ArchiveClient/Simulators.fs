﻿namespace clients.tests.ArchiveClient

open backbone.for'.tests
open clients.tests.ArchiveClient.Scenario
open clients.tests.specs.Simulators

module Simulators = 
    let simulateClientsSource (context:ExeContext) (scenario:Scenario) =
        match scenario with
        | :? ArchiveClient as archiveClient -> 
            [archiveClient.Client] |> createClientsFileFrom context.workingFolder archiveClient.ClientsSource
        | _ -> ()

