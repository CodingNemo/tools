﻿namespace clients.tests.ArchiveClient

open backbone.for'.tests
open clients.tests.Specs

module Scenario =
    
    type ArchiveClient(?seed:int) =
        let faker = Faker.create(seed)
        
        let buildClient = ClientSpec.build faker
        
        member val ClientsSource : ClientsSourceSpec = ClientsSourceSpec.Default
        member val Client : ClientSpec = buildClient None with get,set 
        
        member this.WithClient (?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            this.Client <- buildClient setup
            this
        
        interface Scenario

