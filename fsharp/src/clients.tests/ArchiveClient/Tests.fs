namespace clients.tests.ArchiveClient 

open Xunit

open backbone.for'.tests
open backbone.for'.tests.Asserts.IO

open VerifyXunit
open FsUnit.Xunit

open clients.tests.ArchiveClient.Simulators
open clients.tests.Exe
open clients.tests.ArchiveClient.Scenario


type VerifyArchiveClient = {
    Archive: Run
    ListAll: Run
}

type Should() =

    let clientsExeFor = clientsExe [simulateClientsSource]
    
    [<Fact>]
    member _.``Archive an inactive client by id`` () =
        task {
            let scenario = ArchiveClient(2241)
            use clientsExe = clientsExeFor scenario
           
            let archiveClientRun =
                clientsExe |> archiveById scenario.Client
                           |> withSuccess
           
            clientsExe.FullPathInClientsSource(scenario.Client.slug)
            |> should not' (be anExistingFolder) 

            clientsExe.FullPathInClientsSource($"{scenario.Client.slug}.zip")
            |> should be anExistingFile
            
            let listAllClientsRun =
                clientsExe |> listAll
                           |> withSuccess
            
            let! _ = Verifier.Verify { ListAll=listAllClientsRun; Archive=archiveClientRun }
            ()
        }

    [<Fact>]
    member _.``Archive an inactive client by slug`` () =
        task {
            let scenario = ArchiveClient(2243)
            use clientsExe = clientsExeFor scenario

            let archiveClientRun =
                clientsExe |> archiveBySlug scenario.Client
                           |> withSuccess
           
            clientsExe.FullPathInClientsSource(scenario.Client.slug)
            |> should not' (be anExistingFolder) 

            clientsExe.FullPathInClientsSource($"{scenario.Client.slug}.zip")
            |> should be anExistingFile
            
            let listAllClientsRun =
                clientsExe |> listAll
                           |> withSuccess
            
            let! _ = Verifier.Verify { ListAll=listAllClientsRun; Archive=archiveClientRun }
            ()
        }
        
    [<Fact>]
    member _.``Archive an inactive client by partial slug`` () =
        task {

            let scenario = ArchiveClient(2242)
            use clientsExe = clientsExeFor scenario
           
            let archiveClientRun =
                clientsExe |> archiveByPartialSlug scenario.Client
                           |> withSuccess
           
            clientsExe.FullPathInClientsSource(scenario.Client.slug)
            |> should not' (be anExistingFolder) 

            clientsExe.FullPathInClientsSource($"{scenario.Client.slug}.zip")
            |> should be anExistingFile
            
            let listAllClientsRun =
                clientsExe |> listAll
                           |> withSuccess
            
            let! _ = Verifier.Verify { ListAll=listAllClientsRun; Archive=archiveClientRun }
            ()
        }

    [<Fact>]
    member _.``Fail to archive an active client`` () =
        task {
            let scenario = ArchiveClient(2315)
                            .WithClient(_.AddMission(_.StartedOn(DateOnly.pastDayFromNow(5<days>))))
                                         
            use clientsExe = clientsExeFor scenario
           
            let archiveClientRun =
                clientsExe |> archiveByPartialSlug scenario.Client
                           |> withError clients.ErrorCodes.Client
           
            clientsExe.FullPathInClientsSource(scenario.Client.slug)
            |> should be anExistingFolder 

            clientsExe.FullPathInClientsSource($"{scenario.Client.slug}.zip")
            |> should not' (be anExistingFile)
            
            let listAllClientsRun =
                clientsExe |> listAll
                           |> withSuccess
            
            let! _ = Verifier.Verify { ListAll=listAllClientsRun; Archive=archiveClientRun }
            ()
        }
