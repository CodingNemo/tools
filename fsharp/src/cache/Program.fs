﻿open System.ComponentModel
open cache.lib

open Humanizer

open Spectre.Console
open Spectre.Console.Cli
open Spectre.Console.Json

open System.Globalization

type CacheSettings() =
    inherit CommandSettings()
    [<CommandOption("-g|--global")>]
    [<Description("access the global cache")>]    
    member val Global : bool = false with get, set

module CachePrompt =
    let private choseOneItem cache =
        let items = cache |> Cache.items
        let prompt = SelectionPrompt<Cache.Item>()

        prompt.Title <- "Select a cache item" 
        prompt.PageSize <- 10
        
        prompt.AddChoices(items) |> ignore
        prompt.Converter <- _.key
        
        AnsiConsole.Prompt(prompt)
        
    let private (|IsNullOrWhitespace|_|) k =
        if System.String.IsNullOrWhiteSpace(k) then Some()
        else None
    let getItemByKey key cache =
        match key with
        | IsNullOrWhitespace -> choseOneItem cache
        | key -> 
            let item = Cache.tryGet cache key
            
            match item with
            | None ->
                failwith $"Item '{key}' does not exist"
            | Some item ->
                item

module ListAllCacheItems =
    
    type Settings() =
        inherit CacheSettings()
    
    type Command() =
        inherit Command<Settings>()
        interface ICommandLimiter<CacheSettings>

        override this.Execute(_, settings) =
            let cache = Cache.build settings.Global
            
            let table = Table()
                            .AddColumn("key")
                            .AddColumn("age")
                            .AddColumn("ttl")
                            .AddColumn("expired")
            
            let addRow (cacheItem:Cache.Item) : unit =
                table.AddRow(
                    cacheItem.key,
                    cacheItem.age.Humanize(),
                    cacheItem.ttl.Humanize(),
                    if cacheItem.expired then "[red]yes[/]" else "[green]no[/]"
                ) |> ignore
            
            
            cache |> Cache.items
                  |> List.iter addRow
            
            AnsiConsole.Write(table)
            0
                  
module ClearCache =
    
    type ClearCacheSettings() =
        inherit CacheSettings()
        [<CommandOption("-f|--force")>]
        member val Force : bool = false with get, set
    
    module All =
    
        type Settings() =
            inherit ClearCacheSettings()
        
        type Command() =
            inherit Command<Settings>()
            interface ICommandLimiter<ClearCacheSettings>

            override this.Execute(_, settings) =
                let cache = Cache.build settings.Global
                let items = cache |> Cache.items
                
                items |> List.iter (fun item ->
                        if settings.Force || AnsiConsole.Confirm($"Deleting item {item.key}") then
                            item.Delete()
                    )
                
                0
    
    module Expired =
    
        type Settings() =
            inherit ClearCacheSettings()
        
        type Command() =
            inherit Command<Settings>()
            interface ICommandLimiter<ClearCacheSettings>

            override this.Execute(_, settings) =
                let cache = Cache.build settings.Global
                let items = cache |> Cache.items
                
                items
                    |> List.filter (fun item -> item.expired)
                    |> List.iter (fun item ->
                        if settings.Force || AnsiConsole.Confirm($"Deleting item {item.key}") then
                            item.Delete()
                    )
                0
                
    module Item =
    
        type Settings() =
            inherit ClearCacheSettings()
            [<CommandArgument(0, "[KEY]")>]
            member val Key : string = "" with get, set
        
        type Command() =
            inherit Command<Settings>()
            interface ICommandLimiter<ClearCacheSettings>

            override this.Execute(_, settings) =
                let cache = Cache.build settings.Global
                let item = cache |> CachePrompt.getItemByKey settings.Key
                
                if settings.Force || AnsiConsole.Confirm($"Deleting item {item.key}") then
                    item.Delete()
                
                0
                

open ClearCache

module OpenCacheLocation =
    
    type Settings() =
        inherit CacheSettings()
    
    type Command() =
        inherit Command<Settings>()
        interface ICommandLimiter<CacheSettings>

        override this.Execute(_, settings) =
            let cache = Cache.build settings.Global
           
            cache |> Cache.ensureExists
            
            System.Diagnostics.Process.Start("explorer", cache.path) |> ignore
            
            0
        
module ShowOneCacheItem =
    
    type Settings() =
        inherit CacheSettings()
        [<CommandArgument(0, "[KEY]")>]
        member val Key : string = "" with get, set
    
    type Command() =
        inherit Command<Settings>()
        interface ICommandLimiter<CacheSettings>
        override this.Execute(_, settings) =
            let cache = Cache.build settings.Global
            
            let item = cache |> CachePrompt.getItemByKey settings.Key

            let content = item.GetRawContent()
            let jsonText = JsonText(content)
            let jsonPanel = Panel(jsonText)
            jsonPanel.Expand <- false
            jsonPanel.Header <- PanelHeader("Content", Justify.Left)
                
            let expiration = if item.expired then "[red]expired[/]" else $"Expires in {(item.ttl - item.age).Humanize()}"
                
            let infoLayout = Layout(
                Markup(expiration)
            )
            infoLayout.MinimumSize <- 1
            infoLayout.Size <- 2
            
            let layout = Layout("Root").SplitRows(
                infoLayout,
                Layout(jsonPanel)
            )
            
            let panel = Panel(layout)
            panel.Expand <- false
          
            let header = PanelHeader(item.key, Justify.Left)
            panel.Header <- header

            AnsiConsole.Write(panel)
            0


[<EntryPoint>]
let main args =
        CultureInfo.CurrentCulture <- CultureInfo("en-US")
        CultureInfo.CurrentUICulture <- CultureInfo("en-US")
        
        let app = CommandApp()
        
        app.Configure(fun config ->
            config.AddBranch<CacheSettings>("show", fun show ->
                show.SetDescription("Show cache items")
                show.SetDefaultCommand<ListAllCacheItems.Command>()
                show.AddCommand<ShowOneCacheItem.Command>("item") |> ignore
            ) |> ignore
            
            config.AddCommand<OpenCacheLocation.Command>("open")
                  .WithDescription("Open cache in explorer")
            |> ignore
            
            config.AddBranch<ClearCacheSettings>("clear", fun clear ->
                clear.SetDescription("Clear cache items")
                clear.AddCommand<All.Command>("all")
                     .WithDescription("all items") |> ignore
                clear.AddCommand<Expired.Command>("expired")
                     .WithAlias("exp")
                     .WithDescription("only expired items") |> ignore
                clear.AddCommand<Item.Command>("item")
                     .WithDescription("one item") |> ignore
            ) |> ignore
        )
        app.Run args
