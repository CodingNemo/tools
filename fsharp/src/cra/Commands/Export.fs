﻿namespace cra

open Spectre.Console

open backbone
open backbone.Dates
open backbone.DataSource.DataSource
open backbone.DataSource.Instructions

open backbone.Program

open Spectre.Console.Cli

open clients.lib
open schedule.lib.Schedule

open cra.lib
open cra.lib.Instructions


module Export =

    type Settings() =
        inherit CommandSettings()
        
        [<CommandOption("--as-of")>]
        member val AsOf : string = "" with get,set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
        
        [<CommandOption("--schedule-location", IsHidden = true)>]
        member val ScheduleLocation : string = "" with get, set
        
        [<CommandArgument(0,"[MONTH-REFERENCE]")>]
        member val MonthScheduleReference : string = "" with get, set 

        [<CommandOption("-c|--client")>]
        member val ClientSlugOrId : string = "" with get, set 
        

        [<CommandOption("--no-wait")>] 
        member val NoWait : bool = false with get,set
        

    let run (settings:Settings) =
        resPgm {
           let! asOfDate = AsOfDate.ParseOrDefault settings.AsOf |> lift
           let! monthScheduleReference = MonthReference.ParseOfDefault(settings.MonthScheduleReference, asOfDate) |> lift

           let title = FigletText($"CRA {monthScheduleReference.Display()}")
           AnsiConsole.Write(title)

           let! clientsLocation = Location.TryParse(settings.ClientsLocation)
           let! scheduleLocation = Location.TryParse(settings.ScheduleLocation)
           let! craTarget = CraTarget.parse settings.ClientSlugOrId |> lift
           
           let! clientsDataSource = DataSource.find<Clients> clientsLocation

           let! craList = Cra.GenerateAll monthScheduleReference craTarget clientsDataSource scheduleLocation 
           
           do! Cra.ExportAll craList clientsDataSource.Folder
        
           if not(settings.NoWait) then
                Wait.anyKey()
        }