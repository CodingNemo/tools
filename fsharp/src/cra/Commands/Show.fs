﻿namespace cra.Commands

open Spectre.Console

open backbone
open backbone.DataSource.Instructions
open backbone.DataSource.DataSource
open backbone.Dates
open backbone.Program

open Spectre.Console.Cli
open clients.lib

open schedule.lib.Schedule

open cra.lib
open cra.lib.Instructions

module Show =

    let private displayCra (cra:Cra) =
        AnsiConsole.MarkupLine($"Client: [green bold]{cra.Client.Name}[/]")
        
        let table = Table()
        
        table
            .AddColumns(cra.Days.Keys |> Seq.map _.day.ToString() |> Seq.toArray)
            .AddColumn("[yellow]###[/]") |> ignore
            
        let total = cra.Total
        let values = cra.Days.Values |> Seq.map string |> Seq.toList
        let values = values @ [ $"[yellow]{total}[/]" ]
            
        table.AddRow(values |> Array.ofList) |> ignore

        AnsiConsole.Write(table)
    
    type Settings() =
        inherit CommandSettings()
        
        [<CommandOption("--as-of")>]
        member val AsOf : string = "" with get,set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
        
        [<CommandOption("--schedule-location", IsHidden = true)>]
        member val ScheduleLocation : string = "" with get, set
        
        
        [<CommandArgument(0,"[MONTH-REFERENCE]")>]
        member val MonthScheduleReference : string = "" with get, set 

        
        [<CommandOption("-c|--client")>]
        member val ClientSlugOrId : string = "" with get, set 
        
        [<CommandOption("--no-wait")>] 
        member val NoWait : bool = false with get,set
                      
    let run (settings:Settings) =
        resPgm {
           let! asOfDate = AsOfDate.ParseOrDefault settings.AsOf |> lift
           let! monthScheduleReference = MonthReference.ParseOfDefault(settings.MonthScheduleReference, asOfDate) |> lift

           let title = FigletText($"CRA {monthScheduleReference.Display()}")
           AnsiConsole.Write(title)

           let! craTarget = CraTarget.parse settings.ClientSlugOrId |> lift
           let! clientsLocation = Location.TryParse(settings.ClientsLocation)
           let! clientsDataSource = DataSource.find<Clients> clientsLocation
           let! scheduleLocation = Location.TryParse(settings.ScheduleLocation)

           let! craList = Cra.GenerateAll monthScheduleReference craTarget clientsDataSource scheduleLocation 
           
           craList |> List.iter displayCra 

           if not(settings.NoWait) then
                Wait.anyKey()
        }
