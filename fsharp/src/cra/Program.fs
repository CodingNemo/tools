﻿open cra
open cra.Commands
open backbone.cli.Cli.App


[<EntryPoint>]
let main args =
    app args {
        interpreter Interpreter.interpret
        
        defaultCmd Show.run

        cmd "show" Show.run
        cmd "export" Export.run
    }
