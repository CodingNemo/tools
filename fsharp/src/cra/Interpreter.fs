﻿namespace cra

open backbone
open backbone.DataSource.Instructions
open cra.lib

module Interpreters =
    let rec interpret<'a> (program:Program<'a>): 'a =
        match program with
        | Instruction instruction ->
            match instruction with
            | :? DataSourceLocationInstruction<Program<'a>> as dli ->
                DataSource.Interpreter.interpretDataSourceLocationInstructions interpret dli
            
            | :? DataSourceInstruction<clients.lib.Clients, Program<'a>> as csi ->                  
                clients.lib.Interpreters.interpretDataSourceInstruction interpret csi
            | :? FileDataSourceInstruction<clients.lib.Clients, Program<'a>> as cfi ->
                clients.lib.Interpreters.interpretFileDataSourceInstruction interpret cfi
            
            | :? DataSourceInstruction<schedule.lib.MonthlySchedule, schedule.lib.Schedule.MonthReference, Program<'a>> as ssi ->                  
                 schedule.lib.Interpreters.interpretDataSourceInstruction interpret ssi
            | :? FileDataSourceInstruction<schedule.lib.MonthlySchedule, Program<'a>> as sfi ->
                 schedule.lib.Interpreters.interpretFileDataSourceInstruction interpret sfi
            
            | :? ExporterInstructions<Program<'a>> as ei  ->
                cra.lib.Interpreter.interpret interpret ei

            | _ ->
                failwith $"unknown instruction {instruction.GetType().Name}"
        | Stop p -> p

module Interpreter = 
    let interpret (p:ProgramResult) : int =
        p |> Interpreters.interpret
          |> Result.map (fun () -> 0)
          |> Result.defaultWith (fun err ->
            let code = ErrorHandlers.handleError err
            int code 
            )
