﻿module Helpers

open System
open System.Diagnostics
open Fake.Core

let initializeContext () =
    let execContext = Context.FakeExecutionContext.Create false "build.fsx" []
    Context.setExecutionContext (Context.RuntimeContext.Fake execContext)

let (</>) left right = System.IO.Path.Combine(left,right)

let (<?/>) left right =
    match left with
    | None -> None
    | Some left ->
        Some (left </> right)


module Proc =
    module Parallel =
        open System

        let locker = obj ()

        let colors =
            [| ConsoleColor.Blue
               ConsoleColor.Yellow
               ConsoleColor.Magenta
               ConsoleColor.Cyan
               ConsoleColor.DarkBlue
               ConsoleColor.DarkYellow
               ConsoleColor.DarkMagenta
               ConsoleColor.DarkCyan |]

        let print color (colored: string) (line: string) =
            lock locker (fun () ->
                let currentColor = Console.ForegroundColor
                Console.ForegroundColor <- color
                Console.Write colored
                Console.ForegroundColor <- currentColor
                Console.WriteLine line)

        let onStdout index name (line: string) =
            let color = colors.[index % colors.Length]

            if isNull line then
                print color $"{name}: --- END ---" ""
            else if String.isNotNullOrEmpty line then
                print color $"{name}: " line

        let onStderr name (line: string) =
            let color = ConsoleColor.Red

            if isNull line |> not then
                print color $"{name}: " line

        let redirect (index, (name, createProcess)) =
            createProcess
            |> CreateProcess.redirectOutputIfNotRedirected
            |> CreateProcess.withOutputEvents (onStdout index name) (onStderr name)

        let printStarting indexed =
            for index, (name, c: CreateProcess<_>) in indexed do
                let color = colors.[index % colors.Length]
                let wd = c.WorkingDirectory |> Option.defaultValue ""
                let exe = c.Command.Executable
                let args = c.Command.Arguments.ToStartInfo
                print color $"{name}: {wd}> {exe} {args}" ""

        let run cs =
            cs
            |> Seq.toArray
            |> Array.indexed
            |> fun x ->
                printStarting x
                x
            |> Array.map redirect
            |> Array.Parallel.map Proc.run

let createProcess exe arg dir =
    CreateProcess.fromRawCommandLine exe arg
    |> CreateProcess.withWorkingDirectory dir
    |> CreateProcess.ensureExitCode

let createProcessWithOutput exe arg dir =
    CreateProcess.fromRawCommandLine exe arg
    |> CreateProcess.withWorkingDirectory dir
    |> CreateProcess.redirectOutput
    |> CreateProcess.mapResult _.Output

let runElevatedProcess exe (arg:string) (dir:string) =
    let dirName = System.IO.Path.GetFileName dir
    
    Proc.Parallel.print ConsoleColor.Green $"./{dirName} \"{exe}\" {arg} (In: false, Out: false, Err: false, Elevated: true)" $"{dir}"
    
    let psi : ProcessStartInfo = ProcessStartInfo()
    psi.FileName <- exe
    psi.Arguments <- arg
    psi.WorkingDirectory <- dir
    psi.UseShellExecute <- true
    psi.Verb <- "runas"
    
    use p = new Process()
    p.StartInfo <- psi
    p.Start() |> ignore
    p.WaitForExit()
    
    
let runMklink (alias:string) (origin:string) (dir:string) =
    let alias = if alias.EndsWith ".exe" then alias else $"{alias}.exe"
    let origin = if origin.EndsWith ".exe" then origin else $"{origin}.exe"
    let alias = dir </> alias
    let origin = dir </> origin    
    let arg = $"/C mklink \"{alias}\" \"{origin}\""
    runElevatedProcess "cmd" arg dir

let ln (alias:string) (origin:string) dir =
    let alias = if alias.EndsWith ".exe" then alias.Replace(".exe","") else alias
    let arg = $"-s {origin} {alias}"
    createProcess "ln" arg dir


let dotnet = createProcess "dotnet"

let coverlet = createProcess "coverlet"

let git = createProcessWithOutput "git"

let run proc arg dir = proc arg dir |> Proc.run |> ignore

let runWithResult proc arg dir =
    let p = proc arg dir
    p |> Proc.run

let runParallel processes =
    processes |> Proc.Parallel.run |> ignore

let runOrDefault args =
    try
        match args |> List.ofArray with
        | [ target ] -> Target.runOrDefault target
        | target :: _ -> Target.runOrDefaultWithArguments target
        | _ -> Target.runOrDefault "Run"

        0
    with e ->
        printfn $"%A{e}"
        1


module Directory =
    let isEmpty path =
        let directories = System.IO.Directory.EnumerateDirectories(path) |> Seq.toList
        let files = System.IO.Directory.EnumerateFiles(path) |> Seq.toList
        match directories, files with
        | [], [] -> true
        | _ -> false
    
    let exists path =
        System.IO.Directory.Exists(path)