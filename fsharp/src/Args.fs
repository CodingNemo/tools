﻿module Args

open Fake.Core

let private getArg argName =
    // no idea why Target.getArguments() or ctx.Context.Arguments are empty
    let arguments = System.Environment.GetCommandLineArgs()

    arguments
    |> Array.tryFind (fun arg -> arg |> String.startsWith argName)
    |> Option.map (fun arg ->
        let split = arg |> String.split '='

        match split.Length with
        | 1 -> (split[0], None)
        | 2 -> (split[0], Some(split[1]))
        | _ -> failwith $"Too many = signs in the {argName} argument")

let private getArgValue =
    getArg >> Option.bind snd

let private getFlagArg argName =
    getArg argName
    |> Option.map (fun _ -> true)
    |> Option.defaultValue false



let project  = lazy getArgValue "-p"

let all = lazy getFlagArg "--all"
let force = lazy getFlagArg "--force"

let yes = lazy getFlagArg "--yes"

let skipTests  = lazy getFlagArg "--skip-tests"
let out = lazy getArgValue "--out"

let withTestReport   = lazy getArg "--with-test-report"
let withCoverageReport = lazy getArg "--with-coverage-report"