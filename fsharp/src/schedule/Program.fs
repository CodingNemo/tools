﻿open schedule
open schedule.Commands
open backbone.cli.Cli.App


[<EntryPoint>]
let main args =
    app args {
        interpreter Interpreter.interpret

        defaultCmd Show.run 

        cmd "show" Show.run
        cmd "generate" Generate.run ["gen"]
        
        cmd "set" Set.run
    }