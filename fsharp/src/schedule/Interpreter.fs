﻿namespace schedule

open schedule.lib

module Interpreter =
    
    let interpret (pgm:backbone.ProgramResult) : int =
        pgm |> Interpreters.interpret
          |> Result.map (fun () -> 0)
          |> Result.defaultWith (fun err ->
              let code = ErrorHandlers.handleError err
              int code)
