﻿namespace schedule.Commands

open Spectre.Console
open backbone.DataSource
open backbone.DataSource.Instructions
open backbone.Program
open backbone.Dates

open Spectre.Console.Cli
open clients.lib
open schedule.lib
open schedule.lib.Schedule


module Set = 
    
    type Settings() =
        inherit CommandSettings()

        [<CommandOption("--schedule-location", IsHidden = true)>]
        member val ScheduleLocation : string = "" with get, set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
        
        [<CommandArgument(0,"<TARGET>")>]
        member val TargetActivity : string = "" with get, set
        
        [<CommandArgument(1,"<DATE>")>]
        member val Date : string = "" with get, set
        
        [<CommandArgument(2,"[ACTIVITY-SPAN]")>]
        member val ActivitySpan : string = "F" with get, set
        
        [<CommandOption("-w|--show-weekends")>]
        member val ShowWeekends : bool = false with get, set
    
    let private loadClients (settings:Settings) =
        resPgm {
            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            return! clientsSource.Read()
        }
    
    let run (settings:Settings) =
        resPgm {
            let! date = AsOfDate.Parse(settings.Date)
                        |> lift
            
            let! activitySpan = ActivitySpan.Parse(settings.ActivitySpan) |> lift
            
            let! target = SetActivityTarget.Parse(settings.TargetActivity) |> lift
            
            let! scheduleLocation = DataSource.Location.TryParse settings.ScheduleLocation
            
            let reference = Schedule.Reference.CurrentMonth(date)
            
            AnsiConsole.Write(FigletText(reference.Display()))
            
            let! schedule = Schedule.Query(reference, scheduleLocation)
            let! clients = loadClients settings
            
            let! newSchedule = schedule.Set(date, target, activitySpan, clients)
                                        |> lift
            
            do! newSchedule.StoreIn(scheduleLocation)
            
            let drawSettings =  { Show.RenderSettings.Default with showWeekends = settings.ShowWeekends; highlighted = Some date.date }

            newSchedule |> Show.displaySchedule <|| (drawSettings, clients)
        }