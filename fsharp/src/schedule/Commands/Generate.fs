﻿namespace schedule.Commands

open Spectre.Console
open Spectre.Console.Cli

open backbone.DataSource
open backbone.DataSource.Instructions

open backbone.Dates
open backbone.Program
open clients.lib
open schedule.lib
open schedule.lib.Schedule

module Generate = 
    
    type Settings() =
        inherit CommandSettings()
    
        [<CommandOption("--schedule-location", IsHidden = true)>]
        member val ScheduleLocation : string = "" with get, set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
        
        [<CommandOption("--as-of", IsHidden = true)>]
        member val AsOf : string = "" with get, set
        
        [<CommandOption("-w|--show-weekends")>]
        member val ShowWeekends : bool = false with get, set
        
        [<CommandArgument(0,"[REFERENCE]")>]
        member val ScheduleReference : string = "" with get, set 
    
    let private loadClients (settings:Settings) =
        resPgm {
            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            return! clientsSource.Read()
        }
    
    let run (settings:Settings) =
        resPgm {
            let! asOfDate = AsOfDate.ParseOrDefault settings.AsOf |> lift
            let! clients = loadClients settings
            let! scheduleLocation = DataSource.Location.TryParse settings.ScheduleLocation
            
            let reference = Reference.ParseOrDefault(settings.ScheduleReference, asOfDate)
            
            AnsiConsole.Write(FigletText(reference.Display()))
            
            let! schedule = Schedule.Generate(reference, clients, scheduleLocation)
            
            let drawSettings =  { Show.RenderSettings.Default with showWeekends = settings.ShowWeekends }
               
            schedule |> Show.displaySchedule <|| (drawSettings, clients) 
        }