﻿namespace schedule.Commands

open Spectre.Console
open Spectre.Console.Cli

open Spectre.Console.Rendering
open backbone
open backbone.DataSource
open backbone.DataSource.Instructions
open backbone.Dates
open backbone.Dates.Date
open backbone.Program

open clients.lib
open schedule.lib
open schedule.lib.Schedule

module Show =
    
    type Settings() =
        inherit CommandSettings()
        
        [<CommandOption("--schedule-location", IsHidden = true)>]
        member val ScheduleLocation : string = "" with get, set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set

        [<CommandOption("--as-of", IsHidden = true)>]
        member val AsOf : string = "" with get, set
        
        [<CommandOption("-w|--weekends")>]
        member val ShowWeekends : bool = false with get, set
                
        [<CommandArgument(0,"[REFERENCE]")>]
        member val ScheduleReference : string = "" with get, set 

        [<CommandOption("--no-wait")>]
        member val NoWait : bool = false with get,set
    
    type RenderSettings = {
        showWeekends:bool
        highlighted:Date option
        legend: bool
    }
        with
            static member Default = {showWeekends = false; highlighted = None; legend = true}
      
    
    let private loadClients (settings:Settings) =
        resPgm {
            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            return! clientsSource.Read()
        }
        
    module Chars =
        let fullDay =  'X'
        let morningOnly = 'm'
        
        let afternoonOnly = 'a'

        let dayOfWeek dow = 
            match dow with
            | Monday    -> 'L'
            | Tuesday   -> 'M'
            | Wednesday -> 'M'
            | Thursday  -> 'J'
            | Friday    -> 'V'
            | Saturday  -> 'S'
            | Sunday    -> 'D'
    
    let displayMonth (drawSettings:RenderSettings) (clients:Clients) (monthlySchedule:MonthlySchedule)  =
        let addHeader (days:Date list) (table:Table) =
            let month = monthlySchedule.Reference.month
            let year = monthlySchedule.Reference.year
            
            table.AddColumn($"{month.ToLongString()} {year}") |> ignore
        
            days
            |> Seq.filter (fun date -> drawSettings.showWeekends || not date.IsWeekend)
            |> Seq.iter (fun date ->
                  let mutable dayStyle = []
                  if date.IsWeekend then
                      dayStyle <- dayStyle @ ["gray"]
                  if Some date = drawSettings.highlighted then
                      dayStyle <- dayStyle @ ["bold"]
                  let day = $"{Chars.dayOfWeek date.DayOfWeek}{date.day.ToString()}"
                  
                  match dayStyle with
                  | [] ->
                      table.AddColumn(day) |> ignore
                  | _ ->
                      let style = dayStyle |> String.concat " "
                      let column = TableColumn(Markup($"[{style}]{day}[/]"))
                      table.AddColumn(column) |> ignore
            )
            
        let buildActivity color highlight activity =
            let style = $"{color}" + if highlight then " bold" else ""
            
            let text = match activity with
                            | Some ActivitySpan.FullDay -> $"{Chars.fullDay}"
                            | Some ActivitySpan.MorningOnly -> $"{Chars.morningOnly}"
                            | Some ActivitySpan.AfternoonOnly -> $"{Chars.afternoonOnly}"
                            | None -> " "
            
            Markup($"[{style}]{Markup.Escape(text)}[/]") :> IRenderable
            
        let buildActivities (color:string) (activities: ActivityByDate list) =
            activities
             |> List.filter (fun (_, date) -> drawSettings.showWeekends || not date.IsWeekend)
             |> List.sortBy (fun (_, date) -> date.day.number)
             |> List.map (fun (activity, date) ->
                 let highlight = drawSettings.highlighted = Some date
                 if date.IsWeekend then
                    buildActivity "gray" highlight activity
                 else
                    buildActivity color highlight activity
                )
             
             
        let limitTextLength limit (text:string) =
            if text.Length >= limit then 
                $"{text.Substring(0, limit-1)}…"
            else
                text

        let pickColor (i:int) (colors:string list)=
            let index = abs i % colors.Length
            let color = colors[index]
            color

        let buildMissionRow (missionId:MissionId) (activities: ActivityByDate list) : IRenderable list =
            let colors = ["deeppink4_1"; "darkmagenta"; "darkmagenta_1"; "darkviolet"; "purple_1"; "plum4"; "mediumpurple3"; "mediumpurple3_1"; "slateblue1"]
            let letter = missionId.clientSlug.value[0] |> int
            let color = pickColor letter colors

            let dayColumns = activities |> buildActivities color

            let client = clients.FindBySlug missionId.clientSlug
                         |> List.head
                       
            let clientName = client.Name.ToString()
            
            let mission = client.GetMission(missionId)
            
            let missionLabel = mission.title
                               |> Option.map _.ToString()
                               |> Option.defaultValue $"#{missionId.index}"
                               
            let clientName = clientName.ToString() |> limitTextLength 7
            let missionLabel = missionLabel |> limitTextLength 7
                    
            Markup($"[{color}]{clientName} ({missionLabel})[/]") :> IRenderable::dayColumns
        
        let cantorPairing day month =
            let a = day - 1
            let b = month - 1
            let sum = a + b
            (sum * (sum + 1)) / 2 + b
        
        
        let buildHolidayRow (days: Date list) (holiday:Holiday)  : IRenderable list =
            let colors = ["blue"; "teal"; "deepskyblue3"; "deepskyblue3_1"; "dodgerblue1"; "deepskyblue4_2"; "deepskyblue4_1"; "dodgerblue2"; "dodgerblue3"; "deepskyblue3"; "steelblue3"; "steelblue"]
            let i = cantorPairing holiday.date.day.number (holiday.date.month.ToInt())
            let color = pickColor i colors

            let dayColumns = days
                             |> List.filter (fun day -> drawSettings.showWeekends || not day.IsWeekend)
                             |> List.map (fun day ->
                                if day = holiday.date then
                                    Markup($"[blue]{Chars.fullDay}[/]") :> IRenderable
                                else
                                    Markup(" ") :> IRenderable
                            )
                            
            Markup($"[{color}]{holiday.name}[/]") :> IRenderable::dayColumns
            
        let buildOffRow (activities: ActivityByDate list) =
            let dayColumns = activities |> buildActivities "green"   
            Markup("[green]Off[/]") :> IRenderable::dayColumns
        
        let addActivityRow (days:Date list) (table:Table) (activity:Activity) =
            match activity with
            | Holiday holiday when drawSettings.showWeekends || not holiday.date.IsWeekend ->
                holiday
                |> buildHolidayRow days 
                |> table.AddRow
                |> ignore
                
            | Mission (missionId, activities) ->
                buildMissionRow missionId activities
                |> table.AddRow
                |> ignore
            | Off activities ->
                buildOffRow activities
                |> table.AddRow
                |> ignore
            | _ -> ()

        let month, year = monthlySchedule.Reference.month, monthlySchedule.Reference.year
        let days = month.Days(year) |> List.map (fun day -> date (day,month,year))
            
        match monthlySchedule.Content with
        | NotGenerated ->
            let panel = Panel(Markup($"[red]{monthlySchedule.Reference.Display()}[/] has not been generated yet"))
            AnsiConsole.Write(panel)
        | Generated activities ->            
            let table = Table()
            table |> addHeader days
            activities |> Seq.iter (addActivityRow days table)
            
            AnsiConsole.Write(table)

    let displayQuarter drawSettings (clients:Clients) (quarterlySchedule:QuarterlySchedule)  =
        quarterlySchedule.Months |> List.iter (fun month ->
            displayMonth  drawSettings clients month
            AnsiConsole.WriteLine("")
        )
    
    let displayYear (drawSettings: RenderSettings) (clients:Clients) (yearlySchedule:YearlySchedule)  =
        yearlySchedule.Quarters |> List.iter (fun quarter ->
            displayQuarter drawSettings clients quarter
            AnsiConsole.WriteLine("---")
        )

    let displayLegend () =
        let rows = [
                        $"{Chars.morningOnly} : morning only"
                        $"{Chars.afternoonOnly} : afternoon only"
                        $"{Chars.fullDay} : full day"
                    ] |> List.map (fun item -> Markup(item) :> IRenderable)
                      |> List.toArray
                
        AnsiConsole.Write(Rows(rows) :> IRenderable) 
    
    let displaySchedule (schedule:Schedule) (drawSettings: RenderSettings) (clients:Clients)=
        match schedule with
        | Yearly year -> year |> displayYear drawSettings clients
        | Quarterly quarter -> quarter |>displayQuarter drawSettings clients
        | Monthly month -> month |> displayMonth drawSettings clients
        if drawSettings.legend then 
            displayLegend()
    
    let run (settings:Settings) : ProgramResult =
        resPgm {
            let! asOfDate = AsOfDate.ParseOrDefault settings.AsOf |> lift
            let reference = Reference.ParseOrDefault(settings.ScheduleReference, asOfDate)
            AnsiConsole.Write(FigletText(reference.Display()))

            let! scheduleLocation = DataSource.Location.TryParse settings.ScheduleLocation
            let! schedule = Schedule.Query(reference, scheduleLocation)
            
            let! clients = loadClients settings
            
            let drawSettings =  { RenderSettings.Default with showWeekends = settings.ShowWeekends } 
            schedule |> displaySchedule <|| (drawSettings, clients)
            
            if not(settings.NoWait) then
                Wait.anyKey()
        }