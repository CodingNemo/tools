﻿namespace backbone 

module Wait = 

    open Spectre.Console

    let anyKey () = 
        AnsiConsole.MarkupLine("[gray]Press any key...[/]")
        System.Console.ReadKey() |> ignore