﻿namespace backbone

module Option =
    
    type OptionBuilder() =
        member _.Return(x) = Some x
        member _.ReturnFrom(x) = x
        member _.Bind(x,f) = Option.bind f x
        member _.Zero() = None
        
    let opt = OptionBuilder()

