﻿namespace backbone

open backbone

module Errors = 
    
    type ArgError =
    | InvalidArg of string * string option
        interface IError
