﻿namespace backbone

type IError = interface end

type IInstruction<'a> =
  abstract member Map : ('a->'b) -> IInstruction<'b>


type Program<'a> =
  | Instruction of IInstruction<Program<'a>>
  | Stop of 'a

type ProgramResult = Program<Result<unit,IError>>
    
module Program =
    
    let lift x = Stop x
    
    let rec private bind f program =
        match program with
        | Instruction inst ->
            inst.Map (bind f) |> Instruction
        | Stop x ->
            f x

    type ProgramBuilder() =
        
        member _.Return(x) = Stop x
        member _.ReturnFrom(x) = x
        member _.Bind(x,f) = bind f x
        member _.Zero() = Stop ()

    let pgm = ProgramBuilder()
    
    type ResultProgramBuilder() =
        member _.Return(x) = Stop (Ok x)
        member _.ReturnFrom(x) = x
        
        member _.Delay(f) = f
        
        member _.Run(f) = f()
        
        member _.Bind(result: Program<Result<'a, 'err>>, f: 'a -> Program<Result<'b, IError>>) : Program<Result<'b, IError>> =
            result
            |>  bind (fun result ->
                match result with
                | Ok value -> f value
                | Error err -> Stop (Error (err :> IError)))
         
        member _.Zero() = Stop (Ok ())
        
        member this.While(guard, body) =
            if not (guard()) 
            then this.Zero() 
            else
                let while' () = this.While(guard, body)
                this.Bind(body(), while')  

        member this.TryWith(body, handler) =
            try this.ReturnFrom(body())
            with e -> handler e

        member this.TryFinally(body, compensation) =
            try this.ReturnFrom(body())
            finally compensation() 

        member this.Using(disposable:#System.IDisposable, body) =
            let body' = fun () -> body disposable
            this.TryFinally(body', fun () -> 
                match disposable with 
                    | null -> () 
                    | disposable -> disposable.Dispose())

        member this.For(sequence:seq<_>, body) =
            this.Using(sequence.GetEnumerator(),fun enum -> 
                this.While(enum.MoveNext, 
                    this.Delay(fun () -> body enum.Current)))
            
        member this.Combine (a,b) = 
            this.Bind(a, fun () -> b())
        
    let resPgm = ResultProgramBuilder()