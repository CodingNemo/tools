﻿namespace backbone.IO

open backbone

type FileAccessError =
    | FolderNotFound of RootedFolderPath
    | AccessDenied of FilePath
    | InvalidPath of FilePath * reason:string
    interface IError
    
type FolderAccessError =
    | AccessDenied of RootedFolderPath
    | InvalidPath of RootedFolderPath * reason:string
    interface IError

type FileCreationError =
    | FileAlreadyExists of FilePath
    | FileAccess of FileAccessError
    interface IError

type FolderCreationError =
    | FolderAlreadyExists of RootedFolderPath
    | ParentFolderNotFound of RootedFolderPath
    | FolderAccess of FolderAccessError
    interface IError

type FolderDeletionError =
    | FolderNotFound of RootedFolderPath
    | FolderAccess of FolderAccessError
    interface IError
    
type FolderArchiveError =
    | FolderNotFound of RootedFolderPath * string
    | ParentFolderNotFound of RootedFolderPath
    | FolderAccess of FolderAccessError
    interface IError

type RootedFolderPathParsingError =
    | PathIsNotRooted of string
    | InvalidRoot of string
    interface IError

type RelativeFolderPathParsingError =
    | PathIsRooted of string
    interface IError

type FilePathParsingError =
    | FolderPath of RootedFolderPathParsingError
    interface IError
