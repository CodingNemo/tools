﻿namespace backbone.IO

open backbone.IO.Instructions
open backbone.Result

module Interpreter =
    open System
    open System.IO
        
    let parseRootedFolder (folderPath:string) : Result<RootedFolderPath, RootedFolderPathParsingError> =
        let rec parseFolderPath (folderPath:string) =
            let parentPath = Path.GetDirectoryName(folderPath)
            
            if String.IsNullOrEmpty(parentPath) then
                match folderPath with
                | "/" -> Ok(Root { letter = None })
                | s when s.EndsWith(@":\") ->
                    let letter = s[0]
                    Ok(Root { letter = Some letter })
                | _ ->
                    Error (InvalidRoot folderPath)
            else
                res {
                    let folderName = Path.GetFileName(folderPath)
                    let! parent = parseFolderPath parentPath
                    return Folder {
                        parent = parent
                        name = folderName
                    }    
                }

        if Path.IsPathRooted(folderPath) then
            let folderPath = parseFolderPath folderPath
            folderPath
        else
            Error (PathIsNotRooted folderPath)
    
    let parseRelativeFolder (folderPath:string) : Result<RelativeFolderPath, RelativeFolderPathParsingError> =
        let rec parseFolderPath (folderPath:string) : RelativeFolderPath =
            let parentPath = Path.GetDirectoryName(folderPath)
            let folderName = Path.GetFileName(folderPath)
            
            let parent = if String.IsNullOrEmpty(parentPath) then
                            None
                         else
                            Some (parseFolderPath parentPath)
                 
            {
                parent = parent
                name = folderName
            }    

        if Path.IsPathRooted(folderPath) then
            Error (PathIsRooted folderPath)
        else
            Ok (parseFolderPath folderPath)
    
    let parseFilePath (path:string) : Result<FilePath, FilePathParsingError> =
        res {
            let folder = Path.GetDirectoryName path
            let! rootedFolder = parseRootedFolder folder |> Result.mapError FilePathParsingError.FolderPath
            let fileName = FileName.Parse path
            
            return {
                Name = fileName
                Folder = rootedFolder
            }
        }
        
        
    let private createFolder (folderPath:RootedFolderPath) : Result<unit, FolderCreationError> =
        try
            Directory.CreateDirectory(folderPath.ToString()) |> ignore
            Ok()
        with
        | :? UnauthorizedAccessException ->
            Error(FolderCreationError.FolderAccess (FolderAccessError.AccessDenied folderPath))
        | :? ArgumentException as ex ->
            Error(FolderCreationError.FolderAccess (FolderAccessError.InvalidPath (folderPath, ex.Message)))
        | :? PathTooLongException as ex ->
            Error(FolderCreationError.FolderAccess (FolderAccessError.InvalidPath (folderPath, $"Path too long: {ex.Message}")))
        | :? NotSupportedException ->
            Error(FolderCreationError.FolderAccess (FolderAccessError.InvalidPath (folderPath, "Unsupported path type")))        
        | :? DirectoryNotFoundException ->
            Error(FolderCreationError.ParentFolderNotFound folderPath)
    
    let private deleteFolder (folderPath:RootedFolderPath) : Result<unit, FolderDeletionError> =
        try
            Directory.Delete(folderPath.ToString())
            Ok()
        with
        | :? UnauthorizedAccessException ->
            Error(FolderDeletionError.FolderAccess (FolderAccessError.AccessDenied folderPath))
        | :? ArgumentException as ex ->
            Error(FolderDeletionError.FolderAccess (FolderAccessError.InvalidPath (folderPath, ex.Message)))
        | :? PathTooLongException as ex ->
            Error(FolderDeletionError.FolderAccess (FolderAccessError.InvalidPath (folderPath, $"Path too long: {ex.Message}")))
        | :? NotSupportedException ->
            Error(FolderDeletionError.FolderAccess (FolderAccessError.InvalidPath (folderPath, "Unsupported path type")))        
        | :? DirectoryNotFoundException ->
            Error(FolderDeletionError.FolderNotFound folderPath)
     
    let private archiveFolder (folderPath:RootedFolderPath) : Result<unit, FolderArchiveError> =
        try
            match folderPath with
            | Folder folderPath -> 
                let archiveFileName = FileName.Parse $"{folderPath.name}.zip"
                let archiveFilePath = folderPath.parent / archiveFileName
                System.IO.Compression.ZipFile.CreateFromDirectory(folderPath.ToString(), archiveFilePath.ToString())
                Ok()
            | Root _ ->
                Error(FolderArchiveError.ParentFolderNotFound folderPath)
        with
        | :? UnauthorizedAccessException ->
            Error(FolderArchiveError.FolderAccess (FolderAccessError.AccessDenied folderPath))
        | :? ArgumentException as ex ->
            Error(FolderArchiveError.FolderAccess (FolderAccessError.InvalidPath (folderPath, ex.Message)))
        | :? PathTooLongException as ex ->
            Error(FolderArchiveError.FolderAccess (FolderAccessError.InvalidPath (folderPath, $"Path too long: {ex.Message}")))
        | :? NotSupportedException ->
            Error(FolderArchiveError.FolderAccess (FolderAccessError.InvalidPath (folderPath, "Unsupported path type")))        
        | :? DirectoryNotFoundException as ex ->
            Error(FolderArchiveError.FolderNotFound (folderPath,ex.Message))
    
    let folderExists folderPath =
        Directory.Exists(folderPath.ToString())
    
    let interpretFolderInstruction interpret =
        function
        | RootedFolderInstruction.Exists (folderPath, next) ->
            let exists = folderExists folderPath
            interpret (next exists)
        | RootedFolderInstruction.Create (folderPath, next) ->
            let folderCreation = createFolder(folderPath)
            interpret (next folderCreation)
        | RootedFolderInstruction.Delete (folderPath, next) ->
            let folderDeletion = deleteFolder(folderPath)
            interpret (next folderDeletion)
        | RootedFolderInstruction.Archive (folderPath, next) ->
            let folderArchive = archiveFolder(folderPath)
            interpret (next folderArchive)

    let fileExits filePath =
        File.Exists(filePath.ToString())
    
    let interpretFileInstruction interpret =
        function
        | FileInstruction.Exists (filePath, next) ->
            let exists = fileExits filePath
            interpret (next exists)
        