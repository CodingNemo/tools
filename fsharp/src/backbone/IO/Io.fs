﻿namespace backbone.IO

open System.Diagnostics
open System.IO
open backbone.Result

type FolderName = string

type RootName = char

[<DebuggerDisplay("{this.ToString()}")>]
type FolderPath<'parent> = {
    parent: 'parent
    name: FolderName
} with
    override this.ToString() =
        Path.Combine(this.parent.ToString(), this.name.ToString())

[<DebuggerDisplay("{this.ToString()}")>]    
type RootPath = {
    letter:RootName option
} with
    override this.ToString() =
        match this.letter with
        | Some letter -> $"{letter}:{Path.DirectorySeparatorChar}"
        | None -> $"{Path.DirectorySeparatorChar}"

[<DebuggerDisplay("{this.ToString()}")>]
type RootedFolderPath =
| Folder of FolderPath<RootedFolderPath>
| Root of RootPath
    with
       override this.ToString() =
            match this with
            | Folder folder ->
                folder.ToString()
            | Root root ->
                root.ToString()
                

[<DebuggerDisplay("{this.ToString()}")>]
type RelativeFolderPath = {
    parent: RelativeFolderPath option
    name: FolderName
}   
    with       
        member this.ToString(rootedFolderPath: RootedFolderPath) =
            match this.parent with
            | Some parent ->
                Path.Combine(parent.ToString(rootedFolderPath), this.name)
            | None ->
                Path.Combine(rootedFolderPath.ToString(), this.name)

type FileName = {
    Name: string
    Extension: string option
}
    with
        static member Parse(path:string) =
            let extension = Path.GetExtension path
            let fileName = Path.GetFileNameWithoutExtension path
            {
                Name = fileName
                Extension = if System.String.IsNullOrEmpty extension then None else Some extension
            }
            
        override this.ToString() =
            match this.Extension with
            | Some ext ->
                let ext = if ext.StartsWith "." then ext else $".{ext}"
                $"{this.Name}{ext}"
            | None ->
                $"{this.Name}"
                
[<DebuggerDisplay("{this.ToString()}")>]
type FilePath = {
    Folder:RootedFolderPath
    Name:FileName
}
    with           
        override this.ToString() =
            Path.Combine(this.Folder.ToString(), this.Name.ToString())

type RelativeFolderPath with
   
    static member (/) (folder:RelativeFolderPath, folderName:FolderName) : RelativeFolderPath =
        {
            parent = Some folder
            name = folderName
        }
        
    static member (/) (leftFolder:RelativeFolderPath, rightFolder:RelativeFolderPath) : RelativeFolderPath =
        match rightFolder.parent with
        | None  ->
            {
                parent = Some leftFolder
                name = rightFolder.name
            }
        | Some parent ->
            {
                parent = Some (leftFolder / parent)
                name = rightFolder.name
            }

type RootedFolderPath with
    static member (/) (folder:RootedFolderPath, fileName:FileName) : FilePath =
        {
            Folder = folder
            Name = fileName
        }
   
    static member (/) (folder:RootedFolderPath, folderName:FolderName) : RootedFolderPath =
        Folder {
            parent = folder
            name = folderName
        }
        
    static member (/) (leftFolder:RootedFolderPath, rightFolder:RelativeFolderPath) : RootedFolderPath =
        match rightFolder.parent with
        | None  ->
            Folder {
                parent = leftFolder
                name = rightFolder.name
            }
        | Some parent ->
            Folder {
                parent = (leftFolder / parent)
                name = rightFolder.name
            }
            
//type RootFolder = {
//    path:RootedFolderPath
//}