﻿module backbone.IO.Instructions

open backbone
open backbone.Program
open backbone.IO

type FileInstruction<'a> =
    | Exists of FilePath * next:(bool -> 'a)
    interface IInstruction<'a> with
        member this.Map f =
            match this with
            | Exists (filePath, next) ->
                Exists (filePath, next >> f)

type FilePathInstruction<'a> =
    | Parse of string * next:(Result<FilePath, FilePathParsingError> -> 'a)
    | TryParse of string * next:(Result<FilePath option, FilePathParsingError> -> 'a)
    | TryFetchFromEnvVar of string * next:(Result<FilePath, FilePathParsingError> -> 'a)
    interface IInstruction<'a> with
        member this.Map f =
            match this with
            | Parse (path, next) ->
                Parse (path, next >> f)
            | TryParse (path, next) ->
                TryParse (path, next >> f)
            | TryFetchFromEnvVar (envVarName, next) ->
                TryFetchFromEnvVar (envVarName, next >> f)
                
type FilePath with

    static member Parse(path:string) =
        Instruction (FilePathInstruction.Parse(path, Stop))
    
    static member TryParse(path:string) =
        Instruction (FilePathInstruction.TryParse(path, Stop))
    
    static member TryFromEnvVar(envVarName:string) =
        Instruction (FilePathInstruction.TryFetchFromEnvVar(envVarName, Stop))
    
    member this.Exists() : Program<bool> =
        Instruction (FileInstruction.Exists (this, Stop))

type RootedFolderPathInstruction<'a> =
    | FetchFromEnvVar of string * next:(Result<RootedFolderPath, RootedFolderPathParsingError> -> 'a)
    | TryFetchFromEnvVar of string * next:(Result<RootedFolderPath option, RootedFolderPathParsingError> -> 'a)
    | Parse of string * next:(Result<RootedFolderPath, RootedFolderPathParsingError> -> 'a)
    | TryParse of string * next:(Result<RootedFolderPath option, RootedFolderPathParsingError> -> 'a)
    | Current of  next:(RootedFolderPath -> 'a)
    interface IInstruction<'a> with
        member this.Map f =
            match this with
            | FetchFromEnvVar (path, next) ->
                FetchFromEnvVar (path, next >> f)
            | TryFetchFromEnvVar (path, next) ->
                TryFetchFromEnvVar (path, next >> f)
            | Parse (path, next) ->
                Parse (path, next >> f)
            | TryParse (path, next) ->
                TryParse (path, next >> f)
            | Current next ->
                Current (next >> f)

type RootedFolderInstruction<'a> =
    | Exists of RootedFolderPath * next:(bool -> 'a)
    | Create of RootedFolderPath * next:(Result<unit, FolderCreationError> -> 'a)
    | Archive of RootedFolderPath * next:(Result<unit, FolderArchiveError> -> 'a)
    | Delete of RootedFolderPath * next:(Result<unit, FolderDeletionError> -> 'a)
    interface IInstruction<'a> with
        member this.Map f =
            match this with
            | Exists (folderPath, next) ->
                Exists (folderPath, next >> f)
            | Create (folderPath, next) ->
                Create (folderPath, next >> f)
            | Archive (folderPath, next) ->
                Archive (folderPath, next >> f)
            | Delete (folderPath, next) ->
                Delete (folderPath, next >> f)

type RootedFolderPath with

    static member Parse(path: string) =
        Instruction (RootedFolderPathInstruction.Parse (path, Stop))
        
    static member TryParse(path: string) =
        Instruction (RootedFolderPathInstruction.TryParse (path, Stop))
    
    static member FromEnvVar(path: string) =
        Instruction (RootedFolderPathInstruction.FetchFromEnvVar (path, Stop))
        
    static member TryFromEnvVar(path: string) =
        Instruction (RootedFolderPathInstruction.TryFetchFromEnvVar (path, Stop))
    
    static member Current =
        Instruction (RootedFolderPathInstruction.Current Stop)
        
    member this.Exists() : Program<bool> =
        Instruction (RootedFolderInstruction.Exists (this, Stop))

    member this.Create() : Program<Result<unit, FolderCreationError>> =
        Instruction (RootedFolderInstruction.Create (this, Stop))
        
    member this.CreateIfNotExist() : Program<Result<unit, FolderCreationError>> =
        pgm {
            let! exists = this.Exists()
            if exists then
                return Ok ()
            else
                return! this.Create()
        }
    
    member this.Archive() : Program<Result<unit, FolderArchiveError>> =
        Instruction (RootedFolderInstruction.Archive (this, Stop))
       
    member this.Delete() : Program<Result<unit, FolderDeletionError>> =
        Instruction (RootedFolderInstruction.Delete (this, Stop))

//type RootFolderInstruction<'a> =
//    | Get of RootedFolderPath option * next:(RootFolder -> 'a)
//    interface IInstruction<'a> with
//        member this.Map f =
//            match this with
//            | Get (folder, next) ->
//                Get (folder, next >> f)
       
//type RootFolder with
//    static member Get(folder: RootedFolderPath option) : Program<RootFolder> =
//        Instruction (RootFolderInstruction.Get (folder, Stop))
    