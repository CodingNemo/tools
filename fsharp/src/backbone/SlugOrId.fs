﻿namespace backbone

open backbone.Errors
open backbone.Result

type SlugOrId<'id> =
| Slug of Slug
| Id of 'id
    with
        override this.ToString() =
            match this with
            | Id id -> $"id '{id.ToString()}'"
            | Slug slug -> $"slug '{slug.ToString()}'"



module SlugOrId =

    let inline parse<
            'a when ^a : (
                static member Parse : string -> Result<^a, ArgError>
            )>
            (text:string) : Result<SlugOrId<'a>, ArgError> =
        text
        |>'a.Parse 
        |> Result.map SlugOrId.Id
        |> bindError (fun _ ->
            Slug.Parse text
            |> Result.map Slug
            )
        
    let inline tryParse<'a when ^a : (
                static member Parse : string -> Result<^a, ArgError>
            )> = parse<'a> >> Result.toOption
        