﻿namespace backbone

open System


module Result =
    
    let bindError (f: 'err -> Result<'a, 'newErr>) (result: Result<'a, 'err>) : Result<'a, 'newErr> =
        match result with
        | Ok x -> Ok x
        | Error e -> f e
    
    let value (result: Result<'a, 'err>) : 'a =
        match result with
        | Ok x -> x
        | Error e -> failwith $"Result is Error : {Environment.NewLine}{e}"
    
    let error (result: Result<'a, 'err>) : 'err =
        match result with
        | Ok x -> failwith $"Result is Ok : {Environment.NewLine}{x}"
        | Error e -> e
        
    let mapList (aggregateErrors: 'err seq -> 'aggregatedError) (results: seq<Result<'a, 'err>>) : Result<'a list, 'aggregatedError> =
        let oks, errors =
            Seq.fold (fun (oks, errors) result ->
                match result with
                | Ok v    -> (v :: oks, errors)
                | Error e -> (oks, e :: errors)
            ) ([], []) results
        if List.isEmpty errors then
            Ok (List.rev oks)
        else
            Error (aggregateErrors errors)
 
    
    type ResultBuilder() =
        member _.Return(x) = Ok x
        member _.ReturnFrom x = x
        member _.Yield(x) = Ok x
        member _.Bind (x,f) = Result.bind f x
        member this.Zero() = this.Return()
        member _.Delay(f) = f
        member _.Run(f) = f()
        member this.While(guard, body) =
            if not (guard()) 
            then this.Zero() 
            else
                let while' () = this.While(guard, body)
                this.Bind(body(), while')  

        member this.TryWith(body, handler) =
            try this.ReturnFrom(body())
            with e -> handler e

        member this.TryFinally(body, compensation) =
            try this.ReturnFrom(body())
            finally compensation() 

        member this.Using(disposable:#IDisposable, body) =
            let body' = fun () -> body disposable
            this.TryFinally(body', fun () -> 
                match disposable with 
                    | null -> () 
                    | disposable -> disposable.Dispose())

        member this.For(sequence:seq<_>, body) =
            this.Using(sequence.GetEnumerator(),fun enum -> 
                this.While(enum.MoveNext, 
                    this.Delay(fun () -> body enum.Current)))

        member this.Combine (a,b) = 
            this.Bind(a, fun () -> b())

    let res = ResultBuilder()