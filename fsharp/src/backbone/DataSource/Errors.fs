﻿namespace backbone.DataSource.Errors

open backbone
open backbone.IO

type InvalidFileContent = FilePath * string

type FileSavingError =
    | FileCreation of FileCreationError
    | FileAccess of FileAccessError
    | FileNotFound of FilePath
    | InvalidFileContent of InvalidFileContent
    interface IError
    
type FileParsingError =
    | FileSave of FileSavingError
    | FileAccess of FileAccessError
    | InvalidFileContent of InvalidFileContent
    interface IError


type DataSourceLocationParsingError =
    | InvalidLocation of string
    | FilePath of FilePathParsingError
    | FolderPath of RootedFolderPathParsingError
    interface IError
            
type DataSourceLoadingError =
    | Location of DataSourceLocationParsingError
    interface IError
