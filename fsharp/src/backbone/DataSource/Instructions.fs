﻿namespace backbone.DataSource

open backbone
open backbone.Dates
open backbone.Program
open backbone.DataSource.Errors
open backbone.IO

module Instructions = 
    
    type FileDataSourceInstruction<'content, 'next> =
    | Parse of FilePath * next:(Result<'content, FileParsingError> -> 'next)    
    | Save of (FilePath * 'content) * next:(Result<unit, FileSavingError> -> 'next)
        interface IInstruction<'next> with
            member this.Map<'b>(f:'next -> 'b) : IInstruction<'b> =
                match this with
                | Parse (filePath, next) ->
                    Parse (filePath, next >> f)
                | Save ((filePath, data), next) ->
                    Save ((filePath, data), next >> f)
    
    type DataSourceInstruction<'data, 'query, 'next> =
        | Find of (DataSource.Location option * 'query) * next:(Result<DataSource<'data, 'query>, DataSourceLoadingError> -> 'next)
        interface IInstruction<'next> with
            member this.Map f =
                match this with
                | Find (p,next) ->
                    Find (p, next >> f)
    
    type DataSourceInstruction<'data, 'next> = DataSourceInstruction<'data, unit, 'next>
        
    type DataSourceLocationInstruction<'next> =
        | TryParse of string * next:(Result<DataSource.Location option, DataSourceLocationParsingError> -> 'next)
        interface IInstruction<'next> with
            member this.Map f=
                match this with
                | TryParse (text, next) ->
                    TryParse (text, next >> f)
                    
    type DataSource.Location with
        static member TryParse(text:string) =
            Instruction (DataSourceLocationInstruction.TryParse (text, Stop))
            
    type DataSource.File<'data> with
        member this.Save (data:'data) =
            Instruction (FileDataSourceInstruction.Save ((this.path, data), Stop))

        member this.Read() =
            Instruction (FileDataSourceInstruction.Parse (this.path, Stop))
    
    type DataSource<'content, 'query> with
        member this.Read() : Program<Result<'content, IError>> =
            resPgm {
                match this with
                | FromFile file ->
                    let! data = file.Read()
                    return data
            }
            
        member this.Store(content:'content) : Program<Result<unit, IError>> =
            resPgm {
                match this with
                | FromFile file ->
                    let! data = file.Save(content)
                    return data
            }

    module DataSource =
        let find<'content>(location: DataSource.Location option) =
            let inst = DataSourceInstruction<'content, unit, Program<Result<DataSource<'content>, DataSourceLoadingError>>>.Find((location, ()), Stop)
            Instruction inst
        
        let readAll<'content>(location: DataSource.Location option) =
            resPgm {
                let! dataSource = find<'content>(location)
                return! dataSource.Read()
            }

        let saveAll<'content>(location: DataSource.Location option, content:'content) =
            resPgm {
                let! dataSource = find<'content>(location)
                return! dataSource.Store(content)
            }

        let findWithQuery<'content, 'query>(location: DataSource.Location option, query:'query) =
            let inst = DataSourceInstruction<'content, 'query, Program<Result<DataSource<'content, 'query>, DataSourceLoadingError>>>.Find((location, query), Stop)
            Instruction inst   

        let read<'content, 'query>(location: DataSource.Location option, query: 'query) =
            resPgm {
                let! dataSource = findWithQuery<'content, 'query>(location, query)
                return! dataSource.Read()
            }

        let save<'content, 'query>(location: DataSource.Location option, query:'query, content:'content) =
            resPgm {
                let! dataSource = findWithQuery<'content, 'query>(location, query)
                return! dataSource.Store(content)
            }