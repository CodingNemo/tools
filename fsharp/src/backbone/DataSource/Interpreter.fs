﻿namespace backbone.DataSource

open System
open Spectre.Console
open backbone.DataSource.Errors
open backbone.DataSource.Instructions
open backbone.IO
open backbone.IO.Interpreter
open backbone.Result

module Interpreter =    
    let rec parseDataSourceLocation (text:string) : Result<DataSource.Location option, DataSourceLocationParsingError> =
         match text with
         | s when String.IsNullOrEmpty(s) -> Ok None
         | s -> 
            let indexOfFirstColumn = s.IndexOf(':')
            if indexOfFirstColumn < 0 then
                Error (InvalidLocation "Could not find any ':'. A Datasource location must start with 'env:', 'file:' or 'folder:'")
            else
                let dataSourceType = s.Substring(0, indexOfFirstColumn)
                let dataSourceQueryString = s.Substring(indexOfFirstColumn+1)
                match dataSourceType.ToLowerInvariant() with
                | "env" ->
                    let envVarValue = Environment.GetEnvironmentVariable(dataSourceQueryString)
                    if String.IsNullOrEmpty(envVarValue) then
                        Error (InvalidLocation $"Environment variable '{dataSourceQueryString}' does not exist")
                    else
                        parseDataSourceLocation envVarValue 
                | "file" ->
                    res {
                        let! filePath = parseFilePath dataSourceQueryString |> Result.mapError DataSourceLocationParsingError.FilePath
                        return Some(DataSource.Location.File filePath)
                    }
                | "folder" ->
                    res {
                        let! folderPath = parseRootedFolder dataSourceQueryString |> Result.mapError DataSourceLocationParsingError.FolderPath
                        return Some(DataSource.Location.Folder folderPath)
                    }
                | _ ->
                    Error (InvalidLocation "Unsupported datasource location. Supported types are 'env:', 'file:' or 'folder:'")
        
    let interpretDataSourceLocationInstructions interpret =
        function
        | DataSourceLocationInstruction.TryParse (text, next) ->
            let location = parseDataSourceLocation text
            interpret (next location)
