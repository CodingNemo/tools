﻿namespace backbone.DataSource

open backbone.IO

module DataSource =
    
    type Location =
    | Folder of RootedFolderPath
    | File of FilePath
            
    type File<'data> = {
        path: FilePath
    }   
    
type DataSource<'data, 'query> =
| FromFile of DataSource.File<'data>
    with
        static member File path =
            FromFile { path = path }
        
        member this.Folder =
            match this with
            | FromFile file -> file.path.Folder
 
type DataSource<'data> = DataSource<'data, unit>
