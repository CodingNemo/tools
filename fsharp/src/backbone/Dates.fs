﻿namespace backbone

open System
open backbone.Errors

module Dates = 

    type UnknownMonthException(month: string) =
        inherit Exception($"Unknown month {month}. Expected : Jan, Feb|Fev, Mar, Apr|Avr, May|Mai, Jun, Jul, Aug, Sep, Oct, Nov, Dec")

    type UnknownQuarterException(quarter: char) =
        inherit Exception($"Unknown quarter {quarter}. It should be 1, 2, 3 or 4")
    
    [<Measure>]
    type days
    
    [<Measure>]
    type day = days
   
    type Day = {
        number:int
    } with
        static member From(day:int<day>) =
            { number = int day }
        
        override this.ToString() =
            this.number.ToString().PadLeft(2, '0')
    
    module private DayParser =
        open FParsec
        
        let parser =
            let twoDigits =
                parse {
                    let! digit1 = digit
                    let! digit2 = digit
                    return $"{digit1}{digit2}"
                }
            
            parse {
                let! dayStr = twoDigits
                let day = int dayStr
                return {
                    number = day
                }
            }
            
        let parse text =
            match run parser text with
            | Success(result, _, _) -> Result.Ok result
            | Failure(errorMsg, _, _) -> Result.Error errorMsg
    
            
    type Day with
        static member Parse = DayParser.parse

    type Year = {
        value:int
    } with
        member this.IsLeap =
            let value = this.value
            value % 4 = 0 && (value % 100 <> 0 || value % 400 = 0)
        
        override this.ToString() =
            this.value.ToString()
        
        member this.ToShortString() =
            this.value.ToString().Substring(2,2)
            
    module Year =
        let year (value:int) =
            {
                value = value
            }

    module private YearParser =
        open FParsec
        
        let parser =
            let fourDigits = manyMinMaxSatisfy 4 4 isDigit
            let twoDigits = manyMinMaxSatisfy 2 2 isDigit
            
            parse {
                let! yearStr = fourDigits <|> twoDigits
                let year =
                    match yearStr.Length with
                    | 2 -> 2000 + int yearStr
                    | 4 -> int yearStr
                    | _ -> failwith "Invalid year format"
                return {
                    value = year
                }
            }
            
        let parse text =
            match run parser text with
            | Success(result, _, _) -> Result.Ok result
            | Failure(errorMsg, _, _) -> Result.Error errorMsg
            
    type Year with
        static member Parser = YearParser.parser
        static member Parse = YearParser.parse
        
    type Quarter =
       | First
       | Second
       | Third
       | Last
        with 
            override this.ToString() =
                match this with
                | First -> "Q1"
                | Second -> "Q2"
                | Third -> "Q3"
                | Last -> "Q4"

    type Year with
        member this.Quarters = [First; Second; Third; Last]
    
    type Month =
        | January
        | February
        | March
        | April
        | May
        | June
        | July
        | August
        | September
        | October
        | November
        | December
        member this.Days(year: Year) : Day list =
            let daysInMonth = 
                match this with
                | January -> 31
                | February -> if year.IsLeap then 29 else 28
                | March -> 31
                | April -> 30
                | May -> 31
                | June -> 30
                | July -> 31
                | August -> 31
                | September -> 30
                | October -> 31
                | November -> 30
                | December -> 31
            [1..daysInMonth]
            |> List.map (fun day -> { number = day })
        
            
        member this.Quarter =
            match this with
            | January | February | March -> First
            | April | May | June -> Second
            | July | August | September -> Third
            | October | November | December -> Last
            
        override this.ToString() =
            match this with
            | January -> "Jan"
            | February -> "Feb"
            | March -> "Mar"
            | April -> "Apr"
            | May -> "May"
            | June -> "Jun"
            | July -> "Jul"
            | August -> "Aug"
            | September -> "Sep"
            | October -> "Oct"
            | November -> "Nov"
            | December -> "Dec"
        
        member this.ToLongString() =
            match this with
            | January -> "January"
            | February -> "February"
            | March -> "March"
            | April -> "April"
            | May -> "May"
            | June -> "June"
            | July -> "July"
            | August -> "August"
            | September -> "September"
            | October -> "October"
            | November -> "November"
            | December -> "December"
        
    module private Quarter =
        open FParsec
        
        let private quarterMap =
            [ ('1', First)
              ('2', Second)
              ('3', Third)
              ('4', Last) ]
            |> Map.ofList
        
        let private findQuarterByChar quarterNumber =
            match quarterMap |> Map.tryFind quarterNumber with
            | Some quarter ->
                quarter
            | None ->
                raise(UnknownQuarterException(quarterNumber))
        
        let parser =
            parse {
                do! skipChar 'Q'
                let! quarterNumber = digit
                let quarter = findQuarterByChar quarterNumber
                return quarter  
            }
            
        let parse text =
            match run parser text with
            | Success(result, _, _) -> Result.Ok result
            | Failure(errorMsg, _, _) -> Result.Error errorMsg
    
    type Quarter with
        member this.Months =
            match this with
            | First -> [January; February; March]
            | Second -> [April; May; June]
            | Third -> [July; August; September]
            | Last -> [October; November; December]
        static member Parser = Quarter.parser
        static member Parse = Quarter.parse
        
    module private Month =
        open FParsec

        let fromInt month = 
            match month with
            | 1 -> January
            | 2 -> February
            | 3 -> March
            | 4 -> April
            | 5 -> May
            | 6 -> June
            | 7 -> July
            | 8 -> August
            | 9 -> September
            | 10 -> October
            | 11 -> November
            | 12 -> December
            | _ -> raise (UnknownMonthException(month.ToString()))
        
        let toInt month =
            match month with
            | January -> 1
            | February -> 2
            | March -> 3
            | April -> 4
            | May -> 5
            | June -> 6
            | July -> 7
            | August -> 8
            | September -> 9
            | October -> 10
            | November -> 11
            | December -> 12
        
        let private nameMap = 
            [ ("jan", January); ("feb", February); ("fev", February); ("mar", March)
              ("apr", April); ("avr", April); ("may", May); ("mai", May); ("jun", June)
              ("jul", July); ("aug", August); ("sep", September)
              ("oct", October); ("nov", November); ("dec", December) ]
            |> Map.ofList
        
        let private findMonthByName (name:string) =
            let month = nameMap |> Map.tryFind (name.ToLowerInvariant())
            match month with
            | Some month -> month
            | None -> raise (UnknownMonthException(name))
        
        let parser =
            parse {
                let twoDigits  =
                    parse {
                        let! firstDigit = digit
                        let! secondDigit = digit
                        return $"{firstDigit}{secondDigit}"
                    }
                let threeLetters =
                    parse {
                        let! firstLetter = asciiLetter
                        let! secondLetter = asciiLetter
                        let! thirdLetter = asciiLetter
                        return $"{firstLetter}{secondLetter}{thirdLetter}"
                    }
                
                let! monthStr = threeLetters <|> twoDigits
                return
                    match monthStr.Length with
                    | 2 ->
                        let monthAsInt = int monthStr
                        monthAsInt |> fromInt
                    | 3 ->
                        monthStr |> findMonthByName
                    | _ -> failwith "Invalid month format"
            }
            
        let parse (text:string) =
            match run parser text with
            | Success(result, _, _) -> Result.Ok result
            | Failure(errorMsg, _, _) -> Result.Error errorMsg
            
        let findMonthName month =
            nameMap |> Map.findKey month
        let serialize month =
            findMonthName month
   
    type Month with
        static member Parser = Month.parser
        static member Parse = Month.parse
        static member FromInt = Month.fromInt
        member this.ToInt() = this |> Month.toInt
        member this.ToIntString(?padding:int) = 
            let monthNumber = this |> Month.toInt
            let padding = padding |> Option.defaultValue 2
            monthNumber.ToString().PadLeft(padding, '0')
        member this.ToShortString() =
            this.ToInt().ToString().PadLeft(2, '0')

    type DayOfWeek =
    | Monday
    | Tuesday
    | Wednesday
    | Thursday
    | Friday
    | Saturday
    | Sunday

    [<CustomEquality;CustomComparison>]
    type Date = {
        day: Day
        month: Month
        year: Year
    }
        with
            static member val Today =
                DateTime.Today |> DateOnly.FromDateTime |> Date.FromDateOnly

            member this.ToLongString() =
                $"{this.day} {this.month} {this.year}"
            override this.ToString() =
                $"{this.year}-{this.month.ToShortString()}-{this.day}"

            static member val WeekendDays = [Saturday; Sunday]

            member this.DayOfWeek = 
                let day = this.day.number
                let month = this.month.ToInt()
                let year = this.year.value

                let t = [| 0; 3; 2; 5; 0; 3; 5; 1; 4; 6; 2; 4 |]
                let y = if month < 3 then year - 1 else year
                let dow = (y + (y / 4) - (y / 100) + (y / 400) + t.[month - 1] + day) % 7
                match dow with
                | 0 -> Sunday
                | 1 -> Monday
                | 2 -> Tuesday
                | 3 -> Wednesday
                | 4 -> Thursday
                | 5 -> Friday
                | 6 -> Saturday
                | _ -> failwith $"unknown day of week '{dow}'"


            member this.IsWeekend =
                Date.WeekendDays |> List.contains(this.DayOfWeek)
            member private this.AsDateOnly() : DateOnly =
                DateOnly(this.year.value, this.month.ToInt(), this.day.number)
            static member private FromDateOnly(date: DateOnly) =
                {
                    day = { number = date.Day }
                    month = date.Month |> Month.FromInt
                    year = { value = date.Year }
                }
            member this.Add(days:int<days>) =
                this.AsDateOnly().AddDays(int days) |> Date.FromDateOnly
            override this.GetHashCode() =
                HashCode.Combine(this.day.number, this.month, this.year)
            
            override this.Equals(obj) =
                match obj with
                | :? Date as other ->
                    this.Equals(other)
                | _ -> false
                                
            interface IEquatable<Date> with
                member this.Equals(other) =
                    this.day.number = other.day.number
                    && this.month = other.month
                    && this.year = other.year
                
            interface IComparable with
                member this.CompareTo(obj) =
                    match obj with
                    | :? Date as other ->
                        let compareYear = this.year.value.CompareTo(other.year.value)
                        if compareYear <> 0 then compareYear
                        else
                            let compareMonth = this.month.ToInt().CompareTo(other.month.ToInt())
                            if compareMonth <> 0 then compareMonth
                            else this.day.number.CompareTo(other.day.number)
                    | _ -> invalidArg "obj" "Cannot compare Date with other types"
     
    module private DateParser =
        open FParsec
        
        // TODO : handler special cases :
        // today, tomorrow, yesterday
        // +/- days from today (ex +1d, -2d)
        // +/- weeks from today (ex +1w, -2w)
        // +/- months from today (ex +1m, -2m)
        // +/- years from today (ex +1y, -2y)
        // som/eom : start/eom of month (ex eomJan25, somFeb22)
        // soy/eoy : start/end of year 
        // sow/eow : start/end of week
        let parser =
            parse {
                let! year = YearParser.parser
                do! skipChar '-'
                let! month = Month.parser
                do! skipChar '-'
                let! day = DayParser.parser
                return {
                    day = day
                    month = month
                    year = year
                }
            }
            
        let parse (text:string) =
            if text.Length < 10 then
                Result.Error $"Invalid date format. Expected yyyy-MM-dd. Actual '{text}'"
            else
                match run parser text with
                | Success(result, _, _) -> Result.Ok result
                | Failure(errorMsg, _, _) -> Result.Error errorMsg

    module Date =
        let date (day:Day, month:Month, year:Year) =
            {
                day = day
                month = month
                year = year
            }
            
    type Date with
        static member Parse = DateParser.parse
        
    // TODO : Handle periods
    // case 1: single date (see Date parser)
    //         - yyyy-MM-dd
    //         - today
    //         - tomorrow
    //         - yesterday 
    // case 2: start/from period [start date..end date]
    //         - [today..+3d]
    //         - [-6m..today]
    // case 3: quantitative periods x(d/w/m/y)
    //         - 3d
    //         - 2w
    //         - 1m
        
    type AsOfDate = {
        date:Date
    } with    
        static member Parse =
            Date.Parse
            >> Result.map (fun date -> { date = date })
            >> Result.mapError (fun err -> ArgError.InvalidArg ("AsOfDate", Some err))
        
        static member From(date:Date) =
            { date = date }
        
        static member Today = AsOfDate.From Date.Today
        
        static member ParseOrDefault(text:string) =
            if String.IsNullOrWhiteSpace text then
                Ok AsOfDate.Today
            else
                AsOfDate.Parse text
        
        override this.ToString() = this.date.ToString()

    
    