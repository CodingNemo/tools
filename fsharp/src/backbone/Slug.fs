﻿namespace backbone

open System.Text.RegularExpressions
open backbone.Errors

 
type Slug = {
    value: string
}
    with
        static member Parse(slug:string) : Result<Slug, ArgError> =
            let isValid (slug:string) =
                // Step 1: Check if there are consecutive spaces
                let noConsecutiveSpaces = not (Regex.IsMatch(slug, @"\s{2,}"))
                // Step 2: Ensure the string contains only alphabetic characters and hyphens
                let onlyAlphaAndHyphen = Regex.IsMatch(slug, @"^[a-zA-Z-]+$")
                // Step 3: Ensure no spaces are left (they should be replaced by hyphens)
                let noSpaces = not (slug.Contains(" "))
                
                noConsecutiveSpaces && onlyAlphaAndHyphen && noSpaces
            
            if isValid slug then
                Ok {
                    value = slug
                }
            else
                Error(InvalidArg("slug", Some slug))
        
        static member TryParse(text:string) =
            match Slug.Parse text with
            | Ok slug -> Some slug
            | Error _ -> None
        
        static member GenerateFrom(text:string) =
            let text = text.Replace("-", "")
            let text = Regex.Replace(text, @"\s+", " ")
            let text = text.Replace(" ", "-")
            let text = Regex.Replace(text, "[^a-zA-Z-]", "")
            let slug = text.ToLowerInvariant()
            { value = slug }
            
        member this.StartsWith(expected:Slug) =
            this.value.StartsWith(expected.value)
            
        override this.ToString() =
            this.value

    
