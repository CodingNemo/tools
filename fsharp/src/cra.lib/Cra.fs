﻿namespace cra.lib

open clients.lib

open backbone
open backbone.Dates

open schedule.lib
open schedule.lib.Schedule
open backbone.Result
open backbone.IO

type CraTarget = 
| AllClients
| SingleClient of SlugOrId<ClientId>

module CraTarget = 
    let parse (clientOrSlugId:string) =
        res { 
            if System.String.IsNullOrWhiteSpace(clientOrSlugId) then
                return CraTarget.AllClients
            else
                let! slugOrClientId = SlugOrId.parse<ClientId> clientOrSlugId
                return CraTarget.SingleClient slugOrClientId
        }

type Cra(reference:MonthReference, client:Client, mission:Mission, days:Map<Date, float>) =
    static member FromSchedule (monthlySchedule:MonthlySchedule, clients:Clients, target:CraTarget) : Cra list =
        let matchesSlugOrId (slugOrId:SlugOrId<ClientId>) (client:Client) =
            match slugOrId with
            | SlugOrId.Slug slug ->
                client.Slug = slug
            | SlugOrId.Id id ->
                client.Id = id

        let matchesTarget (target:CraTarget) (client:Client) = 
            match target with
            | AllClients -> true
            | SingleClient slugOrId -> 
                client |> matchesSlugOrId slugOrId

        match monthlySchedule.Content with
        | NotGenerated -> []
        | Generated schedule ->
            schedule |> Seq.choose (
                                fun activity ->
                                    match activity with
                                    | Mission (missionId, activitiesByDates) ->
                                        let client = clients.FindBySlug(missionId.clientSlug)

                                        match client with
                                        | [client] when client |> matchesTarget target ->
                                            Some (monthlySchedule.Reference, client, missionId, activitiesByDates)
                                        | _ -> None
                                    | _ ->
                                        None
                                )
                        |> Seq.map Cra.Create
                        |> Seq.toList
    
    static member private Create(reference: MonthReference, client:Client, missionId:MissionId, activitiesByDate : ActivityByDate list) =
        let toDayAmount (span:ActivitySpan option) : float =
                        match span with
                        | None -> 0
                        | Some ActivitySpan.FullDay -> 1
                        | Some ActivitySpan.MorningOnly
                        | Some ActivitySpan.AfternoonOnly -> 0.5
                       
        let amountByDate = activitiesByDate
                            |> Seq.map (fun (span, date) -> date, span |> toDayAmount)
                            |> Map.ofSeq

        let mission = client.GetMission(missionId)
            
        Cra(reference, client, mission, amountByDate)
    
    member this.Reference = reference
    member this.Client = client
    member this.Mission = mission
    member this.Days = days
    member this.Total = days.Values |> Seq.sum

type ExportingCraError =
        | NoExporterFound of Client
        | InvalidExporter of obj
        | ExportFailed of exn * exporterName:string * Cra
        interface IError

