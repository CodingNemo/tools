﻿namespace cra.lib

open backbone.IO

module Interpreter =
    let interpretExport (cra:Cra) (targetFile:FilePath) (exporter:obj) = 
        match exporter with
        | :? cra.exporters.IExportCra as exporter -> 
            try 
                exporter.Export targetFile cra 
                Ok()
            with 
            | ex -> Error (ExportingCraError.ExportFailed (ex, exporter.GetType().FullName, cra))
        | _ -> Error (ExportingCraError.InvalidExporter exporter)

    let interpret interpret = 
        function 
        | ExporterInstructions.ExportCra ((exporter, cra, targetFile), next) ->
            let exportResult = exporter |> interpretExport cra targetFile
            interpret (next exportResult)
