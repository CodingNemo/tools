﻿namespace cra.exporters

open backbone.IO
open cra.lib

type IExportCra = 
    abstract member Export : FilePath -> Cra -> unit
