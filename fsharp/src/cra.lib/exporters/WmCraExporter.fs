﻿namespace cra.exporters


type WmCraExporter() =
    interface IExportCra with
        member this.Export(targetFilePath: backbone.IO.FilePath) (cra: cra.lib.Cra): unit = 
            raise (System.NotImplementedException())
        
