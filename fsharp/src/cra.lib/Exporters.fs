﻿namespace cra.lib

open backbone
open backbone.Program

type ExporterInstructions<'a> =
    | ExportCra of (obj * Cra * backbone.IO.FilePath)  * next: (Result<unit, ExportingCraError> -> 'a)
    interface IInstruction<'a> with
        member this.Map f =
            match this with
            | ExportCra ((exporter, cra, targetFile), next) ->
                ExportCra ((exporter, cra, targetFile), f << next)
                 
module Exporter = 
    let export (cra:Cra) (targetFile:backbone.IO.FilePath) (exporter:obj) =
        Instruction (ExporterInstructions.ExportCra ((exporter, cra, targetFile), Stop))

module Exporters =
    let exporters = Map.empty
                        .Add("wm", new cra.exporters.WmCraExporter() :> obj)

    let export (targetFile: backbone.IO.FilePath) (cra: Cra) =
        let exporter = exporters.TryFind cra.Client.Slug.value
        match exporter with
        | Some exporter ->
            exporter |> Exporter.export cra targetFile
        | None ->
            Error (ExportingCraError.NoExporterFound cra.Client) |> lift

