﻿namespace cra.lib

open backbone
open backbone.IO
open backbone.DataSource
open backbone.DataSource.Instructions
open backbone.Program


open clients.lib
open schedule.lib
open schedule.lib.Schedule

module Instructions =
        
    
    type Cra with 

        static member GenerateAll
            (monthReference:MonthReference) (target:CraTarget) 
            (clientsSource:DataSource<Clients>) 
            (scheduleLocation:DataSource.Location option) = 
            resPgm {
               let! monthlySchedule = Schedule.QueryMonthlySchedule monthReference scheduleLocation
               let! clients = clientsSource.Read()
               
               let! craList = Cra.FromSchedule(monthlySchedule, clients, target) |> Ok |> lift
               return craList
            }

        static member ExportAll 
            (craList:Cra list) 
            (clientsFolder:RootedFolderPath) = 
            resPgm {
                for cra in craList do
                    do! cra.Export clientsFolder
            }

        member this.Export(clientsFolder:RootedFolderPath) = 
            resPgm {
                let clientFolderName : FolderName = this.Client.Slug.value
                
                let yearFolderName : FolderName = this.Reference.year.ToString()
                let monthFolderName : FolderName = $"{this.Reference.month.ToIntString()} - {this.Reference.month.ToString()}"
                let fileName = FileName.Parse $"CRA {this.Reference.Display()}.pdf"
                
                let filePath = clientsFolder / clientFolderName / yearFolderName / monthFolderName / fileName

                do! Exporters.export filePath this
            }


