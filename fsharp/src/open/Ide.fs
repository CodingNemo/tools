﻿namespace Open

module Ide =

    open System.Text.Json
    open cache.lib
    
    open Humanizer
    
    open Spectre.Console

    type OpenTarget =
        | File
        | Directory
    
    type OpenTargetJsonConverter() =
        inherit System.Text.Json.Serialization.JsonConverter<OpenTarget>()
        
        override this.Read (reader, _, _) =
            let value = reader.GetString()
            match value with
            | "File" -> OpenTarget.File
            | "Directory" -> OpenTarget.Directory
            | _ -> failwith $"Unsupported value '{value}'"
            
        override this.Write (writer, value, _) =
            writer.WriteStringValue(value.ToString())
    
    let private jsonSerializerOptions =
        let options = JsonSerializerOptions()
        options.Converters.Add(OpenTargetJsonConverter())
        options
    
    type Ide =
        {
            aliases: string list
            name: string
            exeFileName: string
            exeFilePath: string
            targetFilePattern: string
            openTarget: OpenTarget 
        }
    let private idesCacheKey = "ides"
    let private idesCacheTtl = (720).Days()
    let private list =
        let cache = Cache.``global``
        let cacheItem = Cache.tryGet cache idesCacheKey
        match cacheItem with
        | Some item -> item.GetContent<Ide list>(jsonSerializerOptions)
        | None -> []
    let private register (ide:Ide) =
        let cache = Cache.``global``
        
        let updateIdes (ides:Ide list) =
            ide::ides |> Cache.Result.Value
        let createIdes () =
            [ide]

        Cache.addOrUpdateWithOptions jsonSerializerOptions idesCacheKey idesCacheTtl updateIdes createIdes cache
        |> ignore
    let private tryFind (search:string) =
        if System.String.IsNullOrEmpty(search) then
            None
        else
            let cache = Cache.``global``
            let item = Cache.tryGet cache idesCacheKey
            
            match item with
            | Some item when not(item.expired) ->
                let ides = item.GetContent<Ide list>(jsonSerializerOptions)
                ides |> List.tryFind (fun ide -> ide.name = search || ide.aliases |> List.exists (fun alias -> alias = search))
            | _ ->
                None
        
    type FilePathChoice =
        | Exit
        | Prompted
        | Found of string
        
    let private findFile (filePattern:string) (directory:string) =
        
        let files = System.IO.Directory.GetFiles(directory, filePattern, System.IO.SearchOption.AllDirectories)

        match files with
        | [||] -> None
        | [|file|] -> Some file
        | files ->
            
            let formatPath (directory:string) (path:string) =
                System.IO.Path.GetRelativePath(directory, path)
            
            let prompt = SelectionPrompt()
        
            prompt.Title <- $"File {filePattern} found in {files.Length} locations. Please choose one"
    
            prompt.Converter <- fun choice ->
                match choice with
                | FilePathChoice.Exit -> " :door: [red]EXIT[/]"
                | FilePathChoice.Prompted -> " :keyboard:  [blue]PROMPT[/]"
                | FilePathChoice.Found file -> $" :black_medium_small_square: {formatPath directory file}"
            
            let choices = FilePathChoice.Exit::FilePathChoice.Prompted::(files |> Array.map FilePathChoice.Found |> Array.toList)
            
            prompt.AddChoices(choices) |> ignore
            let choice = AnsiConsole.Prompt(prompt)
            
            match choice with
            | FilePathChoice.Exit ->
                System.Environment.Exit(0)
                None
            | FilePathChoice.Prompted ->
                let path = AnsiConsole.Ask<string>("Please enter the path to the file :")
                Some path
            | FilePathChoice.Found path ->
                Some path

    let private (/) (left:System.Environment.SpecialFolder) right = System.IO.Path.Combine(System.Environment.GetFolderPath(left), right)

    let private askForExeFilePath (exeFile:string) =
        let promptedExeFilePath = AnsiConsole.Ask<string>($"Please enter the path to the {exeFile} executable. (leave blank to skip this)")
            
        if System.String.IsNullOrWhiteSpace(promptedExeFilePath) then
            None
        elif not (promptedExeFilePath.EndsWith(exeFile)) then
            AnsiConsole.MarkupLine($"Path {promptedExeFilePath} does not target file {exeFile}")
            None
        elif not (System.IO.File.Exists(promptedExeFilePath)) then
            AnsiConsole.MarkupLine($"File {promptedExeFilePath} does not exist")
            None
        else
            Some promptedExeFilePath
    
    let private findProgramExeFilePath (exeFile:string) : string option =
        let findFileWithLog fileName directory =
            AnsiConsole.MarkupLine($"Searching in [green]{directory}[/]")
            findFile fileName directory
        
        let foundExeFile = AnsiConsole.Status()
                            .Start("Finding .exe file [green]{exeFile}[/]", fun ctx ->
                                findFileWithLog exeFile (System.Environment.SpecialFolder.LocalApplicationData / "Programs")
                                |> Option.orElseWith (fun () -> findFileWithLog exeFile (System.Environment.GetFolderPath(System.Environment.SpecialFolder.Programs)))
                                |> Option.orElseWith (fun () -> findFileWithLog exeFile (System.Environment.GetFolderPath(System.Environment.SpecialFolder.ProgramFiles)))
                                |> Option.orElseWith (fun () -> findFileWithLog exeFile (System.Environment.GetFolderPath(System.Environment.SpecialFolder.ProgramFilesX86)))
                            )
        
        match foundExeFile with
        | Some exeFilePath ->
            AnsiConsole.MarkupLine($"Found {exeFilePath}")
            Some exeFilePath
        | None ->
            let attempt = 0
            let mutable foundFile = None
            
            while attempt < 3 && foundFile |> Option.isNone do
                let promptedExeFilePath = askForExeFilePath exeFile
                
                match promptedExeFilePath with
                | Some exeFilePath ->
                    foundFile <- Some exeFilePath
                | None ->
                    ()
            None

    let private newIdePrompt () =
        AnsiConsole.MarkupLine(":factory: Please enter the required information for this new IDE")
        
        let name = AnsiConsole.Ask(" :diamond_with_a_dot: name: ")
        
        if System.String.IsNullOrEmpty(name) then
            None
        else 
            let aliasesStr = AnsiConsole.Ask<string>(" :diamond_with_a_dot: aliases (coma separated): ")
            
            let exeFileName = AnsiConsole.Ask(" :diamond_with_a_dot: exe file name: ")
            
            let exeFilePath = findProgramExeFilePath exeFileName
            
            if exeFilePath |> Option.isNone then
                None
            else 
                let targetFilePattern = AnsiConsole.Ask(" :diamond_with_a_dot: target file pattern: ")
                
                let openTargetPrompt = SelectionPrompt<OpenTarget>()
                openTargetPrompt.Title <- " :diamond_with_a_dot: choose what to open"
                openTargetPrompt.AddChoice(OpenTarget.Directory) |> ignore
                openTargetPrompt.AddChoice(OpenTarget.File) |> ignore
                
                let openTarget = AnsiConsole.Prompt(openTargetPrompt)
                
                let aliases =
                    aliasesStr.Split([|','|], System.StringSplitOptions.RemoveEmptyEntries) |> List.ofArray
                
                let ide = {
                    name = name
                    aliases = aliases
                    exeFileName = exeFileName
                    exeFilePath = exeFilePath |> Option.get 
                    targetFilePattern = targetFilePattern
                    openTarget = openTarget
                }
                
                register ide
            
                Some ide

    type IdeChoice =
        | Exit 
        | New
        | Known of Ide
        
    let private ideSelectionPrompt () =
        let prompt = SelectionPrompt()
        
        prompt.Title <- "Choose an IDE"
        
        let ides = list
        
        let choices = IdeChoice.Exit::IdeChoice.New::(ides |> List.map IdeChoice.Known)
        
        prompt.Converter <- fun choice ->
            match choice with
            | IdeChoice.Exit -> " :door: [red]EXIT[/]"
            | IdeChoice.New -> " :plus: [green]NEW IDE[/]"
            | IdeChoice.Known ide ->
                match ide.aliases with
                | [] -> ide.name
                | aliases ->
                    $" :diamond_with_a_dot: {ide.name}[gray] as {aliases.Humanize()}[/]"
        
        prompt.AddChoices(choices) |> ignore
        
        let choice = AnsiConsole.Prompt(prompt)
        
        match choice with
        | IdeChoice.Known ide ->
            Some ide
        | IdeChoice.Exit ->
            System.Environment.Exit(0)
            None
        | IdeChoice.New ->
            let ide = newIdePrompt()
            ide 
        
    let private select directory =
        let cache = Cache.``for`` directory
        let cacheKey = "default-ide"
        let ttl = (365).Days()
        
        cache |> Cache.getOrAddWithOptions jsonSerializerOptions cacheKey ttl (fun () ->
            let ide = ideSelectionPrompt()
            match ide with
            | Some ide ->
                Cache.Result.Value (Some ide.name)
            | None ->
                Cache.Result.Skipped None
        ) |> Option.bind tryFind
        
    let private findTarget directory ide =
        let cache = Cache.``for`` directory
        let cacheKey = "ide-default-target"
        let ttl = (5).Days()
        
        cache |> Cache.getOrAdd cacheKey ttl (fun () ->
            let targetFile = findFile ide.targetFilePattern directory
            match targetFile with
            | Some targetFile ->
                Cache.Result.Value (Some targetFile)
            | None ->
                Cache.Result.Skipped None
        )
        
    let ``open`` (directory:string) (search:string) =
        let ide = if not(System.String.IsNullOrEmpty(search)) then
                      let found = tryFind search
                      match found with
                      | Some ide -> Some ide
                      | None ->
                          AnsiConsole.MarkupLine($"[red]Unknown IDE {search}[/]")
                          select directory
                  else 
                      select directory
        
        match ide with
        | None ->
            AnsiConsole.MarkupLine("No IDE selected")
        | Some ide ->
            let target = ide |> findTarget directory

            match ide.openTarget with
                | OpenTarget.File ->
                    match target with
                    | None ->
                        AnsiConsole.MarkupLine($"No {ide.targetFilePattern} found in {directory}")
                    | Some target ->
                        System.Diagnostics.Process.Start(ide.exeFilePath, target) |> ignore
                | OpenTarget.Directory ->
                    let targetDirectory = target
                                        |> Option.map System.IO.Path.GetDirectoryName
                                        |> Option.defaultValue directory
                    System.Diagnostics.Process.Start(ide.exeFilePath, targetDirectory) |> ignore

                