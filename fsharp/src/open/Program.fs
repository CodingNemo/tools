﻿open System.ComponentModel

open Spectre.Console.Cli

module Open =

    type Settings() =
        inherit CommandSettings()

        [<CommandOption("-w|--with|--ide")>]
        [<Description("specify the ide to use")>]    
        member val Ide : string = null with get, set
        
    type Command() =
        inherit Command<Settings>()
        
        override this.Execute(_, settings) =
            let currentDirectory = System.IO.Directory.GetCurrentDirectory()
            Open.Ide.``open`` currentDirectory settings.Ide
            0


[<EntryPoint>]
let main args =
    let app = CommandApp<Open.Command>()
    app.Run args