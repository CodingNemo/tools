﻿namespace cra.exporter.wm

open cra.lib


type WmCraExporter() =
    interface IExportCra with
        member this.Export(arg1: backbone.IO.FilePath) (arg2: Cra): Result<unit,ExportingCraError> = 
            raise (System.NotImplementedException())
        
