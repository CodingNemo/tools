﻿module EnvironmentVariables

open System
open Definitions

let Path = PathEnvironmentVariable()

let OneDrive = EnvironmentVariable("OneDrive", EnvironmentVariableTarget.User)

let LocalAppData = EnvironmentVariable("LOCALAPPDATA", EnvironmentVariableTarget.Process)

let T_TOOLS = EnvironmentVariable("T_TOOLS", EnvironmentVariableTarget.User)
let T_CLIENTS_SOURCE = EnvironmentVariable("T_CLIENTS_SOURCE", EnvironmentVariableTarget.User)
let T_SCHEDULE_ROOT = EnvironmentVariable("T_SCHEDULE_ROOT", EnvironmentVariableTarget.User)