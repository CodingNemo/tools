﻿namespace backbone.cli

open System.Reflection
open Spectre.Console
open Spectre.Console.Cli
open Spectre.Console.Cli.Unsafe

module Cli =

    module Command =
        let withAliases (aliases:string list option) (configurator:ICommandConfigurator) =
            aliases |> Option.iter (fun aliases ->
                aliases |> List.iter (fun alias -> configurator.WithAlias(alias) |> ignore)
                )
    
    module Branch =
        
        type Branch<'r, 'settings when 'settings :> CommandSettings>(configurator: IUnsafeConfigurator, interpreter: 'r -> int) =
            member this.Configure(f: IUnsafeConfigurator -> 'b) =
                f configurator |> ignore
                
            member val Interpreter = interpreter with get
        
        type private BranchUnitFuncCommand<'settings when 'settings :> CommandSettings>() =
            inherit Command()
            
            override _.Execute(context) =
                let handle = context.Data :?> unit -> int
                handle()
            
            interface ICommandLimiter<'settings>
        
        type private BranchFuncCommand<'settings when 'settings :> CommandSettings>() =
            inherit Command<'settings>()
            
            override _.Execute(context, settings) =
                let handle = context.Data :?> 'settings -> int
                handle settings
            
            interface ICommandLimiter<'settings>
            
        type Builder<'r, 'branchSettings when 'branchSettings :> CommandSettings>(branch: Branch<'r, 'branchSettings>) =
            member _.Bind(_: unit, f) = f()
            member _.Return(x) = x
            member _.ReturnFrom(x) = x
            member _.Yield(()) = ()
            
            [<CustomOperation("cmd_")>]
            member _.CommandWithoutSettings(_: unit, name:string, handle: unit -> 'r) =
                branch.Configure _.AddCommand(name, typeof<BranchUnitFuncCommand<'branchSettings>>)
                                  .WithData(handle >> branch.Interpreter)  
                

            [<CustomOperation("cmd")>]
            member _.CommandWithSettings<'settings when 'settings :> CommandSettings>(
                _: unit,
                name:string,
                handle: 'settings -> 'r) =
                
                branch.Configure _.AddCommand(name, typeof<BranchFuncCommand<'settings>>)
                                  .WithData(handle >> branch.Interpreter)
            
            member this.Run _ =
                ()
            
                
        let branch<'r, 'settings  when 'settings :> CommandSettings> b = Builder<'r,'settings>(b)
    
    module App =
        
        type App(app:CommandApp) =
            member this.Configure(f: CommandApp -> 'b) =
                f app |> ignore
            
            member this.Configure2(f: IConfigurator -> 'b) =
                
                app.Configure (fun c ->
                    f c |> ignore)
            
            member this.ConfigureUnsafe2(f: IUnsafeConfigurator -> 'b) =
                app.Configure (fun c ->  f (c.SafetyOff()) |> ignore)
                
            member this.Run(args) =
                app.Run(args)
        
        type private FuncCommand() =
            inherit Command()
            
            override _.Execute(context) =
                let handle = context.Data :?> unit -> int
                handle ()
        
        type private FuncCommand<'settings when 'settings :> CommandSettings>() =
            inherit Spectre.Console.Cli.Command<'settings>()
            
            override _.Execute(context, settings : 'settings) =
                let handle = context.Data :?> 'settings -> int
                handle settings
        
        type Builder<'r>(args: string array) =
            let app = App(CommandApp())

            let mutable interpreter : 'r -> int = unbox
            
            member _.Bind(_: unit, f) = f()
            member _.Return(x) = x
            member _.ReturnFrom(x) = x
            member _.Yield(()) = ()
            member _.Run _ =
                app.Run(args)
            
            [<CustomOperation("defaultCmd_")>]
            member _.DefaultWithoutSettings(_: unit, handle: unit -> 'r) =
                app.Configure _.SetDefaultCommand<FuncCommand>()
                               .WithData(handle >> interpreter)
            
            [<CustomOperation("defaultCmd")>]
            member _.DefaultWithSettings<'settings when 'settings :> CommandSettings>(_: unit, handle: 'settings -> 'r) =
                app.Configure _.SetDefaultCommand<FuncCommand<'settings>>()
                               .WithData(handle >> interpreter)
            
            [<CustomOperation("cmd_")>]
            member _.CommandWithoutSettings(_: unit, name:string, handle: unit -> 'r, ?aliases:string list) =
                let handle = handle >> interpreter
                app.Configure2 (_.AddDelegate(name, fun _ -> handle()) >> Command.withAliases aliases)
                
                
            [<CustomOperation("cmd")>]
            member _.CommandWithSettings<'settings when 'settings :> CommandSettings> (_: unit, name:string, handle: 'settings -> 'r, ?aliases:string list) =
                app.Configure2 (_.AddDelegate(name, fun _ -> handle >> interpreter)  >> Command.withAliases aliases)

                
            [<CustomOperation("branch")>]
            member _.Branch<'branchSettings when 'branchSettings :> CommandSettings>(_:unit, branchName:string, buildBranch: Branch.Branch<'r, 'branchSettings> -> unit) =
                app.ConfigureUnsafe2 _.AddBranch(branchName, typeof<'branchSettings>, fun branchConfiguration ->
                                        buildBranch(Branch.Branch(branchConfiguration, interpreter))
                                    )
            
            [<CustomOperation("onUnhandledException")>]
            member _.OnUnhandledException(_: unit, handler: exn -> ITypeResolver -> unit) =
                app.Configure2 _.PropagateExceptions()
                                .SetExceptionHandler(handler)
                
            [<CustomOperation("name")>]
            member _.Name(_: unit, name:string) =
                app.Configure2 _.SetApplicationName(name)
                
            [<CustomOperation("interpreter")>]
            member _.Interpreter(_: unit, i: 'r -> int) =
                interpreter <- i

        let private handleException (ex:exn) _ : unit =
            AnsiConsole.WriteException(ex, ExceptionFormats.Default)
        
        let app args =
            let builder = Builder(args)
            builder.OnUnhandledException((), handleException)
            let defaultName = Assembly.GetCallingAssembly().GetName()
            builder.Name((), defaultName.Name)
            builder