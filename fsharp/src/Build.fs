﻿module Build

open Fake.Core
open Fake.Core.TargetOperators
open Fake.IO

open Definitions
open Helpers
open Console

#nowarn "3391"

initializeContext ()

let nautilusConsultingDefaultPath = EnvironmentVariables.OneDrive.GetOrNone() <?/> "Administratif" <?/> "Travail" <?/> "Nautilus Consulting SARL" 

let clientsInstall (project:Project) path force yes=
    
    if project.IsInstalledIn(path) && not(force) then
        writeWarning $"{project} already installed"
    else 
        let defaultClientsSource = nautilusConsultingDefaultPath <?/> "Clients" <?/> "clients.csv"
        let promptUserForClientsSource() = promptUserForExistingPath "the csv file containing clients"
        
        let getClientsSourcePath () =
            match defaultClientsSource with
            | Some path when File.exists path -> 
                writeInfo $"Default path to client source file was found : '{path}'"
                if yes || confirm "Use default path ?" then
                    path
                else
                    promptUserForClientsSource()
            | _ ->
                promptUserForClientsSource()

        let setClientsSourcePath () =
            let clientsSourcePath = getClientsSourcePath()
            EnvironmentVariables.T_CLIENTS_SOURCE.Set($"file:{clientsSourcePath}")
                    
        writePending "Setting up environment variables"

        let actualClientsSourcePath = EnvironmentVariables.T_CLIENTS_SOURCE.GetOrNone()

        match actualClientsSourcePath with 
        | Some actualClientsSourcePath -> 
            if confirm $"Clients source file path already set with '[yellow]{actualClientsSourcePath}[/]'. Do you wish to use it ?" then 
                ()
            else
                setClientsSourcePath()
                
        | None -> 
            setClientsSourcePath()
        
        writeSuccess "Environment variables ready"
        
        Installers.defaultExeInstall project path force yes

let clientsUninstall project path =
    writePending "Cleaning Environment variables"
    EnvironmentVariables.T_CLIENTS_SOURCE.Clear()
    writeSuccess "Environment variables cleaned"
    
    Installers.defaultExeUninstall project path

let scheduleInstall (project:Project) path force yes=
    if project.IsInstalledIn(path) && not(force) then
        writeWarning $"{project} already installed"
    else 
    
        let defaultScheduleSource = nautilusConsultingDefaultPath <?/> "Schedule"
        let promptUserForScheduleSource() = promptUserForExistingPath "the root directory containing schedules directory tree"
            
        let getScheduleRootPath () =
            match defaultScheduleSource with
            | Some path when Directory.exists path ->
                writeInfo $"Default path to schedule root was found : '{path}'"
                if yes || confirm "Use default path ?" then
                    path
                else
                    promptUserForScheduleSource()
            | _ ->
                promptUserForScheduleSource()

        let setScheduleRootPath() =
            let scheduleRootPath = getScheduleRootPath()
            EnvironmentVariables.T_SCHEDULE_ROOT.Set($"folder:{scheduleRootPath}")
                                
        writePending "Setting up environment variables"
        let actualSourceRootPath = EnvironmentVariables.T_SCHEDULE_ROOT.GetOrNone()
        match actualSourceRootPath with 
        | Some actualSourceRootPath -> 
            if yes || confirm $"Schedule root path already set with '[yellow]{actualSourceRootPath}[/]'. Do you wish to use it ?" then 
                ()
            else 
                setScheduleRootPath()
        | None -> 
            setScheduleRootPath()
        
        writeSuccess "Environment variables ready"
    
        Installers.defaultExeInstall project path force yes
        
let scheduleUninstall project path =
    writePending "Cleaning Environment variables"
    EnvironmentVariables.T_SCHEDULE_ROOT.Clear()
    writeSuccess "Environment variables cleaned"
    
    Installers.defaultExeUninstall project path
    
    

let backbone = Project.Library("backbone").WithTests()
let cache = Project.Console("cache")
                    .Install(Installers.defaultExeInstall)
                    .Uninstall(Installers.defaultExeUninstall)

let open' = Project.Console("open")
                          .Install(Installers.defaultExeInstall)
                          .Uninstall(Installers.defaultExeUninstall)

let clients = Project.Console("clients")
                            .Install(clientsInstall)
                            .Uninstall(clientsUninstall)
                            .WithTests()

let schedule = Project.Console("schedule", ["sched"])
                             .Install(scheduleInstall)
                             .Uninstall(scheduleUninstall)
                             .WithTests()
                        
let cra = Project.Console("cra")
                 .Install(Installers.defaultExeInstall)
                 .Uninstall(Installers.defaultExeUninstall)
                 .DependsOn([clients; schedule])

let projects =
    [ backbone
      cache
      open'
      clients
      schedule
      cra
    ] |> ProjectList

let deployDir = "../../deploy"

Target.initEnvironment ()

Target.create "Clean" (Targets.Clean.run deployDir)

Target.create "Build" (Targets.Build.run projects)

Target.create "Tests" (
    Targets.Tests.run projects Args.project Args.skipTests Args.withTestReport Args.withCoverageReport)

Target.create "Install" (
    Targets.Install.run projects Args.project Args.all Args.force Args.yes Args.out EnvironmentVariables.LocalAppData EnvironmentVariables.T_TOOLS    
)

Target.create "Uninstall" (
    Targets.Uninstall.run projects Args.project Args.all EnvironmentVariables.T_TOOLS
)           

let dependencies =
    [ "Clean" ==> "Build"; "Build" ?=> "Tests"; "Tests" ==> "Install" ]

[<EntryPoint>]
let main args = runOrDefault args
