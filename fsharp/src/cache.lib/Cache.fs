﻿namespace cache.lib

open System
open System.IO
open System.Security.Cryptography
open System.Text
open System.Text.Json

module Cache =
    
    type Item = {
        path:string
        key:string
        age:TimeSpan
        ttl:TimeSpan
    } with
        static member FromFile(filePath:string) =
            let key = Path.GetFileNameWithoutExtension(filePath).ToLower()
            let ttlFile = Path.ChangeExtension(filePath, ".ttl")
            let ttl = File.ReadAllText(ttlFile) |> TimeSpan.Parse
            let age = File.GetCreationTime(filePath) |> DateTimeOffset.Now.Subtract
            { path=filePath; key = key; age = age; ttl = ttl }
        static member FromKey(cachePath:string, key:string) =

            let filePath = Path.Combine(cachePath, $"{key}.json")
            
            let age = if File.Exists(filePath) then File.GetCreationTime(filePath) |> DateTimeOffset.Now.Subtract else TimeSpan.Zero

            let ttlFile = Path.ChangeExtension(filePath, ".ttl")
            let ttl = if File.Exists(ttlFile) then File.ReadAllText(ttlFile) |> TimeSpan.Parse else TimeSpan.Zero

            { path=filePath; key = key; age = age; ttl = ttl }
            
        member this.exists = File.Exists(this.path)
        member this.expired = this.age > this.ttl
        
        member this.SetContent<'a> (content:'a, ?options: JsonSerializerOptions) =
            let opt = options |> Option.defaultValue null
            let content = JsonSerializer.Serialize<'a>(content, opt)
            
            File.WriteAllText(this.path, content)
            let ttlFile = Path.ChangeExtension(this.path, ".ttl")
            File.WriteAllText(ttlFile, this.ttl.ToString())
            
        member this.GetContent<'a> (?options: JsonSerializerOptions) =
            let rawContent = File.ReadAllText(this.path)
            let opt = options |> Option.defaultValue null
            JsonSerializer.Deserialize<'a>(rawContent, opt)

        member this.GetRawContent () =
            File.ReadAllText(this.path)
            
        member this.Delete () =
            if this.exists then
                File.Delete(this.path)
                
                let ttlFile = Path.ChangeExtension(this.path, ".ttl")
                if File.Exists(ttlFile) then 
                    File.Delete(ttlFile)

    type Container = {
        path : string
    }
    let private directoryName = "cache"
        
    let private home = lazy Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)
       
    let private computeHash (directoryPath:string) =
        use hasher = SHA256.Create()
        let bytes = Encoding.UTF8.GetBytes(directoryPath)
        let hashBytes = hasher.ComputeHash(bytes)
        BitConverter.ToString(hashBytes).Replace("-", "").ToLower()
        
    let ``for`` directory =
        let hash = directory |> computeHash
        let path = Path.Combine(home.Value, "tools", directoryName, hash)

        {
            path = path
        }
        
    let ``global`` = ``for`` home.Value 
        
    let build isGlobal =
        if isGlobal then ``global`` else ``for`` (Directory.GetCurrentDirectory()) 
        
    let private exists cache =
        Directory.Exists(cache.path)

    let ensureExists cache =
        if not (cache |> exists) then Directory.CreateDirectory(cache.path) |> ignore
    
    let items cache : Item list =
        match (cache |> exists) with
        | false -> []
        | true ->
            Directory.EnumerateFiles(cache.path) 
            |> Seq.filter (fun file -> Path.GetExtension(file) = ".json" || Path.GetExtension(file) = ".cache")
            |> Seq.map Item.FromFile
            |> Seq.toList
            
    let tryGet cache key = 
        let item = Item.FromKey(cache.path, key)
        if item.exists then Some item else None
        
    let item cache key =
        Item.FromKey(cache.path, key)       
    
    type Result<'a> =
    | Value of 'a
    | Skipped of 'a
        
    let getOrAddWithOptions<'a> (jsonSerializationOptions:JsonSerializerOptions) key ttl (fetch:unit -> Result<'a>) cache =
        let item = Item.FromKey(cache.path, key)
        match (item.exists, item.expired) with
        | false, _             
        | true, true ->
            let content = fetch()
            match content with
            | Value content ->
                cache |> ensureExists
                { item with ttl = ttl }.SetContent<'a>(content, jsonSerializationOptions)
                content
            | Skipped content ->
                content
        | _ ->
            let content = item.GetContent<'a>(jsonSerializationOptions)
            content
            
    let getOrAdd<'a> key ttl (fetch:unit -> Result<'a>) cache =
        let item = Item.FromKey(cache.path, key)
        match (item.exists, item.expired) with
        | false, _             
        | true, true ->
            let content = fetch()
            match content with
            | Value content ->
                cache |> ensureExists
                { item with ttl = ttl }.SetContent<'a>(content)
                content
            | Skipped content ->
                content
        | _ ->
            let content = item.GetContent<'a>()
            content
            
            
    let addOrUpdateWithOptions<'a>
        (jsonSerializationOptions:JsonSerializerOptions)
        key ttl
        (update:'a -> Result<'a>)
        (create:unit -> 'a)
        cache =
        
        
        
        let item = Item.FromKey(cache.path, key)
        let content =
            if item.exists then
                let oldContent = item.GetContent<'a>(jsonSerializationOptions)
                update(oldContent)
            else
                create() |> Value
            
        match content with
        | Value content ->
            cache |> ensureExists
            { item with ttl = ttl }.SetContent<'a>(content, jsonSerializationOptions)
            content
        | Skipped content ->
            content
            
        
            
    let addOrUpdate<'a>
        key ttl
        (update:'a -> Result<'a>) (create:unit -> 'a)
        cache =
        
        let item = Item.FromKey(cache.path, key)
        let content =
            match (item.exists, item.expired) with
            | false, _             
            | true, true ->
                let content = item.GetContent<'a>()
                update(content)
            | _ ->
                create() |> Value
            
        match content with
        | Value content ->
            cache |> ensureExists
            { item with ttl = ttl }.SetContent<'a>(content)
            content
        | Skipped content ->
            content
