﻿open clients
open clients.Commands

open backbone
open backbone.cli.Cli.App
open backbone.cli.Cli.Branch

let set (set:Branch<ProgramResult, Set.BranchSettings>) =
    branch set {
        cmd "email" Set.Email.run
    }

let mission (set:Branch<ProgramResult, Missions.BranchSettings>) =
    branch set {
        cmd "list" Missions.List.run

        cmd "start" Missions.Start.run
        cmd "end" Missions.End.run

        cmd "tj" Missions.Tj.run
        
        
    }

[<EntryPoint>]
let main args =
    app args {
        interpreter Interpreter.interpret

        cmd "add" Add.run
        cmd "current" Current.run
        cmd "list" List.run
        cmd "archive" Archive.run
        
        branch "set" set
        branch "mission" mission
    }