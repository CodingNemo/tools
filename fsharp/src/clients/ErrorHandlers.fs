﻿namespace clients

open Spectre.Console
open backbone.DataSource.Errors
open backbone.Errors
open clients.lib

type ErrorCodes =
| IO = -1
| Csv = -2
| ClientsSource = -3 
| Client    = -4
| ArgError = -5
| Mission = -6
| Unknown = 999

module ErrorHandlers =
    open backbone
    open backbone.IO
    
    let private handleFolderNotFound folderPath =
        AnsiConsole.MarkupLine($"[red]Folder {folderPath} was not found[/]")
        ErrorCodes.IO
    
    let private handleFileAccessError =
        function
        | FileAccessError.AccessDenied filePath ->
            AnsiConsole.MarkupLine($"[red]Access to file {filePath} denied[/]")
            ErrorCodes.IO
        | FileAccessError.InvalidPath (filePath, reason) ->
            AnsiConsole.MarkupLine($"[red]Path {filePath} is invalid : {reason}[/]")
            ErrorCodes.IO
        | FileAccessError.FolderNotFound folderPath ->
            handleFolderNotFound folderPath
            
    let private handleFolderAccessError =
        function
        | FolderAccessError.AccessDenied filePath ->
            AnsiConsole.MarkupLine($"[red]Access to folder {filePath} denied[/]")
            ErrorCodes.IO
        | FolderAccessError.InvalidPath (filePath, reason) ->
            AnsiConsole.MarkupLine($"[red]Path {filePath} is invalid : {reason}[/]")
            ErrorCodes.IO
            
    let private handleFolderDeletionError =
         function
         | FolderDeletionError.FolderNotFound folderPath ->
            handleFolderNotFound folderPath
         | FolderDeletionError.FolderAccess folderAccess ->
             handleFolderAccessError folderAccess
        
    let private handleFileCreationError =
        function
        | FileCreationError.FileAccess fileAccessError ->
            handleFileAccessError fileAccessError
        | FileCreationError.FileAlreadyExists filePath ->
            AnsiConsole.MarkupLine($"[red]File {filePath} already exists[/]")
            ErrorCodes.IO
    
    let private handleFolderCreationError =
        function
        | FolderCreationError.FolderAccess folderAccessError ->
            handleFolderAccessError folderAccessError
        | FolderCreationError.FolderAlreadyExists folderPath ->
            AnsiConsole.MarkupLine($"[red]File {folderPath} already exists[/]")
            ErrorCodes.IO
        | FolderCreationError.ParentFolderNotFound folderPath ->
            handleFolderNotFound folderPath

    let private handleInvalidCsv filePath error =
        AnsiConsole.MarkupLine($"[red]Error parsing csv file {filePath} : {error}[/]")
        ErrorCodes.Csv
    
    let private handleFileSavingError =
        function
        | FileSavingError.FileAccess fileAccessError ->
            handleFileAccessError fileAccessError
        | FileCreation fileCreationError ->
            handleFileCreationError fileCreationError
        | FileSavingError.InvalidFileContent (filePath, error) ->
            handleInvalidCsv filePath error
        | FileSavingError.FileNotFound filePath ->
            AnsiConsole.MarkupLine($"[red]File {filePath} not found[/]")
            ErrorCodes.Csv

    let handleFileParsingError =
        function
        | FileParsingError.FileAccess fileAccessError -> handleFileAccessError fileAccessError
        | FileParsingError.FileSave csvFileSavingError -> handleFileSavingError csvFileSavingError
        | FileParsingError.InvalidFileContent(filePath, error) -> handleInvalidCsv filePath error
    
    let private handleUnsupportedFileExtension extension =
        AnsiConsole.MarkupLine($"[red]Unsupported extension {extension}[/]")
        ErrorCodes.ClientsSource

    let private handleDataSourceLoadingError (_:DataSourceLoadingError) =
        AnsiConsole.MarkupLine("[red]Error loading data source[/]") // TODO: add more details in the error to make a difference between clients source and missions source
        ErrorCodes.ClientsSource
    
    let private handleFindingClientError =
        function
        | FindingBySlugOrIdError.ClientNotFound slugOrId ->
            match slugOrId with
            | Id clientId ->
                AnsiConsole.MarkupLine($"[red]Client with id {clientId.Value} does not exist[/]")
                ErrorCodes.Client
            | Slug slug ->
                AnsiConsole.MarkupLine($"[red]No client matching slug {slug.value} exist[/]")
                ErrorCodes.Client
        | FindingBySlugOrIdError.TooManyClientsFound (slug, clientsCount) ->
            AnsiConsole.MarkupLine($"[red]{clientsCount} clients matching slug {slug.value} found[/]")
            ErrorCodes.Client
    
    let private handleFindingActiveClientError =
        function
        | FindingActiveClientError.NoActiveClients asOfDate ->
            AnsiConsole.MarkupLine($"[red]No active clients as of {asOfDate}[/]")
        | FindingActiveClientError.ClientIsNotActive (client, asOfDate, clientStatus) ->
            AnsiConsole.MarkupLine($"[red]Client {client.Name} is not active as of {asOfDate}. Status : {clientStatus}[/]")
    
    let private handleFolderArchiveError =
        function     
        | FolderNotFound (folderPath, _) ->
            handleFolderNotFound folderPath
        | FolderAccess folderAccessError ->
            handleFolderAccessError folderAccessError    
        | ParentFolderNotFound rootedFolderPath ->
            handleFolderNotFound rootedFolderPath
    
    let private handleArgError =
        function
        | ArgError.InvalidArg (argName, Some reason) ->
            AnsiConsole.MarkupLine($"[red]Invalid arg '{argName}' :{reason}[/]")
            ErrorCodes.ArgError
        | ArgError.InvalidArg (argName, None) ->
            AnsiConsole.MarkupLine($"[red]Invalid arg '{argName}'[/]")
            ErrorCodes.ArgError
    
    
    let private handleAddingClientError =
        function
        | ClientWithTheSameIdFound foundClient ->
            AnsiConsole.MarkupLine($"[red]Client '{foundClient.Name}' already exists with the id '{foundClient.Id}'[/]")
        | ClientWithTheSameNameFound foundClient ->
            AnsiConsole.MarkupLine($"[red]Client '{foundClient.Slug}' already exists with the name '{foundClient.Name}'[/]")
        | ClientsWithTheSameSlugFound (slug, clients) ->
            match clients with
            | [] ->
                failwith "clients cannot be empty in this error case"
            | [client] when client.Slug = slug ->
                AnsiConsole.MarkupLine($"[red]Client '{client.Name}' already exists with the slug {client.Slug} [/]")
            | [client] when client.Slug = slug ->
                AnsiConsole.MarkupLine($"[red]Client '{client.Name}' already exists and has a slug starting with {slug} : {client.Slug} [/]")
            | _ ->
                let table = Table()
                table.AddColumns("Name", "Slug") |> ignore
                
                for client in clients do
                    table.AddRow($"{client.Name}", $"{client.Slug}") |> ignore
                
                AnsiConsole.MarkupLine($"[red]{clients.Length} clients already exists with a slug starting with '{slug}'[/]")
                AnsiConsole.Write(table)
                AnsiConsole.WriteLine()
            AnsiConsole.MarkupLine("[red]Make sure slug is unique and not included at the start of other clients slug[/]")
            
    let private handleArchivingClientError =
        function
        | ArchivingClientError.ClientIsStillActive slug ->
            AnsiConsole.MarkupLine($"[red]Client {slug} is still active. Please end all missions before archiving the client[/]")
    let handleMissionError =
        function
        | MissionError.MissionAlreadyStarted (missionId, startDate) ->
            AnsiConsole.MarkupLine($"[red]Mission {missionId} already started on {startDate}[/]")
            ErrorCodes.Mission
        | MissionError.NoMissionStarted ->
            AnsiConsole.MarkupLine($"[red]No pending mission to end[/]")
            ErrorCodes.Mission
    
    let handleStartingMissionError =
        function
        | StartingMissionError.FindClientError slugOrId ->
            handleFindingClientError slugOrId
        | StartingMissionError.MissionError missionError ->
            handleMissionError missionError
            
    
    let handleError (err:IError) =
        match err with
        | :? ArgError as ae ->
            handleArgError ae

        | :? FileParsingError as fpe ->
            handleFileParsingError fpe
        | :? FileSavingError as fse ->
            handleFileSavingError fse
            
        | :? DataSourceLoadingError as csle ->
            handleDataSourceLoadingError csle
            
        | :? FolderCreationError as foce ->
            handleFolderCreationError foce
        | :? FolderAccessError as foae ->
            handleFolderAccessError foae
        | :? FolderDeletionError as fde ->
            handleFolderDeletionError fde
        | :? FolderArchiveError as fae ->
            handleFolderArchiveError fae
        | :? FileAccessError as fiae ->
            handleFileAccessError fiae
        | :? FindingBySlugOrIdError as fbsoie ->
            handleFindingClientError fbsoie
        | :? FindingActiveClientError as fae ->
            handleFindingActiveClientError fae
            ErrorCodes.Client
        
        | :? AddingClientError as ace ->
            handleAddingClientError ace 
            ErrorCodes.Client
        
        | :? ArchivingClientError as ace ->
            handleArchivingClientError ace 
            ErrorCodes.Client
        
        | :? StartingMissionError as se ->
            handleStartingMissionError se
        | _ ->
            AnsiConsole.MarkupLine($":skull_and_crossbones: [bold red on white]UNEXPECTED ERROR OF TYPE {err.GetType()}[/] :skull_and_crossbones:")
            ErrorCodes.Unknown