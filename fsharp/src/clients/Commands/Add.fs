﻿namespace clients.Commands

open Spectre.Console.Cli

open backbone
open backbone.DataSource
open backbone.DataSource.Instructions
open backbone.IO
open backbone.IO.Instructions
open backbone.Program

open clients.lib

module Add =

    type Settings() =
        inherit CommandSettings()
        
        [<CommandArgument(0, "<NAME>")>]
        member val Name : string = "" with get, set
        
        [<CommandOption("-i|--id")>]
        member val Id : string = "" with get, set
        
        [<CommandOption("-s|--slug")>]
        member val Slug : string = "" with get, set
                
        [<CommandOption("-e|--email")>]
        member val Email : string = "" with get, set
                
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
            
    let run (settings:Settings) =
        resPgm {
            let! location = DataSource.Location.TryParse(settings.ClientsLocation)
            let! source = DataSource.find<Clients>(location)
            let! clients = source.Read()
            
            let name = ClientName.Create settings.Name
            let id = ClientId.TryParse settings.Id
            let email = Email.TryParse settings.Email
            let slug = Slug.TryParse settings.Slug
        
            let client = Client.Create id slug name email
            let! newClients = clients.Add(client) |> lift
            
            let folderName : FolderName = client.Slug.value
            let folder = source.Folder / folderName            
            do! folder.Create()

            do! source.Store(newClients)
        }
