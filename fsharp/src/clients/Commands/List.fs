﻿namespace clients.Commands

open Spectre.Console
open Spectre.Console.Cli

open Spectre.Console.Rendering
open backbone
open backbone.DataSource
open backbone.DataSource.Instructions
open backbone.Dates
open backbone.Program
open clients.lib
open backbone.IO

module List =
    type Settings() =
        inherit CommandSettings()   
        
        [<CommandOption("-a|--all")>]
        member val All : bool = false with get, set
        
        [<CommandOption("--as-of")>]
        member val AsOf : string = "" with get,set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
   
    let displayClients clients asOfDate clientsLocation =
        let table = Table() 
                        .AddColumns("Id", "Name", "Slug", "Email", "Status")
                        
        clients
        |> Seq.toList
        |> List.iter (fun client ->
            table.AddRow([|
                Markup(client.Id.ToString()) :> IRenderable
                Markup(client.Name.Value)
                Markup(client.Slug.value)
                Markup(
                       match client.Email with
                       | Some email -> email.Address
                       | None -> ""
                )
                Markup(client.Status(asOfDate).ToString())           
            |]) |> ignore
            )

        AnsiConsole.Write(table)
    let private filterClient (all:bool) (asOfDate:AsOfDate) (client:Client) =
        if all then
            true
        else
            client.Status(asOfDate) <> ClientStatus.Archived    
            
    let run (settings:Settings) =
         resPgm {
             let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
             let! asOfDate = settings.AsOf
                             |> AsOfDate.ParseOrDefault 
                             |> lift
             let! source = DataSource.find<Clients>(clientsLocation)
             let! clients = source.Read()
                        
             let displayedClients = clients
                                    |> Seq.filter (filterClient settings.All asOfDate)
                                    |> Seq.toList
             
             displayClients displayedClients asOfDate clientsLocation
         }
