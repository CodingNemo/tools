﻿namespace clients.Commands

open Spectre.Console
open Spectre.Console.Cli

open backbone.DataSource.DataSource
open backbone.DataSource.Instructions
open backbone.Dates
open backbone.IO
open backbone.Program
open clients.lib
open clients.lib.Instructions

module Current =
    type Settings() =
        inherit CommandSettings()   
                
        [<CommandOption("--as-of")>]
        member val AsOf : string = "" with get,set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
        
    let displayFolderPath (folderPath:RelativeFolderPath) =
        TextPath(folderPath.ToString())
    
    let displayClient (asOfDate:AsOfDate) (client:Client)  =
        let table = Table()
                        .AddColumns("PropertyName", "PropertyValue")
                        .HideHeaders()
                        .NoBorder()
                        
        table
            .AddRow("name", $"{client.Name}")
            .AddRow("slug", $"{client.Slug}")
            .AddRow("id", $"{client.Id}") |> ignore
        
        let email = client.Email |> Option.map(_.ToString())
                                 |> Option.defaultValue("")
        
        table.AddRow("email", email) |> ignore
        
        let status = client.Status(asOfDate)
        let status = match status with
                               | Active -> Markup($"[green]{status}[/]")
                               | Inactive -> Markup($"[yellow]{status}[/]")
                               | Archived -> Markup($"[gray]{status}[/]")
        table.AddRow(Markup("status"), status) |> ignore
        
        let currentMission = client.GetCurrentMission(asOfDate)
        
        match currentMission with
        | Some mission ->
            let missionName = mission.title
                              |> Option.map(_.ToString())
                              |> Option.defaultValue($"#{mission.id.index}")
            table.AddRow("current mission", $"{missionName} since {mission.startDate}") |> ignore
        | None -> ()
        
        match client.ArchiveDate with
        | Some date ->
            table.AddRow("archived on", $"{date}") |> ignore
        | None -> ()
                     
        let panel = Panel(table)
        panel.Header <- PanelHeader("client")
        AnsiConsole.Write(panel)
    
    let run (settings:Settings) =
         resPgm {
             let! clientsLocation = Location.TryParse(settings.ClientsLocation)
             let! asOfDate = settings.AsOf
                             |> AsOfDate.ParseOrDefault 
                             |> lift
             let! source = DataSource.find<Clients>(clientsLocation)
             let! clients = source.Read()
             
             let! current = clients.FindCurrentlyActiveClient(asOfDate)
             
             current |> displayClient asOfDate
         }
