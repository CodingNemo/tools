﻿namespace clients.Commands

open backbone
open backbone.DataSource
open backbone.IO
open backbone.Program
open backbone.DataSource.Instructions

open Spectre.Console.Cli

open clients.lib

module Set =
    
    type BranchSettings() =
        inherit CommandSettings()
        
        [<CommandArgument(0, "[SLUG-OR-ID]")>]
        member val SlugOrId : string = "" with get,set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set

    module Email =
        
        type Settings() =
            inherit BranchSettings()
            
            [<CommandArgument(0, "[EMAIL]")>]
            member val Email : string = "" with get,set
        
        let run (settings:Settings) =
            resPgm {
                let! slugOrId = SlugOrId.parse settings.SlugOrId |> lift
                let email = Email.Parse settings.Email
                
                let! dataSourceQuery = DataSource.Location.TryParse(settings.ClientsLocation)
                let! source = DataSource.find<Clients>(dataSourceQuery)
                let! clients = source.Read()
                
                let! newClients = clients.SetEmail(slugOrId, email)
                
                do! source.Store(newClients)
            }
