﻿namespace clients.Commands.Missions

open Spectre.Console
open Spectre.Console.Cli
open backbone
open clients.lib

module Tj =
    open backbone.Program
    open backbone.DataSource
    open backbone.DataSource.Instructions
        
    type Settings() =
        inherit BranchSettings()
        
        [<CommandArgument(0, "<NEW-TJ>")>]
        member val NewTj : int = 0 with get,set
        
        [<CommandOption("--as-of")>]
        member val AsOf : string = "" with get,set
        
        [<CommandOption("-t|--new-title")>]
        member val NewTitle : string = "" with get,set

    let run (settings:Settings) =
        resPgm {
            let! updateDate = UpdateDate.ParseOrDefault settings.AsOf
                             |> lift
            let! slugOrId = SlugOrId.parse settings.SlugOrId |> lift
            let newTjm = settings.NewTj
            
            let newTitle = settings.NewTitle |> MissionTitle.TryParse

            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            let! clients = clientsSource.Read()

            let! newClients, (endedMission, startedMission) = clients.UpdateTj(slugOrId, newTjm, updateDate, newTitle) |> lift
                           
            do! clientsSource.Store(newClients)
            
            AnsiConsole.MarkupLine($"Tj was updated from [yellow]{endedMission.tj}[/] to [green]{startedMission.tj}[/]")
            AnsiConsole.MarkupLine($"Mission [blue]#{endedMission.id.index}[/] with tj {endedMission.tj} ended on {endedMission.endDate |> Option.get}")
            AnsiConsole.MarkupLine($"A new mission [blue]#{startedMission.id.index}[/] with tj {startedMission.tj} was started on {startedMission.startDate}")
            
        }

