﻿namespace clients.Commands.Missions

open System.ComponentModel
open Spectre.Console
open Spectre.Console.Cli
open backbone
open backbone.Dates
open clients.lib

module End =
    open backbone.Program
    open backbone.DataSource
    open backbone.DataSource.Instructions
    
    type Settings() =
        inherit BranchSettings()
        
        [<CommandArgument(0, "<END-DATE>")>]
        [<Description("format : yyyy-MM-dd")>]
        member val EndDate : string = "" with get,set

    let run (settings:Settings) =
        resPgm {
            let! endDate = settings.EndDate
                           |> EndDate.ParseOrDefault
                           |> lift
            
            let! slugOrId = SlugOrId.parse settings.SlugOrId |> lift
            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            let! clients = clientsSource.Read()

            let! newClients, endedMission = clients.EndCurrentMission(slugOrId, endDate) |> lift
            
            do! clientsSource.Store(newClients)
             
            AnsiConsole.MarkupLine($"Mission [blue]#{endedMission.id.index}[/] for client with {slugOrId} ended on {endDate}")
        }

