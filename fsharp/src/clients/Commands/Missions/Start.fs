﻿namespace clients.Commands.Missions

open System.ComponentModel
open Spectre.Console
open Spectre.Console.Cli
open backbone
open clients.lib

module Start =
    open backbone.Program
    open backbone.DataSource
    open backbone.DataSource.Instructions    
    type Settings() =
        inherit BranchSettings()
        
        [<CommandArgument(0, "<TJ>")>]
        member val Tj : int = 0 with get, set
        
        [<CommandArgument(1, "[START-DATE]")>]
        [<Description("format : yyyy-MM-dd")>]
        member val StartDate : string = "" with get,set
        
        [<CommandOption("-t|--title")>]
        member val Title : string = "" with get,set
        
    let run (settings:Settings) =
        resPgm {
            let tj = settings.Tj
            let! startDate = settings.StartDate
                            |> StartDate.ParseOrDefault 
                            |> lift
            let title = settings.Title |> MissionTitle.TryParse
            
            let! slugOrId = SlugOrId.parse settings.SlugOrId |> lift
            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            let! clients = clientsSource.Read()

            let! newClients, newMission = clients.StartMission(slugOrId, startDate, tj, title) |> lift
            
            do! clientsSource.Store(newClients)
            
            AnsiConsole.MarkupLine($"New mission [blue]#{newMission.id.index}[/] started for client with {slugOrId}")
        }
