﻿namespace clients.Commands.Missions

open Spectre.Console.Cli

type BranchSettings() =
    inherit CommandSettings()
    
    [<CommandArgument(0, "[SLUG-OR-ID]")>]
    member val SlugOrId : string = "" with get,set
    
    [<CommandOption("--clients-location", IsHidden = true)>]
    member val ClientsLocation : string = "" with get, set


