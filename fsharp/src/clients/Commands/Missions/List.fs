﻿namespace clients.Commands.Missions

open Spectre.Console
open Spectre.Console.Cli
open Spectre.Console.Rendering
open backbone
open backbone.Dates
open clients.lib

module List =
    open backbone.Program
    open backbone.DataSource
    open backbone.DataSource.Instructions
    
    type Settings() =
        inherit BranchSettings()
        
        [<CommandOption("--as-of")>]
        member val AsOf : string = "" with get,set
    
    let private displayMissions (asOfDate:AsOfDate) (missions: Mission list) =
        let table = Table().AddColumns("Id", "Title", "Start", "End", "TJ", "Status")
        for mission in missions do
            let status = mission.Status(asOfDate)
            table.AddRow([|
                Markup($"#{mission.id.index}") :> IRenderable
                Markup(mission.title
                       |> Option.map (_.ToString())
                       |> Option.defaultValue "")
                Markup(mission.startDate.ToString())
                match mission.endDate with
                | Some end' -> Markup(end'.ToString())
                | None -> Markup(":black_circle:")
                Markup(mission.tj.ToString())
                match status with
                | Ended -> Markup(":red_circle:")
                | Pending -> Markup(":counterclockwise_arrows_button:")
                | NotStarted -> Markup(":black_circle:")
            |]) |> ignore
            
        AnsiConsole.Write(table)

    let run (settings:Settings) =
        resPgm {
            let! asOfDate = settings.AsOf
                            |> AsOfDate.ParseOrDefault 
                            |> lift
            let! slugOrId = SlugOrId.parse settings.SlugOrId |> lift
            let! clientsLocation = DataSource.Location.TryParse(settings.ClientsLocation)
            let! clientsSource = DataSource.find<Clients>(clientsLocation)
            let! clients = clientsSource.Read()

            let! client = clients.FindBySlugOrId slugOrId |> lift
            
            client.Missions |> displayMissions asOfDate
        }
