﻿namespace clients.Commands

open Spectre.Console
open Spectre.Console.Cli

open backbone
open backbone.DataSource
open backbone.DataSource.Instructions
open backbone.IO
open backbone.IO.Instructions
open backbone.Program

module Archive =
    open clients.lib

    type Settings() =
        inherit CommandSettings()
        
        [<CommandArgument(0, "<SLUG-OR-ID>")>]
        member val SlugOrId : string = "" with get,set
        
        [<CommandArgument(1, "[ARCHIVE-DATE]")>]
        member val ArchiveDate : string = "" with get, set
        
        [<CommandOption("--clients-location", IsHidden = true)>]
        member val ClientsLocation : string = "" with get, set
        
                
        
    let run (settings:Settings) =
        resPgm {
           let! dataSourceQuery = DataSource.Location.TryParse(settings.ClientsLocation)
           let! archiveDate = ArchiveDate.ParseOrDefault(settings.ArchiveDate)
                              |> lift
           let! source = DataSource.find<Clients>(dataSourceQuery)
           let! clients = source.Read()
           
           let! slugOrClientId = SlugOrId.parse(settings.SlugOrId) |> lift
           let! client = clients.FindBySlugOrId slugOrClientId |> lift
           
           let! newClients = clients.Archive(client, archiveDate)
           
           let folderName : FolderName = client.Slug.value
           let folderPath = source.Folder / folderName
           
           do! folderPath.Archive()
           do! folderPath.Delete()
           
           do! source.Store(newClients)
           
           AnsiConsole.MarkupLine($"Client with {slugOrClientId} archived")
        }
