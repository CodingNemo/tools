﻿namespace clients

open clients.lib
    
module Interpreter =
    let interpret p =
        p |> Interpreters.interpret
          |> Result.map (fun () -> 0)
          |> Result.defaultWith (fun err ->
            let code = ErrorHandlers.handleError err
            int code 
            )