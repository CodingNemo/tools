﻿namespace client.FreeMonad

type Free<'f, 'a> =
    | Pure of 'a
    | Impure of 'f<Free<'f, 'a>>



module FreeMonad =
    ()

