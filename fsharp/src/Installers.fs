﻿module Installers

open Console
open Helpers
open Definitions
open Fake.Core
open Fake.IO


let generateLink alias original dir =
    if Environment.isWindows then
          runMklink alias original dir
    else
          ln alias original dir |> Proc.run |>  ignore
                
let createAlias (alias:string) (original:string) directory =
    writePending $"Creating alias '{alias}' for '{original}'"

    try                    
        generateLink alias original directory
        writeSuccess $"Alias '{alias}' successfully created"        
    with
    | ex ->
        writeError $"Alias '{alias}' creation failed"
        writeException ex

let createProjectAliases  (projectInstallDir:string) (project:Project) =
    match project.aliases with
    | [] -> ()
    | aliases -> 
   
        writePending $"Creating {aliases.Length} aliases for '{project.name}'"
        
        for alias in aliases do 
            createAlias alias project.name projectInstallDir
            
let addToPathEnv (newPath:string) = 
    writePending "Updating Path"
    if EnvironmentVariables.Path.Add(newPath) then 
        writeSuccess "Path updated"
    else
        writeInfo "Path already up to date"

let removeFromPathEnv (path:string) = 
    writePending "Updating Path"
    if EnvironmentVariables.Path.Remove(path) then 
        writeSuccess "Path updated"
    else
        writeInfo "Path already up to date"

let defaultExeInstall (project:Project) (installDir:string) (force:bool) (yes:bool)=
    if project.IsInstalledIn(installDir) && not(force) then
        writeWarning $"{project} already installed"
    else 
        writePending $"Installing {project}"
        try
            let projectInstallDirPath = $"{installDir}" </> $"{project.path}"
                  
            let result = runWithResult dotnet $"publish -c Release -o %s{projectInstallDirPath} --self-contained" project.path
            match result.ExitCode with
            | 0 ->
                projectInstallDirPath |> addToPathEnv
                project |> createProjectAliases projectInstallDirPath 
                
                writeSuccess $"Project {project} successfully installed"

                match project.children with
                | [] -> ()
                | children -> 
                    writePending $"Installing {children.Length} children of {project}"
                    for child in children do
                        child.install |> Option.iter (fun install -> install child installDir force yes)
            | _ ->
                writeError $"Project {project} installation failed with exit code {result.ExitCode}"
        with
        | ex ->
            writeError $"Project {project} installation failed"
            writeException ex

let defaultExeUninstall (project:Project) (path:string) =
    writePending $"Uninstalling {project}"
    try
        match project.children with
        | [] -> ()
        | children -> 
            writePending $"Uninstalling {children.Length} children of {project}"
            for child in children do
                child.uninstall |> Option.iter (fun uninstall -> uninstall child path)

        let path = $"{path}" </> $"{project.path}"
        if not(Directory.exists path) then
            writeError $"{project} was not installed"
        else 
            Directory.delete path
            path |> removeFromPathEnv
            writeSuccess $"Project {project} successfully uninstalled"    

    
    with
    | ex ->
        writeError $"Project {project} uninstallation failed"
        writeException ex