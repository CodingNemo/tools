﻿namespace schedule.lib

open System
open System.Collections.Generic
open System.Diagnostics
open backbone
open backbone.Dates
open backbone.DataSource
open backbone.Errors

open backbone.Program

open backbone.Result
open clients.lib


open backbone.DataSource.Instructions
open backbone.Dates.Date

module Schedule =
    
    type MonthReference = {
        month: Month
        year: Year
    }
        with
            
            member this.GetDate(day:Day) =
                date(day, this.month, this.year)
            
            member this.Days() =
                this.month.Days(this.year)
                |> List.map (fun day -> date(day, this.month, this.year))
            
            member this.Start() =
                this.GetDate(Day.From(1<days>))

            static member Current(asOfDate:AsOfDate) =
                {
                    month= asOfDate.date.month
                    year = asOfDate.date.year
                }
                
    module private MonthReference =
        open FParsec  
        let monthYearParser =
            parse {
                let! month = Month.Parser
                let! year = Year.Parser
                return {
                    month = month
                    year = year
                }
            }
            
        let parse text =
            match run monthYearParser text with
            | Success(result, _, _) -> Result.Ok result
            | ParserResult.Failure(errorAsString, _, _) ->
                Result.Error errorAsString

        let serialize (monthReference:MonthReference) =
            $"{monthReference.month}{monthReference.year.ToShortString()}"

    type MonthReference with
        static member Parse(text:string) : Result<MonthReference, ArgError> =
            if text.Length > 6 then
                Error (ArgError.InvalidArg("MonthReference", Some $"{text} is too long"))
            else 
                text
                |> MonthReference.parse
                |> Result.mapError (fun error ->
                    ArgError.InvalidArg("MonthReference", Some error)
                )
        
        static member ParseOfDefault(text:string, asOfDate:AsOfDate) =
            if String.IsNullOrEmpty text then
                Ok(MonthReference.Current(asOfDate))
            else
                text
                |> MonthReference.Parse
        
        member this.Display() =
            MonthReference.serialize this
                
    type QuarterReference = Quarter * Year
    
    type Reference =
        | ForMonth   of MonthReference
        | ForQuarter of QuarterReference
        | ForYear    of Year
    
    module Reference =
        open FParsec         
           
        let private monthYearParser =
            parse {
                let! monthReference = MonthReference.monthYearParser
                return Reference.ForMonth monthReference
            }
            
        let private quarterYearParser =
            parse {
                let! quarter = Quarter.Parser
                let! year = Year.Parser
                return Reference.ForQuarter(quarter, year)
            }

        let private yearOnlyParser =
            parse {
                do! skipChar 'Y'
                let! year = Year.Parser
                return Reference.ForYear year
            }
            
        let parser =
            attempt yearOnlyParser <|> attempt quarterYearParser <|> monthYearParser
    
        let parse text =
            match run parser text with
            | Success(result, _, _) -> Result.Ok result
            | ParserResult.Failure(errorAsString, _, _) ->
                Result.Error errorAsString
        
        let serialize reference =
            match reference with
            | ForMonth monthReference ->
                MonthReference.serialize monthReference
            | ForQuarter(quarter, year) ->
                $"{quarter}{year.ToShortString()}"
            | ForYear year -> $"Y{year.ToShortString()}"
    
    type Reference with
        static member CurrentQuarter(asOfDate: AsOfDate) : Reference =
            Reference.ForQuarter (asOfDate.date.month.Quarter, asOfDate.date.year)
        static member CurrentMonth(asOfDate: AsOfDate) : Reference =
            MonthReference.Current(asOfDate) |> Reference.ForMonth 
        static member CurrentYear(asOfDate: AsOfDate) : Reference =
            Reference.ForYear asOfDate.date.year
        
        static member Default = Reference.CurrentMonth
        
        static member Parse(reference:string) : Result<Reference, ArgError> =
            if reference.Length > 5 then
                Error (ArgError.InvalidArg("Reference", Some $"{reference} is too long"))
            else 
                reference
                |> Reference.parse
                |> Result.mapError (fun error ->
                    ArgError.InvalidArg("Reference", Some error)
                )

        static member TryParse(reference:string) : Reference option =
            if String.IsNullOrEmpty reference then
                None
            else
                reference
                |> Reference.Parse
                |> Result.toOption
                
        static member ParseOrDefault(reference:string, asOfDate:AsOfDate) : Reference =
            reference
            |> Reference.TryParse
            |> Option.defaultWith (fun () -> Reference.CurrentMonth asOfDate)                    
                
        member this.Display() =
            Reference.serialize this

    type SetActivityTarget =
    | Off
    | Mission of MissionId
    | Client of SlugOrId<ClientId>
    with
        static member Parse(text:string) : Result<SetActivityTarget, ArgError> =
            if text.ToLowerInvariant() = "off" then
                Ok SetActivityTarget.Off
            else
                let missionId = MissionId.TryParse text
                match missionId with
                | Some missionId ->
                    SetActivityTarget.Mission(missionId) |> Ok
                | None ->
                    let slugOrId = SlugOrId.tryParse text
                    match slugOrId with
                    | Some slugOrId ->
                        SetActivityTarget.Client slugOrId |> Ok
                    | None ->
                        Error (ArgError.InvalidArg ("ActivityTarget", Some $"{text} is neither a missionId, a clientId or a client slug"))
    
    [<CustomComparison>]
    [<CustomEquality>]
    type ActivitySpan =
    | MorningOnly
    | AfternoonOnly
    | FullDay
     with
        static member Parse(text:string) : Result<ActivitySpan,ArgError> =
            match text.ToUpperInvariant() with
            | "F" | "FULL" | "X" -> Ok(ActivitySpan.FullDay)
            | "M" | "AM" | "MORNING" -> Ok(ActivitySpan.MorningOnly)
            | "A" | "PM" | "AFTERNOON" -> Ok(ActivitySpan.AfternoonOnly)
            | _ ->
                Error(ArgError.InvalidArg ("ActivitySpan", Some text))
                
        override this.GetHashCode() =
            HashCode.Combine(this)
        
        override this.Equals(other:obj) =
            match other with
            | :? ActivitySpan as activitySpan ->
                this.Equals(activitySpan)
            | _ -> false

        static member op_Equality (left:ActivitySpan, right:ActivitySpan) =
            left.Equals(right)
                    
        static member op_Inequality (left:ActivitySpan, right:ActivitySpan) =
            left.Equals(right) |> not
                    
        static member op_GreaterThan (left:ActivitySpan, right:ActivitySpan) =
            (left :> IComparable<ActivitySpan>).CompareTo(right) > 0
        static member op_GreaterThanOrEqual (left:ActivitySpan, right:ActivitySpan) =
            (left :> IComparable<ActivitySpan>).CompareTo(right) >= 0
        
        static member op_LessThan (left:ActivitySpan, right:ActivitySpan) =
            (left :> IComparable<ActivitySpan>).CompareTo(right) < 0
        static member op_LessThanOrEqual (left:ActivitySpan, right:ActivitySpan) =
            (left :> IComparable<ActivitySpan>).CompareTo(right) <= 0
        
        interface IEquatable<ActivitySpan> with
            member this.Equals(other:ActivitySpan) =
                match this, other with
                | MorningOnly, MorningOnly -> true
                | AfternoonOnly, AfternoonOnly -> true
                | FullDay, FullDay -> true
                | _ -> false
                
        interface IComparable<ActivitySpan> with
            member this.CompareTo(other) =
                match this, other with
                | MorningOnly, MorningOnly -> 0
                | AfternoonOnly, AfternoonOnly -> 0
                | FullDay, FullDay -> 0
                | MorningOnly, _ -> -1
                | AfternoonOnly, FullDay -> -1
                | AfternoonOnly, _ -> 1
                | FullDay, _ -> 1
        
        interface IComparable with
            member this.CompareTo(other) =
                match other with
                | :? ActivitySpan as activitySpan -> (this :> IComparable<ActivitySpan>).CompareTo(activitySpan)
                | _ -> 1

    type ActivityByDate = ActivitySpan option * Date
    
    type Holiday = {
        name: string
        date: Date
    }
    
    type HalfDayDailyActivity =
        | Off
        | Mission of MissionId
    
    type FullDayDailyActivity =
        | Off
        | Mission of MissionId
        | Holiday of string    
    
    type OverbookedDayDailyActivity =
        | Off
        | Mission of MissionId
        | Holiday of string
    
    type DailyActivities =
        | Full of FullDayDailyActivity
        | Half of HalfDayDailyActivity * HalfDayDailyActivity
        | Overbooked of (OverbookedDayDailyActivity * ActivitySpan) list
        with
            static member Empty = DailyActivities.Full(FullDayDailyActivity.Off)
            
    type AddableActivity =
        | Off
        | Mission of MissionId
        
    type AddActivityError =
        | CannotSetHoliday of string * Date
        interface IError
    
    type ActivityDay =
        {
            activities: DailyActivities
            date: Date
        }
           with
                static member Empty(date:Date) =
                    {
                        date = date
                        activities = DailyActivities.Empty
                    }
                    
                member private this.AddOff(activitySpan:ActivitySpan) : Result<DailyActivities, AddActivityError> =
                    match activitySpan with
                    | ActivitySpan.FullDay ->
                        match this.activities with
                        | Full(FullDayDailyActivity.Holiday name) ->
                            Error (CannotSetHoliday (name, this.date))
                        | _ ->
                            Full(FullDayDailyActivity.Off) |> Ok
                           
                    | ActivitySpan.MorningOnly ->
                        match this.activities with
                        | Full(FullDayDailyActivity.Holiday name) ->
                            Error (CannotSetHoliday (name, this.date))
                        | Full(FullDayDailyActivity.Off) ->
                            Full(FullDayDailyActivity.Off) |> Ok
                        | Full(FullDayDailyActivity.Mission mission) ->
                            Half(HalfDayDailyActivity.Off, HalfDayDailyActivity.Mission mission) |> Ok
                        | Half(_, HalfDayDailyActivity.Off) ->
                            Full(FullDayDailyActivity.Off) |> Ok
                        | Half(_, other) ->
                            Half(HalfDayDailyActivity.Off, other) |> Ok
                        | Overbooked activities ->
                            let overbookedActivity = OverbookedDayDailyActivity.Off, ActivitySpan.MorningOnly
                            Overbooked (overbookedActivity::activities) |> Ok
                    | ActivitySpan.AfternoonOnly ->
                        match this.activities with
                        | Full(FullDayDailyActivity.Holiday name) ->
                            Full(FullDayDailyActivity.Holiday name) |> Ok
                        | Full(FullDayDailyActivity.Off) ->
                            Full(FullDayDailyActivity.Off) |> Ok
                        | Full(FullDayDailyActivity.Mission missionId) ->
                            Half(HalfDayDailyActivity.Mission missionId, HalfDayDailyActivity.Off) |> Ok
                        
                        | Half(HalfDayDailyActivity.Off, _) ->
                            Full(FullDayDailyActivity.Off) |> Ok
                        | Half(other, _) ->
                            Half(other, HalfDayDailyActivity.Off) |> Ok
                        
                        | Overbooked activities ->
                            let overbookedActivity = OverbookedDayDailyActivity.Off, ActivitySpan.AfternoonOnly
                            Overbooked (overbookedActivity::activities) |> Ok
                        
                            
                member private this.AddMission(missionId: MissionId, activitySpan:ActivitySpan) : Result<DailyActivities, AddActivityError> =
                   match activitySpan with
                    | ActivitySpan.FullDay ->
                        match this.activities with
                        | Full(FullDayDailyActivity.Holiday name) ->
                            Error (CannotSetHoliday (name, this.date))
                        | _ ->
                            Full(FullDayDailyActivity.Mission missionId) |> Ok
                           
                    | ActivitySpan.MorningOnly ->
                        match this.activities with
                        | Full(FullDayDailyActivity.Holiday name) ->
                            Error (CannotSetHoliday (name, this.date))

                        | Full(FullDayDailyActivity.Mission otherMissionId) when missionId <> otherMissionId ->
                            Half(HalfDayDailyActivity.Mission missionId, HalfDayDailyActivity.Mission otherMissionId) |> Ok
                        | Full(FullDayDailyActivity.Mission _) ->
                            Full(FullDayDailyActivity.Mission missionId) |> Ok
                        | Full(FullDayDailyActivity.Off) ->
                            Half(HalfDayDailyActivity.Mission missionId, HalfDayDailyActivity.Off) |> Ok
                            
                        | Half(_, HalfDayDailyActivity.Mission afternoonMissionId) when missionId = afternoonMissionId ->
                            Full(FullDayDailyActivity.Mission missionId) |> Ok
                        | Half(_, afternoon) ->
                            Half(HalfDayDailyActivity.Mission missionId, afternoon) |> Ok

                        | Overbooked activities ->
                            let overbookedActivity = OverbookedDayDailyActivity.Mission missionId, ActivitySpan.MorningOnly
                            Overbooked (overbookedActivity::activities) |> Ok
                        
                    | ActivitySpan.AfternoonOnly ->
                        match this.activities with
                        | Full(FullDayDailyActivity.Holiday name) ->
                            Error (CannotSetHoliday (name, this.date))

                        | Full(FullDayDailyActivity.Mission otherMissionId) when missionId <> otherMissionId ->
                            Half(HalfDayDailyActivity.Mission otherMissionId, HalfDayDailyActivity.Mission missionId) |> Ok
                        | Full(FullDayDailyActivity.Mission _) ->
                            Full(FullDayDailyActivity.Mission missionId) |> Ok
                        | Full(FullDayDailyActivity.Off) ->
                            Half(HalfDayDailyActivity.Off, HalfDayDailyActivity.Mission missionId) |> Ok
                            
                        | Half(HalfDayDailyActivity.Mission morningMissionId, _) when missionId = morningMissionId ->
                            Full(FullDayDailyActivity.Mission missionId) |> Ok
                        | Half(morning, _) ->
                            Half(morning, HalfDayDailyActivity.Mission missionId) |> Ok

                        | Overbooked activities ->
                            let overbookedActivity = OverbookedDayDailyActivity.Mission missionId, ActivitySpan.AfternoonOnly
                            Overbooked (overbookedActivity::activities) |> Ok
                        
                   
                member this.Add(newActivity:AddableActivity, newActivitySpan:ActivitySpan) : Result<ActivityDay, AddActivityError> =
                        res {
                            let! activities =
                                match newActivity with
                                | Off ->
                                    this.AddOff newActivitySpan
                                | Mission missionId ->
                                    this.AddMission(missionId, newActivitySpan)
                                        
                            return { this with activities = activities }    
                        }
                        
                member this.SetHoliday(name:string) =
                    match this.activities with
                    | Full (FullDayDailyActivity.Holiday otherName) when otherName <> name ->
                        { this with activities = Overbooked [
                                                (OverbookedDayDailyActivity.Holiday name, ActivitySpan.FullDay)
                                                (OverbookedDayDailyActivity.Holiday otherName, ActivitySpan.FullDay)
                        ]}
                    | Full (FullDayDailyActivity.Mission missionId) ->
                        { this with activities = Overbooked [
                                                (OverbookedDayDailyActivity.Holiday name, ActivitySpan.FullDay)
                                                (OverbookedDayDailyActivity.Mission missionId, ActivitySpan.FullDay)
                        ]}
                    | Half (HalfDayDailyActivity.Mission morningMissionId, HalfDayDailyActivity.Mission afternoonMissionId) ->
                        { this with activities = Overbooked [
                                                (OverbookedDayDailyActivity.Holiday name, ActivitySpan.FullDay)
                                                (OverbookedDayDailyActivity.Mission morningMissionId, ActivitySpan.MorningOnly)
                                                (OverbookedDayDailyActivity.Mission afternoonMissionId, ActivitySpan.AfternoonOnly)
                        ]}
                    | Half (HalfDayDailyActivity.Mission missionId, _) ->
                        { this with activities = Overbooked [
                                                (OverbookedDayDailyActivity.Holiday name, ActivitySpan.FullDay)
                                                (OverbookedDayDailyActivity.Mission missionId, ActivitySpan.MorningOnly)
                        ]}
                    | Half (_, HalfDayDailyActivity.Mission missionId) ->
                        { this with activities = Overbooked [
                                                (OverbookedDayDailyActivity.Holiday name, ActivitySpan.FullDay)
                                                (OverbookedDayDailyActivity.Mission missionId, ActivitySpan.AfternoonOnly)
                        ]}   
                     
                    | _ ->
                        { this with activities = Full(FullDayDailyActivity.Holiday name) }

    type Activity =
        | Holiday of Holiday
        | Mission of MissionId * ActivityByDate list
        | Off of ActivityByDate list
        
    type SettingActivityError =
    | WrongScheduleTypes of Reference
    | FindClientError of FindingBySlugOrIdError
    | CurrentMissionNotFound of SlugOrId<ClientId>
    | AddActivity of AddActivityError
        interface IError
    
    module private Activities =
        
        let private initActivityDay date = 
            ActivityDay.Empty(date)
    
        let setSomeActivity date (setupActivity: ActivityDay -> Result<ActivityDay, AddActivityError>) (map:Map<Date, ActivityDay option>) =
            res {
                let activityDay = if map |> Map.containsKey date then
                                     map[date] |> Option.defaultValue (initActivityDay date)
                                  else
                                     initActivityDay date
                                     
                let! newActivityDay = setupActivity(activityDay)
                let newActivityDay = Some newActivityDay
            
                let map = map |> Map.add date newActivityDay
                return map
            }
            
        
        let setNoActivity date map =
            map |> Map.change date (fun _ -> Some(None))
        
        
        // the group by date can't return an error. we assume that the input is correct
        let groupByDate (activities:Activity list) =
            let mutable map : Map<Date, ActivityDay option> = Map.empty
            
            let setSome date (setupActivity: ActivityDay -> Result<ActivityDay, AddActivityError>) =
                map <- map |> setSomeActivity date setupActivity |> value
                
            let setNone date =
                map <- map |> setNoActivity date
            
            for activity in activities do
                match activity with
                | Holiday holiday ->
                    setSome holiday.date (fun activity ->
                        activity.SetHoliday(holiday.name) |> Ok
                        )
                    
                | Mission (missionId, dailyActivities) ->
                    for span, date in dailyActivities do
                        match span with
                        | Some span ->
                            setSome date _.Add(AddableActivity.Mission missionId, span)
                        | None when map |> (not << Map.containsKey date) ->
                            setNone date
                        | _ -> ()
                        
                | Off dailyActivities ->
                    for span, date in dailyActivities do
                        match span with
                        | Some span -> 
                            setSome date _.Add(AddableActivity.Off, span) 
                        | None when map |> (not << Map.containsKey date) ->
                            setNone date
                        | _ -> ()
            
            map

        let flatten (map:Map<Date, ActivityDay option>) : Activity list =
            
            let mutable holidays : Map<string, Date> = Map.empty
            let setHoliday date name =
                holidays <- holidays |> Map.add name date
            
            let mutable missions : Map<MissionId, Map<Date, ActivitySpan option>> = Map.empty
            
            let setMission date missionId (span:ActivitySpan option) =
                missions <- missions |> Map.change missionId (fun previous ->
                    let dates =
                        match previous with
                        | Some dates -> dates
                        | None -> Map.empty
                        
                    dates |> Map.add date span |> Some
                )
            
            let mutable offDays : Map<Date, ActivitySpan option> = Map.empty
            let setOff date (span:ActivitySpan option) =
                offDays <- offDays |> Map.add date span
                    
            for kvp in map do
                let date, activityDay = kvp.Key, kvp.Value
                
                match activityDay with
                | None ->
                    setOff date None
                    for mission in missions do
                        setMission date mission.Key None
                    
                | Some activityDay ->
                    match activityDay.activities with
                    | Full(FullDayDailyActivity.Holiday name) ->
                        setHoliday date name
                    | Full(FullDayDailyActivity.Mission missionId) ->
                        setMission date missionId (Some ActivitySpan.FullDay) 
                    | Full(FullDayDailyActivity.Off) ->                
                        setOff date (Some ActivitySpan.FullDay)
                        
                    | Half(HalfDayDailyActivity.Mission morningMissionId, HalfDayDailyActivity.Mission afternoonMissionId) ->
                        setMission date morningMissionId (Some ActivitySpan.MorningOnly)
                        setMission date afternoonMissionId (Some ActivitySpan.AfternoonOnly)
                        
                    | Half(HalfDayDailyActivity.Mission morningMissionId, HalfDayDailyActivity.Off) ->
                        setMission date morningMissionId (Some ActivitySpan.MorningOnly)
                        setOff date (Some ActivitySpan.AfternoonOnly)
                
                    | Half(HalfDayDailyActivity.Off, HalfDayDailyActivity.Mission afternoonMissionId) ->
                        setOff date (Some ActivitySpan.MorningOnly)
                        setMission date afternoonMissionId (Some ActivitySpan.AfternoonOnly)
                        
                    | Half(HalfDayDailyActivity.Off, HalfDayDailyActivity.Off) ->
                        setOff date (Some ActivitySpan.FullDay)
                        
                    | Overbooked overbookedActivities ->
                        for activity, span in overbookedActivities do
                            match activity with
                            | OverbookedDayDailyActivity.Holiday name ->
                                setHoliday date name
                            | OverbookedDayDailyActivity.Mission missionId ->
                                setMission date missionId (Some span)
                            | OverbookedDayDailyActivity.Off ->
                                setOff date (Some span)         
            
            let holidaysActivities = holidays |> Seq.map (fun kvp -> Holiday { name = kvp.Key; date = kvp.Value }) |> Seq.toList
            let missionActivities = missions
                                    |> Seq.map (fun missionKvp ->
                                        let missionId = missionKvp.Key
                                        let missionDates = missionKvp.Value
                                        let dates = map.Keys
                                        
                                        let activities = dates
                                                         |> Seq.map (fun date ->
                                                                let span = 
                                                                    if missionDates.ContainsKey(date) then
                                                                        missionDates[date]
                                                                    else
                                                                        None
                                                                (span, date)
                                                         )
                                                         |> List.ofSeq
                                        
                                        Activity.Mission (missionId, activities)
                                    )
                                    |> List.ofSeq
            
            let offActivity = map.Keys
                                     |> Seq.map (fun date ->
                                            let span = 
                                                if offDays.ContainsKey(date) then
                                                    offDays[date]
                                                else
                                                    None
                                            (span, date)
                                     )
                                    |> List.ofSeq
                                    |> Activity.Off
            holidaysActivities @ missionActivities @ [offActivity]
            
        
    

    
    type Activities =
       | AsList of Activity list
       | AsMap of Map<Date, ActivityDay option>
        with
            member private this.ToMap() =
                match this with
                | AsMap _ -> this
                | AsList activities -> AsMap (Activities.groupByDate activities)
                
            member private this.ToList() =
                match this with
                | AsList _ -> this
                | AsMap map -> AsList (Activities.flatten map)
                
            member this.Set(date:Date, newActivity:AddableActivity, activitySpan:ActivitySpan) : Result<Activities, SettingActivityError> =
                match this with
                | AsList _ ->
                    this.ToMap().Set(date, newActivity, activitySpan)
                | AsMap activitiesByDate ->
                    res {
                        let! newActivitiesByDate = activitiesByDate |> Activities.setSomeActivity date _.Add(newActivity, activitySpan)
                                                                    |> Result.mapError SettingActivityError.AddActivity                        
        
                        Activities.AsMap(newActivitiesByDate)    
                    }
                    
            interface IEnumerable<Activity> with
                member this.GetEnumerator(): IEnumerator<Activity> =
                    match this with
                    | AsList activities ->
                        (activities :> IEnumerable<Activity>).GetEnumerator()
                    | AsMap _ ->
                        (this.ToList() :> IEnumerable<Activity>).GetEnumerator() 
                    
                member this.GetEnumerator(): System.Collections.IEnumerator =
                    match this with
                    | AsList activities ->
                        (activities :> System.Collections.IEnumerable).GetEnumerator()
                    | AsMap _ ->
                        (this.ToList() :> System.Collections.IEnumerable).GetEnumerator() 
                
open Schedule

module Holidays =
    let private easterDay (year:Year) : Date =
        let a = year.value % 19
        let b = year.value / 100
        let c = year.value % 100
        let d = b / 4
        let e = b % 4
        let f = (b + 8) / 25
        let g = (b - f + 1) / 3
        let h = (19 * a + b - d - g + 15) % 30
        let i = c / 4
        let k = c % 4
        let l = (32 + 2 * e + 2 * i - h - k) % 7
        let m = (a + 11 * h + 22 * l) / 451
        let month = (h + l - 7 * m + 114) / 31
        let day = (((h + l - 7 * m + 114) % 31) + 1) * 1<day> |> Day.From
        
        date (day, month |> Month.FromInt, year)
     
    let generate (year:Year) : Schedule.Holiday list =
        let date (day,month,year) = date (day |> Day.From, month, year)
        
        let easter = easterDay year
        [
            { name = "Nouvel an"; date = date(1<day>, January, year) }
            { name = "Lundi de Pâques"; date = easter.Add(1<day>) }
            { name = "Ascension"; date = easter.Add(39<days>) }
            { name = "Lundi de Pentecôte"; date = easter.Add(50<days>) }
            { name = "Fête des travailleurs"; date = date(1<day>, May, year) }
            { name = "Victoire 39/45"; date = date(8<day>, May, year) }
            { name = "Fête nationale"; date = date(14<day>, July, year) }
            { name = "Assomption"; date = date(15<day>, August, year) }
            { name = "Toussaint"; date = date(1<day>, November, year) }
            { name = "Armistice 14/18"; date = date(11<day>, November, year) }
            { name = "Noël"; date = date(25<day>, December, year) }
        ]


type MonthlyScheduleContent =
    | Generated of Activities
    | NotGenerated

type MonthlySchedule = {
    Reference : MonthReference
    Content : MonthlyScheduleContent
} with
    static member Generate(monthReference: MonthReference, holidays: Holiday list, clients: Clients) =
        let daysInMonth = monthReference.Days()
                          
        let firstDay = daysInMonth |> List.head
        let lastDay = daysInMonth |> List.last
        
        let holidaysOfTheMonth = holidays
                                |> List.filter (fun holiday -> holiday.date.month = monthReference.month && holiday.date.year = monthReference.year)
        
        let missions = clients.GetMissionsBetween (firstDay, lastDay)
        
        let emptyActivity = daysInMonth |> List.map ( fun day -> (None, day))
        
        let fullTimeMissionActivity (mission:Mission) =
            Activity.Mission (mission.id, daysInMonth |> List.map (
                    fun day ->
                        if day.IsWeekend then
                            (None, day)
                        else if holidaysOfTheMonth |> List.exists (fun holiday -> holiday.date = day) then
                            (None, day)
                        else 
                            (Some ActivitySpan.FullDay, day)
                ))

        let emptyMissionActivity (mission:Mission) =
            Activity.Mission (mission.id, emptyActivity)
                    
        let missionActivities =
            match missions with
            | [] -> []
            | [firstMission] -> [fullTimeMissionActivity firstMission]
            | firstMission::otherMissions ->
                 let firstMissionAsActivity = fullTimeMissionActivity firstMission
                 let otherMissionsAsActivities = otherMissions |> List.map emptyMissionActivity
                 firstMissionAsActivity::otherMissionsAsActivities
       
        let holidaysAsActivities = holidaysOfTheMonth |> List.map Activity.Holiday
        
        let offActivity = Off emptyActivity
        
        {
            Reference = monthReference
            Content = Generated (Activities.AsList (missionActivities @ holidaysAsActivities @ [offActivity]))       
        }
        
    member this.Set(asOfDate: AsOfDate, newActivity:SetActivityTarget, activitySpan:ActivitySpan, clients:Clients) =
        res {
            match this.Content with
            | NotGenerated ->
                let year = asOfDate.date.year
                let holidays = Holidays.generate year
                let monthlySchedule = MonthlySchedule.Generate(this.Reference, holidays, clients)
                return! monthlySchedule.Set(asOfDate, newActivity, activitySpan, clients)
            | Generated activities ->
                let! newActivity = match newActivity with
                                   | SetActivityTarget.Off -> Ok(AddableActivity.Off)
                                   | SetActivityTarget.Mission missionId -> Ok (AddableActivity.Mission missionId)
                                   | SetActivityTarget.Client clientSlugOrId ->
                                        res {
                                            let! client = clients.FindBySlugOrId clientSlugOrId |> Result.mapError FindClientError
                                            let currentMission = client.GetCurrentMission asOfDate
                                            
                                            let! currentMission =  match currentMission with
                                                                   | None ->
                                                                       Error (CurrentMissionNotFound clientSlugOrId)
                                                                   | Some mission ->
                                                                       Ok mission
                                            
                                            return AddableActivity.Mission(currentMission.id)
                                        }
                
                let! newActivities = activities.Set(asOfDate.date, newActivity, activitySpan)
                return { this with Content = Generated newActivities }    
        }
                
    member this.StoreIn(location: DataSource.Location option) =
        resPgm {
            let! source = DataSource.findWithQuery<MonthlySchedule, MonthReference>(location, this.Reference)
            do! source.Store(this)
        }

type QuarterlySchedule = {
    Reference : QuarterReference
    Months    : MonthlySchedule list
}
    with
        member this.StoreIn(location: DataSource.Location option) =
            resPgm {
                for month in this.Months do
                    do! month.StoreIn(location)
            }

type YearlySchedule = {
    Reference : Year
    Quarters  : QuarterlySchedule list
}
    with
        member this.StoreIn(location: DataSource.Location option) =
            resPgm {
                for quarter in this.Quarters do
                    do! quarter.StoreIn(location)
            }

type Schedule =
    | Monthly of MonthlySchedule
    | Quarterly of QuarterlySchedule
    | Yearly of YearlySchedule
        with
            member this.Reference: Reference =
                match this with
                | Monthly monthlySchedule -> Reference.ForMonth monthlySchedule.Reference
                | Quarterly quarterlySchedule -> Reference.ForQuarter quarterlySchedule.Reference
                | Yearly yearlySchedule -> Reference.ForYear yearlySchedule.Reference


type Schedule with
    
    static member QueryMonthlySchedule (monthReference:MonthReference) (scheduleLocation: DataSource.Location option) =
        resPgm {
            let! source = DataSource.findWithQuery<MonthlySchedule, MonthReference>(scheduleLocation, monthReference)
            return! source.Read()
        }
        
    static member private QueryQuarterlySchedule (quarter:Quarter, year:Year) (scheduleLocation: DataSource.Location option) =
        resPgm {
            let months = quarter.Months |> List.map (fun month -> {
                                                         month = month; year = year
                                                    })
                
            let mutable monthSchedules = []
                
            for month in months do 
                let! month = Schedule.QueryMonthlySchedule month scheduleLocation
                monthSchedules <- monthSchedules @ [month]            
            
            return {
                Reference = (quarter, year)
                Months = monthSchedules
            }
        }

    static member private QueryYearlySchedule (year:Year) (scheduleLocation: DataSource.Location option) =
        resPgm {
           
            // there always be 4 quarters in a year
            let! firstQuarter = Schedule.QueryQuarterlySchedule (Quarter.First, year) scheduleLocation
            let! secondQuarter = Schedule.QueryQuarterlySchedule (Quarter.Second, year) scheduleLocation
            let! thirdQuarter = Schedule.QueryQuarterlySchedule (Quarter.Third, year) scheduleLocation
            let! lastQuarter = Schedule.QueryQuarterlySchedule (Quarter.Last, year) scheduleLocation
            
            return {
                Reference = year
                Quarters = [firstQuarter;secondQuarter;thirdQuarter;lastQuarter]
            }
        }
        
    static member Query(reference: Schedule.Reference, scheduleLocation: DataSource.Location option) =
        match reference with
        | Schedule.Reference.ForMonth monthReference ->
            resPgm {
                let! monthlySchedule = Schedule.QueryMonthlySchedule monthReference scheduleLocation
                return monthlySchedule |> Schedule.Monthly
            }
        | Schedule.Reference.ForQuarter (quarter, year) ->
            resPgm {
                let! quarterlySchedule = Schedule.QueryQuarterlySchedule (quarter,year) scheduleLocation
                return quarterlySchedule |> Schedule.Quarterly
            }
        | Schedule.Reference.ForYear year ->
            resPgm {
                let! quarterlySchedule = Schedule.QueryYearlySchedule year scheduleLocation
                return quarterlySchedule |> Schedule.Yearly
            }
            
type Schedule with
    
    static member GenerateMonthlySchedule
        (reference: MonthReference)
        (holidays: Holiday list)
        (clients: Clients)
        (scheduleLocation: DataSource.Location option) =
        resPgm {
            let monthlySchedule = MonthlySchedule.Generate(reference, holidays, clients)
            
            let! source = DataSource.findWithQuery<MonthlySchedule, MonthReference>(scheduleLocation, reference)
            do! source.Store(monthlySchedule)
                        
            return monthlySchedule
        }
    
    static member GenerateQuarterlySchedule
        ((quarter, year):QuarterReference)
        (holidays: Holiday list)
        (clients: Clients)
        (scheduleLocation: DataSource.Location option) =
        resPgm {
            let mutable months = []
            
            for month in quarter.Months do
                let monthReference = { month = month; year = year }
                let! monthlySchedule = Schedule.GenerateMonthlySchedule monthReference holidays clients scheduleLocation
                months <- monthlySchedule::months
                
            return {
                Reference = (quarter, year)
                Months = (months |> List.rev)
            }
        }
        
    static member GenerateYearlySchedule
        (year:Year)
        (holidays: Holiday list)
        (clients: Clients)
        (scheduleLocation: DataSource.Location option) =
        resPgm {
            let mutable quarters : QuarterlySchedule list = []
            
            for quarter in year.Quarters do
                let quarterReference = (quarter, year)
                let! quarterlySchedule = Schedule.GenerateQuarterlySchedule quarterReference holidays clients scheduleLocation
                quarters <- quarterlySchedule::quarters
                
            return {
                Reference = year
                Quarters = (quarters |> List.rev)
            }
        }
    
    static member Generate(reference: Schedule.Reference, clients: Clients, scheduleLocation: DataSource.Location option) =
        match reference with
        | Schedule.Reference.ForMonth monthReference ->
            resPgm {
                let holidays = Holidays.generate monthReference.year
                let! monthlySchedule = Schedule.GenerateMonthlySchedule monthReference holidays clients scheduleLocation
                return monthlySchedule |> Schedule.Monthly
            }
        | Schedule.Reference.ForQuarter (quarter, year) ->
            resPgm {
                let holidays = Holidays.generate year
                
                let! quarterlySchedule = Schedule.GenerateQuarterlySchedule (quarter,year) holidays clients scheduleLocation
                return quarterlySchedule |> Schedule.Quarterly
            }
        | Schedule.Reference.ForYear year ->
            resPgm {
                let holidays = Holidays.generate year
                let! quarterlySchedule = Schedule.GenerateYearlySchedule year holidays clients scheduleLocation
                return quarterlySchedule |> Schedule.Yearly
            }

type Schedule with

    member this.Set(date:AsOfDate, target:SetActivityTarget, activitySpan:ActivitySpan, clients:Clients) =
        match this with
        | Yearly _
        | Quarterly _ ->
            Error(SettingActivityError.WrongScheduleTypes this.Reference)
        | Monthly monthlySchedule ->
            res {
                let! newMonthlySchedule = monthlySchedule.Set(date, target, activitySpan, clients)        
                return Monthly(newMonthlySchedule)    
            }
                
        
    member this.StoreIn(scheduleLocation: DataSource.Location option) =
        match this with
        | Monthly monthlySchedule ->
            monthlySchedule.StoreIn(scheduleLocation)
        | Quarterly quarterlySchedule ->
            quarterlySchedule.StoreIn(scheduleLocation)
        | Yearly yearlySchedule ->
            yearlySchedule.StoreIn(scheduleLocation)
        