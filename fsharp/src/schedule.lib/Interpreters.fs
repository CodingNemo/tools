﻿namespace schedule.lib

open System
open System.Collections.Generic
open backbone
open backbone.DataSource
open backbone.DataSource.DataSource
open backbone.DataSource.Errors
open backbone.DataSource.Instructions
open backbone.DataSource.Interpreter
open backbone.Dates
open backbone.Errors
open backbone.IO

open backbone.IO.Interpreter
open backbone.Result
open clients.lib
open schedule.lib.Schedule

module Interpreters =
    
    
    let private fetchDefaultScheduleRootLocationFromEnvironmentVariable () =
        res {
            let defaultClientSourcePathFromEnvVariable = Environment.GetEnvironmentVariable("T_SCHEDULE_ROOT")            
            let! clientSourceLocation = parseDataSourceLocation defaultClientSourcePathFromEnvVariable
            match clientSourceLocation with
            | None ->
               return! Error (InvalidLocation "Could not find schedule root folder location from the environment variable T_SCHEDULE_ROOT")
            | Some location ->
               return location
        }
    
    let private getMonthlyScheduleSourceFromLocation (monthReference:MonthReference) location =
        res {
            let! location = match location with
                            | None ->
                                fetchDefaultScheduleRootLocationFromEnvironmentVariable()
                            | Some l ->
                                Ok l
                            |> Result.mapError DataSourceLoadingError.Location
            
            match location with
            | Location.File _ ->
                return! Error (DataSourceLoadingError.Location (InvalidLocation "Schedule root must be a folder"))
            | Location.Folder rootPath ->
                let monthlyScheduleFileName = FileName.Parse $"{monthReference.Display()}.schedule"
                let yearFolder : FolderName = monthReference.year.ToString()
                        
                let filePath = rootPath / yearFolder / monthlyScheduleFileName
                
                return DataSource<MonthlySchedule, MonthReference>.File filePath 
        }   
    

    let interpretDataSourceInstruction interpret instruction =
        match instruction with
        | Find ((location, query), next) ->
            let monthlyScheduleDataSource = getMonthlyScheduleSourceFromLocation query location 
            interpret (next monthlyScheduleDataSource)
    
    module MonthlyScheduleFile =
        module Parser = 

            open backbone.Result
            open backbone.DataSource.Errors
            
            let private parseCells (line:string) =
                    line.Split("|")
                        |> List.ofArray
                        |> List.map _.Trim()
                        
            let private parseHeader (header:string) =
                let headerCells = header |> parseCells 
                match headerCells with
                | month::days ->
                    (month, days |> List.map int) |> Result.Ok
                | _ ->
                    Result.Error "Invalid header"
                                      
           
            let private parseRow (rowIndex:int) (row:string) =
                let cells = row |> parseCells 
                match cells with
                | identifier::cells -> 
                    (identifier, cells) |> Result.Ok
                | _ -> Result.Error $"Invalid row @{rowIndex}"
                
            let parseLines (lines:string seq) =
                res {
                    let header = lines |> Seq.head

                    let! month, days = header |> parseHeader
                    
                    let mutable rows = []
                    
                    for index, row in lines
                               |> Seq.skip 1
                               |> Seq.indexed do
                        let! row = row |> parseRow index
                        rows <- row::rows
                        
                    return month, (days, rows |> List.rev)
                } 
                
            let private parseGenerated (filePath:FilePath) =
                let invalidFileContent reason = Result.Error (FileParsingError.InvalidFileContent (filePath, reason)) 
                                               
                
                let parseMonthReference (month:string) =
                    res {
                        let! reference = MonthReference.Parse month
                                         |> Result.mapError (fun err ->
                                                                match err with
                                                                | InvalidArg (argName, reason) ->
                                                                       let reason = reason
                                                                                    |> Option.map (fun c -> $":{c}")
                                                                                    |> Option.defaultValue ""
                                                                       FileParsingError.InvalidFileContent (filePath, $"Invalid {argName}{reason}")
                                                            )
                        return reference
                    }

                let parseActivities (monthReference:MonthReference) days rows : Result<Activity list, FileParsingError> =
                            
                    let parseActivity (row: string * string list) (days: int list) : Result<Activity, FileParsingError> =
                        let findHolidayDate holidayName cells : Result<Date,FileParsingError>  =
                            try
                                let day = cells
                                          |> List.findIndex (fun cell -> cell = "X")
                                let day = { number =  day + 1 }
                                (monthReference.GetDate >> Result.Ok) day
                            with
                            | :? ArgumentException ->
                                invalidFileContent $"No date found for holiday {holidayName}"
                        
                        let parseMissionId (input:string) =
                            input |> MissionId.Parse
                                  |> bindError (fun _ -> invalidFileContent $"Invalid mission id : {input}")
                        
                        let parseCells cells =
                            let parseCell day cell : Result<ActivityByDate, FileParsingError> =
                                let date = monthReference.GetDate day
                                match cell with
                                | "X" ->  (Some ActivitySpan.FullDay, date) |> Result.Ok
                                | "AM" -> (Some ActivitySpan.MorningOnly, date) |> Result.Ok
                                | "PM" -> (Some ActivitySpan.AfternoonOnly, date) |> Result.Ok
                                | w when String.IsNullOrWhiteSpace(w) -> (None, date) |> Result.Ok
                                | _ -> invalidFileContent $"Invalid cell value '{cell}'"
                                
                            res {
                                let mutable activityByDates = []
                                
                                for i, cell in cells |> Seq.indexed do
                                    let day = Day.From( (i+1) * 1<days>)
                                    let! activityByDate = parseCell day cell
                                    activityByDates <- activityByDate::activityByDates
                                
                                return activityByDates |> List.rev
                            }
                        
                        let identifier, cells = row
                        
                        if cells.Length <> days.Length then
                            invalidFileContent $"Invalid number of days for activity {identifier}. Expected {days.Length} (from the header). Actual {cells.Length}"
                        else
                            match identifier with
                            | missionId when missionId.StartsWith("mission:") ->
                                res {
                                    let! clientId = missionId.Substring(8) |> parseMissionId
                                    let! activitiesByDates = parseCells cells
                                    return Activity.Mission (clientId, activitiesByDates)
                                }
                            | holiday when holiday.StartsWith("holiday:") ->
                                res {
                                    let holidayName = holiday.Substring(8)
                                    let! date = findHolidayDate holidayName cells
                                    return Activity.Holiday {
                                        name = holidayName
                                        date = date
                                    }     
                                }
                            | "off" ->
                                   res {
                                        let! activitiesByDates = parseCells cells
                                        return Activity.Off activitiesByDates    
                                   }
                            | _ ->
                                invalidFileContent $"Unknown activity {identifier}"
                                   
                    res {
                        let mutable activities = []
                        
                        for row in rows do
                            let! activity = parseActivity row days
                            activities <- activity::activities
                            
                        return activities |> List.rev
                    }
                
                res {
                    let lines = filePath.ToString() |> System.IO.File.ReadAllLines
                    let! month,table = lines |> parseLines
                                             |> bindError (fun errors ->
                                                 let error = String.Join(Environment.NewLine, errors)
                                                 invalidFileContent error
                                                )
                    
                    let! reference = month |> parseMonthReference

                    let! activities = table ||> parseActivities reference
                    
                    return {
                        Reference = reference
                        Content = Generated(Activities.AsList(activities))
                    }
                }
                
            let private parseNotGenerated (filePath:FilePath) : Result<MonthlySchedule, FileParsingError> =
                res {
                    let! monthReference = MonthReference.Parse(filePath.Name.Name)
                                          |> Result.mapError (fun err ->
                                                                match err with
                                                                | InvalidArg (argName, reason) ->
                                                                       let reason = reason
                                                                                    |> Option.map (fun c -> $":{c}")
                                                                                    |> Option.defaultValue ""
                                                                       FileParsingError.InvalidFileContent (filePath, $"Invalid {argName}{reason}")
                                                            )
                                 

                    return {
                        Reference = monthReference
                        Content = NotGenerated
                    }
                } 
                    
            let parse (filePath:FilePath) =
                if filePath.ToString() |> System.IO.File.Exists then
                    filePath |> parseGenerated
                else
                    filePath |> parseNotGenerated
        module Writer =
            open backbone.DataSource.Errors
            open backbone.Result

            type Column = {
                index: int
                name: string
            }
            
            type Cell = {
                content: string
            }
                with
                    
                    static member Full = { content = "X" }
                    static member Afternoon = { content = "PM" }
                    static member Morning = { content = "AM" }
                    static member Empty = { content = "" }
                
            type Row = {
                index: int
                cells: Cell list
            }
                
            type Table() =
                let mutable columns: Column list = []
                let mutable rows: Row list = []
                
                let widthPerColumn : Dictionary<int, int> = Dictionary<int, int>()
                               
                let updateColumnWidth cells =
                    for columnIndex, cell in cells |> List.indexed do
                        let exist, value = widthPerColumn.TryGetValue(columnIndex)
                        if not exist || value < cell.content.Length then
                            widthPerColumn[columnIndex] <- cell.content.Length
                            
                let headerToString () : string =
                    let headerBuilder = System.Text.StringBuilder()
                    for column in columns |> List.rev do
                        let width = widthPerColumn[column.index]
                        let paddedColumnName = column.name
                                                   .PadRight(width+1)
                                                   
                        headerBuilder.Append(paddedColumnName) |> ignore
                        
                        if column.index < columns.Length - 1 then
                            headerBuilder.Append("| ") |> ignore
                        
                    headerBuilder.ToString()
                
                let rowToString (row:Row) : string =
                    let rowBuilder = System.Text.StringBuilder()
                    for columnIndex, cell in row.cells |> List.indexed do
                        let width = widthPerColumn[columnIndex]
                        let paddedContent = cell.content
                                                .PadRight(width+1)
                                                   
                        rowBuilder.Append(paddedContent) |> ignore
                        
                        if columnIndex < row.cells.Length - 1 then
                            rowBuilder.Append("| ") |> ignore
                        
                    rowBuilder.ToString()
                        
                member this.AddColumns(columnsNames: string list) =
                    columnsNames |> List.iter this.AddColumn 
                    
                member this.AddColumn(name:string) = 
                    let column = {
                        index = columns.Length
                        name = name
                    }

                    widthPerColumn[column.index] <- column.name.Length
                    
                    columns <- column::columns
                    
                member this.AddRows(rows: Cell list list) =
                    rows |> List.iter this.AddRow
                
                member this.AddRow(cells: Cell list) =
                    let newRow = {
                        index = rows.Length
                        cells = cells
                    }
                    
                    updateColumnWidth cells
                    rows <- newRow::rows
                
                override this.ToString() =
                    let rowsBuilder = System.Text.StringBuilder()
                    rowsBuilder
                        .AppendLine(headerToString()) |> ignore
                        
                    for row in rows |> List.rev do
                        rowsBuilder.AppendLine(rowToString row) |> ignore
                        
                    rowsBuilder.ToString()

            
            let private createFolderIfNotExist (folderPath:RootedFolderPath) =
                let folderPath = folderPath.ToString() 
                if not (System.IO.Directory.Exists folderPath) then
                    System.IO.Directory.CreateDirectory folderPath |> ignore
                Ok()
                
            let private generateHolidayRow (holiday:Holiday) : Cell list =
                
                let name = holiday.name
                let month = holiday.date.month
                let year = holiday.date.year
                let holiday = holiday.date.day
                
                let monthDays = month.Days(year)
                { content = $"holiday:{name}" }::(monthDays |> List.map (fun date ->
                                                                                if date = holiday then
                                                                                    Cell.Full
                                                                                else
                                                                                    Cell.Empty
                                                                    ))

            let private generateActivityCells (activities: ActivityByDate list) : Cell list =
                activities |> List.map (fun (activity,_) ->
                                                        match activity with
                                                        | Some ActivitySpan.FullDay -> Cell.Full
                                                        | Some ActivitySpan.MorningOnly -> Cell.Morning
                                                        | Some ActivitySpan.AfternoonOnly -> Cell.Afternoon
                                                        | None -> Cell.Empty
                                        )
            
            let private generateOffRow (activities: ActivityByDate list) : Cell list =
                { content = "off" }::(activities |> generateActivityCells)
                            
            let private generateMissionRow (missionId:MissionId) (activities: ActivityByDate list) : Cell list =
                { content = $"mission:{missionId}" }::(activities |> generateActivityCells)
                
            let private createTableFrom (monthlySchedule:MonthlySchedule) =
                let monthlyReference = monthlySchedule.Reference 
                let days = monthlyReference.Days()
                
                let reference = Reference.ForMonth monthlySchedule.Reference
                
                let table = Table()
                
                let paddedDays = days
                                 |> List.map _.day
                                 |> List.map _.ToString().PadLeft(2, '0')
                
                let columns = reference.Display()::paddedDays
                table.AddColumns(columns)
                
                let rows =
                    match monthlySchedule.Content with
                    | NotGenerated -> []
                    | Generated activities ->
                        activities |> Seq.map (
                            function
                                | Holiday holiday -> generateHolidayRow holiday
                                | Mission (missionId, activities) -> generateMissionRow missionId activities
                                | Off activities -> generateOffRow activities
                            ) |> Seq.toList
                    
                table.AddRows(rows)
                
                table
            
            let write (filePath:FilePath) (monthlySchedule:MonthlySchedule) : Result<unit, FileSavingError> =
                res {
                    do! createFolderIfNotExist filePath.Folder
                    let table = createTableFrom monthlySchedule
                    
                    System.IO.File.WriteAllText(filePath.ToString(), table.ToString())    
                }
        
    let interpretFileDataSourceInstruction interpret instruction =
        match instruction with
        | FileDataSourceInstruction.Parse (filePath, next) ->          
            let parsingResult = filePath |> MonthlyScheduleFile.Parser.parse
            interpret (next parsingResult)
        | FileDataSourceInstruction.Save ((filePath, monthlySchedule),next) ->
             let saveResult = monthlySchedule |> MonthlyScheduleFile.Writer.write filePath 
             interpret (next saveResult)
    
    let rec interpret<'a> (program:Program<'a>): 'a =
        match program with
        | Instruction instruction ->
            match instruction with
            | :? DataSourceLocationInstruction<Program<'a>> as dli ->                  
                interpretDataSourceLocationInstructions interpret dli 
            | :? DataSourceInstruction<Clients, Program<'a>> as csi ->                  
                Interpreters.interpretDataSourceInstruction interpret csi
            | :? FileDataSourceInstruction<Clients, Program<'a>> as cfi ->
                Interpreters.interpretFileDataSourceInstruction interpret cfi
            
            | :? DataSourceInstruction<MonthlySchedule, MonthReference, Program<'a>> as ssi ->                  
                 interpretDataSourceInstruction interpret ssi
            | :? FileDataSourceInstruction<MonthlySchedule, Program<'a>> as sfi ->
                 interpretFileDataSourceInstruction interpret sfi
            
            | _ ->
                failwith $"unknown instruction {instruction.GetType().Name}"
        | Stop p -> p

