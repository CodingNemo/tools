﻿namespace clients.lib

open System
open System.IO


open Spectre.Console
open backbone
open backbone.DataSource.Instructions
open backbone.DataSource.Errors
open backbone.DataSource.Interpreter
open backbone.Dates
open backbone.IO
open backbone.IO.Instructions
open backbone.Result

open clients.lib.Instructions

module Interpreters =     
    module private MissionCsvFile =
        open FSharp.Data
        
        let private CsvFileName = FileName.Parse "missions.csv"
        
        type private CsvFile = CsvProvider<Sample="Id,Title,StartDate,EndDate,Tj">
        
        module private MissionTitle =
            let parse = MissionTitle.TryParse
            let serialize (title:MissionTitle option) : string =
                title |> Option.map _.ToString()
                      |> Option.defaultValue ""
                            
        module private Tj =
            let parse (text:string) : Tj =
                int text
            
            let serialize (tjm:Tj) : string =
                tjm.ToString()
                
        let private missionsFilePath (rootFolder: RootedFolderPath) (client:Client) : FilePath =
            let folderName : FolderName = client.Slug.value
            rootFolder / folderName / CsvFileName
                
        let saveFor (rootFolder: RootedFolderPath) (client:Client) : Result<unit, FileSavingError> =
            let hasMissions = client.Missions |> Seq.isEmpty |> not
           
            if not hasMissions then
                Ok ()
            else
                let toRow (mission:Mission) : CsvFile.Row =
                    CsvFile.Row(
                        mission.id.ToString(),
                        mission.title |> MissionTitle.serialize, 
                        mission.startDate.ToString(),
                        mission.endDate |> Option.map _.ToString()
                                        |> Option.defaultValue "",
                        mission.tj |> Tj.serialize
                    )
            
                let filePath = client |> missionsFilePath rootFolder
                
                let folderDoesNotExist = filePath.Folder.ToString() |> Directory.Exists |> not

                if folderDoesNotExist then
                    Directory.CreateDirectory(filePath.Folder.ToString()) |> ignore
                
                let rows = client.Missions |> Seq.map toRow
                
                try
                    use csvFile = new CsvFile(rows)
                    csvFile.Save(filePath.ToString())
                    Ok()
                with
                | :? UnauthorizedAccessException ->
                    Error (FileSavingError.FileAccess (FileAccessError.AccessDenied filePath))
                | :? DirectoryNotFoundException ->
                    Error (FileSavingError.FileAccess (FileAccessError.FolderNotFound filePath.Folder))
                | :? ArgumentException as ex ->
                    Error (FileSavingError.FileAccess (FileAccessError.InvalidPath (filePath, ex.Message)))
                | :? PathTooLongException as ex ->
                    Error (FileSavingError.FileAccess (FileAccessError.InvalidPath (filePath, $"Path too long: {ex.Message}")))            
                | ex ->
                    Error (FileSavingError.InvalidFileContent (filePath, ex.Message))
            
        let parseFor (rootFolder: RootedFolderPath) (client:Client) : Result<Mission list, FileParsingError> =
            let fromRow (row:CsvFile.Row) : Mission =
                {
                    id = row.Id |> (MissionId.Parse >> value)
                    title = row.Title |> MissionTitle.parse
                    startDate = row.StartDate |> (StartDate.Parse >> value)
                    endDate = row.EndDate |> EndDate.TryParse
                    tj = row.Tj |> Tj.parse
                }
            let filePath = client |> missionsFilePath rootFolder
            
            if File.Exists(filePath.ToString()) then
                try 
                    use csv = CsvFile.Load(filePath.ToString())
                    let missions = csv.Rows |> Seq.map fromRow 
                                            |> Seq.toList
                    Ok(missions)
                with
                | :? UnauthorizedAccessException ->
                    Error (FileParsingError.FileAccess (FileAccessError.AccessDenied filePath))
                | :? ArgumentException as ex ->
                    Error (FileParsingError.FileAccess (FileAccessError.InvalidPath (filePath, ex.Message)))
                | :? PathTooLongException as ex ->
                    Error (FileParsingError.FileAccess (FileAccessError.InvalidPath (filePath, $"Path too long : {ex.Message}")))
                | ex ->
                    Error (FileParsingError.InvalidFileContent (filePath, ex.Message))
            else
                Ok([])
    
    module private ClientsCsvFile =
        open FSharp.Data
        type private CsvFile = CsvProvider<Sample="Id,Slug,Name,Email,ArchiveDate">
    
        module private Slug =
            let serialize (slug:Slug) : string = slug.value
            let parse (slug:string) : Slug = { value = slug }
        
        module private ClientId =
            let parse =
                ClientId.Parse >> value
            
            let serialize (clientId: ClientId) =
                clientId.ToString()
                
        module private ClientName =
            let parse (clientName:string) : ClientName =
                ClientName.Create(clientName)
            
            let serialize (clientName: ClientName) =
                clientName.ToString()
                
        module private Email =
            let parse (text:string) : Email option =
                Email.TryParse text
                    
            let serialize (email:Email option) : string =
                match email with
                | None -> ""
                | Some email -> email.Address
                
       
        module private ArchiveDate =
            let parse (text:string) : ArchiveDate option =
                if String.IsNullOrWhiteSpace text then
                    None
                else
                    text |> Date.Parse
                         |> Result.toOption
                         |> Option.map ArchiveDate.FromDate
                
            let serialize (archiveDate: ArchiveDate option) : string =
                archiveDate |> Option.map _.date.ToString()
                            |> Option.defaultValue ""
                     
        let save (filePath:FilePath) (clients:Clients) : Result<unit, FileSavingError> =
            let buildRow (client:Client) : CsvFile.Row =
               CsvFile.Row(
                   client.Id |> ClientId.serialize,
                   client.Slug |> Slug.serialize,
                   client.Name |> ClientName.serialize,
                   client.Email |> Email.serialize,
                   client.ArchiveDate |> ArchiveDate.serialize
               )
            
            let rows = clients |> Seq.map buildRow
            try
                use csvFile = new CsvFile(rows)
                csvFile.Save(filePath.ToString())
                Ok()
            with
            | :? UnauthorizedAccessException ->
                Error (FileSavingError.FileAccess (FileAccessError.AccessDenied filePath))
            | :? DirectoryNotFoundException ->
                Error (FileSavingError.FileAccess (FileAccessError.FolderNotFound filePath.Folder))
            | :? ArgumentException as ex ->
                Error (FileSavingError.FileAccess (FileAccessError.InvalidPath (filePath, ex.Message)))
            | :? PathTooLongException as ex ->
                Error (FileSavingError.FileAccess (FileAccessError.InvalidPath (filePath, $"Path too long: {ex.Message}")))            
            | ex ->
                Error (FileSavingError.InvalidFileContent (filePath, ex.Message)) 
            
        let parse (filePath:FilePath) : Result<Clients, FileParsingError> =
            
            let parseCsvRow (row:CsvFile.Row) =
                   {
                        Id = row.Id |> ClientId.parse
                        Slug = row.Slug |> Slug.parse
                        Name = row.Name |> ClientName.parse
                        Email = row.Email |> Email.parse
                        Missions = []
                        ArchiveDate = row.ArchiveDate |> ArchiveDate.parse
                    }    

            
            let path = filePath.ToString();
            
            if File.Exists(path) then
                try
                    use csv = CsvFile.Load(path)
                    
                    let clients = csv.Rows |> Seq.map parseCsvRow 
                                           |> Seq.toList
                                                
                    Ok(Clients(clients))
                with
                | :? UnauthorizedAccessException ->
                    Error (FileParsingError.FileAccess (FileAccessError.AccessDenied filePath))
                | :? ArgumentException as ex ->
                    Error (FileParsingError.FileAccess (FileAccessError.InvalidPath (filePath, ex.Message)))
                | :? PathTooLongException as ex ->
                    Error (FileParsingError.FileAccess (FileAccessError.InvalidPath (filePath, $"Path too long : {ex.Message}")))
                | ex ->
                    Error (FileParsingError.InvalidFileContent (filePath, ex.Message))
            else
                let emptyClients = Clients.Empty
                emptyClients
                    |> save filePath
                    |> Result.map (fun () -> emptyClients)
                    |> Result.mapError FileParsingError.FileSave
    
    let save (filePath:FilePath) (clients:Clients) =
        res {
            do! clients |> ClientsCsvFile.save filePath
            for client in clients do
                 do! MissionCsvFile.saveFor filePath.Folder client 
        }

    let parse (filePath:FilePath) =
        res {
            let mutable result = []
            let! clients = ClientsCsvFile.parse filePath
            for client in clients do
                let! missions = MissionCsvFile.parseFor filePath.Folder client
                let clientWithMissions = { client with Missions = missions }
                result <- clientWithMissions::result
            return Clients(result |> List.rev)
        }  
              
    let interpretFileDataSourceInstruction interpret instruction =
        match instruction with
        | FileDataSourceInstruction.Parse (filePath, next) ->
            let clients = filePath |> parse
            interpret (next clients)
        | FileDataSourceInstruction.Save ((filePath, clients),next) ->
            let saveResult = clients |> save filePath
            interpret (next saveResult)  
    
    let private DefaultClientsCsvFile = FileName.Parse "clients.csv"
    
    let private fetchDefaultClientsLocationFromEnvironmentVariable () : Result<DataSource.DataSource.Location, DataSourceLocationParsingError>=
        res {
            let defaultClientSourcePathFromEnvVariable = Environment.GetEnvironmentVariable("T_CLIENTS_SOURCE")
            let! clientSourceLocation = parseDataSourceLocation defaultClientSourcePathFromEnvVariable
            match clientSourceLocation with
            | None ->
               return! Error (InvalidLocation "Could not find default clients source location from the environment variable T_CLIENTS_SOURCE")
            | Some location ->
               return location
        }
        
    let private getDataSourceFromLocation (location:DataSource.DataSource.Location option)  =
        res {
            let! location = match location with
                            | None -> fetchDefaultClientsLocationFromEnvironmentVariable()
                            | Some location -> Ok location
                            |> Result.mapError DataSourceLoadingError.Location
            
            match location with
            | DataSource.DataSource.Location.Folder folder ->
                let filePath = folder / DefaultClientsCsvFile
                return DataSource.DataSource<Clients>.File filePath
            | DataSource.DataSource.Location.File filePath ->
                DataSource.DataSource<Clients>.File filePath    
        }
        
           
    let interpretDataSourceInstruction interpret instruction =
        match instruction with
        | DataSourceInstruction.Find ((location,_), next) ->
            let clientsDataSource = getDataSourceFromLocation location
            interpret (next clientsDataSource)          
            
    let interpretClientsInstruction interpret instruction=
        match instruction with
        | ClientsInstruction.ChooseOne (clients, next) ->
            let prompt = SelectionPrompt<Client>()
            prompt.Title <- "Choose a client"
            prompt.Converter <- fun client -> $"{client.Name}"
            prompt.AddChoices(clients) |> ignore
            
            let selectedClient = AnsiConsole.Prompt(prompt)            
            
            interpret (next selectedClient)
            
    let rec interpret<'a> (program:Program<'a>) =
        match program with
        | Instruction instruction ->
            match instruction with
            | :? DataSourceLocationInstruction<Program<'a>> as dli ->                  
                interpretDataSourceLocationInstructions interpret dli 
            | :? DataSourceInstruction<Clients, Program<'a>> as csi ->                  
                interpretDataSourceInstruction interpret csi
            | :? FileDataSourceInstruction<Clients, Program<'a>> as cfi ->
                interpretFileDataSourceInstruction interpret cfi
            | :? FileInstruction<Program<'a>> as fi ->
                Interpreter.interpretFileInstruction interpret fi
            | :? RootedFolderInstruction<Program<'a>> as fi ->
                Interpreter.interpretFolderInstruction interpret fi
            | :? ClientsInstruction<Program<'a>> as ci ->
                interpretClientsInstruction interpret ci
            | _ ->
                failwith $"unknown instruction {instruction.GetType().Name}"
        | Stop p -> p
            
