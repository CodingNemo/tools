﻿namespace clients.lib

open System

open backbone
open backbone.Dates
open backbone.Errors
open backbone.DataSource
open backbone.IO
open backbone.IO.Instructions
open backbone.Program
open backbone.Result


type ClientId = {
    Value: Guid
}
    with 
        static member Generate() : ClientId =
            {
                Value = Guid.NewGuid()
            }
        
        static member Parse(text:string) =
            let success, guid = Guid.TryParse(text)
            if success then
                Ok { Value = guid } 
            else
                Error (InvalidArg ("ClientId", Some text))
        
        static member TryParse(text:string) =
            let result = ClientId.Parse text
            match result with
            | Ok clientId -> Some clientId
            | Error _ -> None
        
            
        override this.ToString() =
            this.Value.ToString()

type ClientStatus =
| Active
| Inactive
| Archived

type ClientName = {
    Value: string
}
    with
        static member Create (name: string) =
            {
                Value = name
            }
        override this.ToString() =
            this.Value.ToString()

type Email = {
    Address: string
}
    with
        
        static member Parse(text:string) : Email =
            {
                Address = text 
            }
        static member TryParse(text:string) : Email option =
            if String.IsNullOrWhiteSpace text then
                None
            else
                Some (Email.Parse(text))
        
                
        override this.ToString() = this.Address

type DocumentsFolder = {
    Path: RelativeFolderPath
}

type ClientsFolder = {
    Path: RelativeFolderPath
}
    with
        member this.GenerateDocumentsFolderFor(slug: Slug) : DocumentsFolder =
            let folder : FolderName = slug.value
            {
                Path = {
                    parent = None
                    name = folder
                }  
            }

type Folders = {
    Documents : DocumentsFolder
}

type UpdateDate = {
    date: Date
}
with

    static member Parse =
        Date.Parse
        >> Result.map (fun date -> { date = date })
        >> Result.mapError (fun err -> InvalidArg("UpdateDate", Some err))
 
    static member ParseOrDefault (text:string) =
        if String.IsNullOrWhiteSpace text then
            Ok { date = Date.Today }
        else
            UpdateDate.Parse text
           
    member this.PreviousDay() =
         {
             date = this.date.Add(-1<days>)
         }
         
    override this.ToString() = this.date.ToString()
    
type StartDate = {
    date:Date
}
with
    static member Parse =
        Date.Parse
        >> Result.map (fun date -> { date = date })
        >> Result.mapError (fun err -> InvalidArg ("StartDate", Some err))
    
    static member ParseOrDefault (text:string) =
        if String.IsNullOrWhiteSpace text then
            Ok { date = Date.Today }
        else
            StartDate.Parse text
    
    static member op_Implicit(updateDate: UpdateDate) : StartDate =
        {
            date = updateDate.date
        }
        
    override this.ToString() = this.date.ToString()

type EndDate = {
    date:Date
}
with
    static member Parse =
        Date.Parse
        >> Result.map (fun date -> { date = date })
        >> Result.mapError (fun err -> InvalidArg ("EndDate", Some err))
    
    static member ParseOrDefault (text:string) =
        if String.IsNullOrWhiteSpace text then
            Ok { date = Date.Today }
        else
            EndDate.Parse text
    
    static member TryParse text =
        if String.IsNullOrWhiteSpace text then
            None
        else
            text |> EndDate.Parse
                 |> Result.toOption
                 
    static member op_Implicit(updateDate: UpdateDate) : EndDate =
        {
            date = updateDate.date
        }
        
    override this.ToString() = this.date.ToString()
    

type Tj = int

module MissionId =
    open FParsec
    
    let private parser =
        parse {
            let! slug = manyCharsTill anyChar (pchar '#')
            let! index = pint32
            return (slug, index)
        }
        
    let parse text =
        match run parser text with
        | Success (r, _, _) ->
            Result.Ok r
        | Failure (msg, _, _) ->
            Result.Error (InvalidArg ("MissionId", Some msg))

type MissionId =
    {
        clientSlug: Slug
        index: int
    }
    with 
        static member GenerateFrom(clientSlug:Slug, index:int) : MissionId =
           {
               clientSlug = clientSlug
               index = index 
           }
           
        static member Parse =
            MissionId.parse >> Result.bind (fun (slug,index) ->
                res {
                    let! slug = Slug.Parse slug
                
                    return {
                        clientSlug = slug
                        index = index
                    }    
                })

        static member TryParse = MissionId.Parse >> Result.toOption
        
        override this.ToString() = $"{this.clientSlug}#{this.index}"

type MissionTitle = {
    value:string
}
    with
        static member TryParse (text:string) : MissionTitle option =
            if String.IsNullOrWhiteSpace text then
                None
            else
                Some { value = text }
                
        override this.ToString() =
            this.value

type MissionStatus =
    | NotStarted
    | Pending
    | Ended

type Mission = {
    id: MissionId
    title: MissionTitle option
    startDate: StartDate
    endDate: EndDate option
    tj: Tj
}
    with
        member this.Status (asOfDate:AsOfDate) =
            match this.endDate with
            | Some endDate when asOfDate.date >= endDate.date -> Ended
            | Some endDate when this.startDate.date <= asOfDate.date && asOfDate.date < endDate.date -> Pending
            | None when this.startDate.date <= asOfDate.date -> Pending
            | _ -> NotStarted

type MissionError =
| MissionAlreadyStarted of MissionId * StartDate
| NoMissionStarted
    interface IError

type ArchivingClientError =
| ClientIsStillActive of Slug
    interface IError

type ArchiveDate = {
    date: Date
} with

    static member Parse  =
        Date.Parse
        >> Result.map ArchiveDate.FromDate
        >> Result.mapError (fun err -> InvalidArg ("ArchiveDate", Some err))

    static member ParseOrDefault (text:string) =
        if String.IsNullOrWhiteSpace text then
            ArchiveDate.FromDate Date.Today |> Ok
        else
            ArchiveDate.Parse text

    static member FromDate(date:Date) : ArchiveDate =
        { date = date }

    member this.AsOf() : AsOfDate =
        {
            date = this.date
        }

type Client =
    { Id: ClientId
      Slug: Slug
      Name: ClientName
      Email: Email option
      Missions: Mission list
      ArchiveDate: ArchiveDate option
      }
    with
        static member Create (clientId:ClientId option) (slug: Slug option) (name: ClientName) (email:Email option) =
            let slug = slug |> Option.defaultWith (fun () -> Slug.GenerateFrom name.Value)
            
            let clientId = clientId |> Option.defaultWith ClientId.Generate
            
            {
                Id = clientId 
                Slug = slug
                Name = name
                Email = email
                Missions = []
                ArchiveDate = None
            }
                                     

        member this.Status (asOfDate:AsOfDate) =
            match this.ArchiveDate with
            | Some archiveDate when asOfDate.date >= archiveDate.date -> Archived
            | _ -> 
                let currentMission = this.GetCurrentMission asOfDate
                match currentMission with
                | Some _ -> Active
                | None -> Inactive
                
        member this.SetEmail (email:Email) =
            { this with Email = Some email }

        member this.GetMission(missionId:MissionId) =
            this.Missions |> List.find (fun mission -> mission.id = missionId)
            
        member this.GetCurrentMission(date:AsOfDate) =
            this.Missions |> List.tryFind (fun mission ->
                    let status = mission.Status date
                    status = Pending
                )
        
        member this.GetMissionsBetween (from:Date, to': Date) : Mission list =
            this.Missions |> List.filter (fun mission ->
                let isAfterStartDate = mission.startDate.date <= to'
                let isBeforeEndDate = mission.endDate
                                     |> Option.map (fun endDate -> endDate.date >= from)
                                     |> Option.defaultValue true
                
                
                isAfterStartDate && isBeforeEndDate
            )
        
        member this.StartMission (startDate:StartDate, tj:int, title:MissionTitle option) =
            let currentMission = this.Missions |> List.tryFind (fun mission -> mission.endDate |> Option.isNone)                     
            match currentMission with
            | Some mission ->
                Error (MissionAlreadyStarted (mission.id, mission.startDate))
            | None ->
                let newMission = {
                    id = MissionId.GenerateFrom(this.Slug, this.Missions.Length)
                    title = title
                    startDate = startDate
                    endDate = None
                    tj = tj; 
                }
                Ok ({ this with Missions = newMission :: this.Missions }, newMission)
        
        member this.EndMission (endDate:EndDate) =
            let currentMission = this.Missions |> List.tryFind (fun mission -> mission.endDate |> Option.isNone)                     
            match currentMission with
            | Some mission ->
                let endedMission = { mission with endDate = Some endDate }
                let newMissions = this.Missions |> List.map (fun m -> if m.id = mission.id then endedMission else m)
                
                Ok ({ this with Missions = newMissions }, endedMission)
            | None ->
                Error NoMissionStarted
        
        member this.Archive (archiveDate:ArchiveDate) : Program<Result<Client, IError>> =
            resPgm {
                let status = this.Status(archiveDate.AsOf())
                
                match status with
                | ClientStatus.Active ->
                    return! (Error (ArchivingClientError.ClientIsStillActive this.Slug :> IError)) |> lift 
                | ClientStatus.Archived ->
                    return this
                | ClientStatus.Inactive ->
                    return { this with ArchiveDate = Some(archiveDate) }               
            }
