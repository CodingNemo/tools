﻿namespace clients.lib

open backbone
open backbone.Dates
open backbone.Program

module Instructions = 
    
    type ClientsInstruction<'next> =
    | ChooseOne of Client list * next:(Client -> 'next)    
        interface IInstruction<'next> with
            member this.Map<'b>(f:'next -> 'b) : IInstruction<'b> =
                match this with
                | ChooseOne (clients, next) ->
                    ChooseOne (clients, next >> f)
                
    type Clients with
        static member ChooseOne (clients:Client list) =
            Instruction (ClientsInstruction.ChooseOne (clients, Stop))

        member this.FindCurrentlyActiveClient(asOfDate:AsOfDate) : Program<Result<Client, IError>> =
            let choseActiveClient (activeClients:Client list)  =
                pgm {
                    let! chosenClient = Clients.ChooseOne activeClients
                    return Ok chosenClient
                }
            
            let allActiveClients = this.OnlyActive(asOfDate)
            match allActiveClients with
            | [] ->
                let error = Error(FindingActiveClientError.NoActiveClients asOfDate :> IError)
                error |> lift
            | [client] when client.Status(asOfDate) = Active ->
                (Ok client) |> lift
            | [client] when client.Status(asOfDate) = Active ->
                let clientStatus = client.Status(asOfDate)
                let error = Error(FindingActiveClientError.ClientIsNotActive (client, asOfDate, clientStatus) :> IError)
                error |> lift
            | manyActiveClients ->
                resPgm {
                    let! chosenClient = choseActiveClient manyActiveClients
                    return chosenClient
                }
            