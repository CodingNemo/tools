namespace clients.lib

open System.Collections
open System.Collections.Generic

open backbone
open backbone.Dates
open backbone.IO
open backbone.Program
open backbone.Result

open clients.lib

type AddingClientError =
| ClientWithTheSameIdFound of Client
| ClientWithTheSameNameFound of Client
| ClientsWithTheSameSlugFound of Slug * Client list
    interface IError

type FindingBySlugOrIdError =
| ClientNotFound of SlugOrId<ClientId>
| TooManyClientsFound of Slug * int
    interface IError

type FindingActiveClientError =
| NoActiveClients of AsOfDate
| ClientIsNotActive of Client * AsOfDate * ClientStatus
    interface IError    

type StartingMissionError =
| FindClientError of FindingBySlugOrIdError
| MissionError of MissionError
    interface IError
    
type Clients(clients: Client list) =
    static member Empty = Clients([])
    
    member this.Add(client: Client) : Result<Clients, AddingClientError> =
            let ensureNoClientWithIdExist (id:ClientId) =
                match this.FindById id with
                | Some clientWithSameId ->
                    Error (ClientWithTheSameIdFound clientWithSameId)
                | None ->
                    Ok ()
                    
            let ensureNoClientWithNameExist (name:ClientName) =
                match this.FindByName name with
                | Some clientWithSameName ->
                    Error (ClientWithTheSameNameFound clientWithSameName)
                | None ->
                    Ok()
                    
            let ensureNoClientWithSlugExist (slug:Slug) =
                match this.FindBySlug slug with
                | [] ->
                    Ok()
                | clients -> 
                    Error (ClientsWithTheSameSlugFound (client.Slug, clients))
            
            res {
                do! ensureNoClientWithIdExist client.Id
                do! ensureNoClientWithNameExist client.Name
                do! ensureNoClientWithSlugExist client.Slug
                return Clients(client :: clients)
            }
    
    member this.Archive(client:Client, archiveDate:ArchiveDate) : Program<Result<Clients, IError>> =
        resPgm {
            match client.ArchiveDate with
            | Some _ ->
                return this
            | None ->
                let! archivedClient = client.Archive(archiveDate)
                
                let newClients = clients |> List.map (fun client ->
                    if client.Id = archivedClient.Id
                    then archivedClient
                    else client
                    )
                
                return Clients(newClients)
        }
        
    member this.SetEmail(slugOrId:SlugOrId<ClientId>, email:Email) : Program<Result<Clients, IError>> =
        resPgm {
            let! clientToSetEmail = this.FindBySlugOrId(slugOrId) |> lift
            
            let newClient = clientToSetEmail.SetEmail(email)
            
            let newClients = clients |> List.map (fun client ->
                if client.Id = newClient.Id
                then newClient
                else client
                )
            
            return Clients(newClients)
        }
    member this.GetMissionsBetween(from:Date, to':Date) =
        clients |> List.collect (_.GetMissionsBetween(from, to'))
    
    member this.StartMission (slugOrId:SlugOrId<ClientId>, startDate:StartDate, tj:Tj, title:MissionTitle option)  =
        res {
            let! client = this.FindBySlugOrId slugOrId |> Result.mapError FindClientError
            let! newClient, newMission = client.StartMission(startDate, tj, title) |> Result.mapError MissionError

            let newClients = clients |> List.map (fun client ->
                if client.Id = newClient.Id
                then newClient
                else client
                )
            
            return (Clients(newClients), newMission)
        }

    member this.EndCurrentMission (slugOrId:SlugOrId<ClientId>, endDate:EndDate)  =
        res {
            let! client = this.FindBySlugOrId slugOrId |> Result.mapError FindClientError
            let! newClient, endedMission = client.EndMission(endDate) |> Result.mapError MissionError

            let newClients = clients |> List.map (fun client ->
                if client.Id = newClient.Id
                then newClient
                else client
                )
            
            return (Clients(newClients),endedMission)
        }
        
    member this.UpdateTj (slugOrId:SlugOrId<ClientId>, newTj:Tj, updateDate: UpdateDate, newTitle: MissionTitle option)  =
        res {
            let! client = this.FindBySlugOrId slugOrId |> Result.mapError FindClientError
            
            let! newClient, endedMission = client.EndMission(updateDate.PreviousDay()) |> Result.mapError MissionError
            let newTitle = newTitle |> Option.orElse endedMission.title
            let! newClient, startedMission = newClient.StartMission(updateDate, newTj, newTitle) |> Result.mapError MissionError

            let newClients = clients |> List.map (fun client ->
                if client.Id = newClient.Id
                then newClient
                else client
                )
            
            return (Clients(newClients), (endedMission, startedMission))
        }
        
    member this.FindBySlugOrId(slugOrId:SlugOrId<ClientId>) : Result<Client, FindingBySlugOrIdError> =
        match slugOrId with
            | Slug slug ->
                match this.FindBySlug(slug) with
                | [] -> Error (ClientNotFound slugOrId)
                | [client] -> Ok(client)
                | others -> Error (TooManyClientsFound (slug, others.Length))
            | Id id ->
                match this.FindById(id) with
                | Some found ->
                    Ok(found)
                | None ->
                    Error(ClientNotFound slugOrId)
    
    member private this.FindById(id: ClientId) =
        clients |> List.tryFind (fun c -> c.Id = id)

    member this.FindBySlug(slug: Slug) : Client list =
        clients |> List.filter (fun client -> client.Slug.StartsWith slug)

    member private this.FindByName(name: ClientName) =
        clients |> List.tryFind (fun c -> c.Name = name)
        
    member this.OnlyActive(asOfDate:AsOfDate) =
        clients |> List.filter (fun client -> client.Status(asOfDate) = Active)

    interface IEnumerable<Client> with
        member this.GetEnumerator() =
            (clients :> IEnumerable<Client>).GetEnumerator()

        member this.GetEnumerator() =
            (clients :> IEnumerable).GetEnumerator()
