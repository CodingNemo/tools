﻿module Targets.Tests



open Helpers
open Definitions
open Console

let run (projects:ProjectList)
    (project:Lazy<string option>)
    (skipTests:Lazy<bool>)
    (withTestReport:Lazy<(string * string option) option>)
    (withCoverageReport:Lazy<(string * string option) option>)
    _ =

    if skipTests.Value then
        writeWarning "Skipping tests execution as requested"
        ()
    else 

        let project = project.Value
        let project = project |> Option.bind projects.FindProjectByName 

        let jUnitOptions =
            match withTestReport.Value with
            | Some (_, value) ->
                match value with
                | Some value -> $"-jUnit {value}"
                | None -> ""
            | _ -> ""

        let coverageReportArg = withCoverageReport.Value

        match coverageReportArg with
        | Some _ -> run dotnet "tool install coverlet.console" ""
        | _ -> ()

        let testableProjects = match project with 
                               | Some project when project.HasTests ->
                                    [project]
                               | Some project ->
                                    writeError $"Project {project} is not testable"
                                    []
                               | _ ->
                                    projects |> Seq.toList

        for testProject in testableProjects |> List.choose _.test do
            match coverageReportArg with
            | Some _ ->
                let targetArgs = "--target dotnet --targetargs run"
                let formatArgs = "--format cobertura"
                run coverlet $"{testProject.path} {targetArgs} {formatArgs} --no-build" testProject.path

            | _ -> run dotnet $"run{jUnitOptions}" testProject.path
