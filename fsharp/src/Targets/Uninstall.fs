﻿module Targets.Uninstall

open Console
open Definitions

open Helpers
open Targets.Install

let orderProjectsByDependenciesDesc =
    orderProjectsByDependencies >> List.rev

let run
    (projects: ProjectList)
    (project: Lazy<string option>)
    (all: Lazy<bool>)
    (tTools : EnvironmentVariable)
    _ =
    writeTitle "UNINSTALL TOOLS"
    
    let installDir = tTools.GetOrNone()
    
    match installDir with
    | None ->
        writeError "Tools were not installed. Nothing to uninstall"
    | Some installDir when installDir |> (Directory.exists >> not) -> 
        writeError "Tools were not installed. Nothing to uninstall"
    | Some installDir ->            
        
        let project = project.Value |> Option.bind projects.FindProjectByName
               
        let uninstallableProjects = projects |> Seq.filter (fun project -> project.uninstall |> Option.isSome)
                                                                           |> Seq.filter _.IsInstalledIn(installDir)
                                                                           |> Seq.toList
        let projectsToUninstall = match all.Value, project with
                                  | true, _ -> uninstallableProjects 
                                  | false, Some project -> [project]
                                  | false , None ->
                                      choices "Select the project you want to install" uninstallableProjects |> Seq.toList
                                        
        let projectsToUninstall = projectsToUninstall |> orderProjectsByDependenciesDesc
                                        
        for project in projectsToUninstall do
            try 
                writeBlankLine()
                match project.uninstall with
                | Some uninstall ->
                    uninstall project installDir
                | None ->
                    writeError $"Project {project} is not uninstallable"
            with
            | ex -> 
                writeException ex
                readKey()
    
        if Directory.isEmpty installDir then
            tTools.Clear()