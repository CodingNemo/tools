﻿module Targets.Install

open Console
open Definitions
open Helpers

let orderProjectsByDependencies (projects: Project list) =
    let rec orderProjectsByDependencies (projects: Project list) (acc: Project list) =
        match projects with
        | [] -> acc
        | _ ->
            let independent, dependent = 
                projects 
                |> List.partition (fun project -> 
                    project.dependsOn 
                    |> List.forall (fun dep -> 
                        acc |> List.exists (fun p -> p = dep)))
            
            match independent with
            | [] -> failwith "Circular dependency detected"
            | _ -> orderProjectsByDependencies dependent (acc @ independent)
    
    match projects with 
    | [] -> []
    | [project] -> [project]
    | projects -> 
        orderProjectsByDependencies projects []

let run (projects: ProjectList)
    (project:Lazy<string option>)
    (all:Lazy<bool>)
    (force:Lazy<bool>)
    (yes:Lazy<bool>)
    (out:Lazy<string option>)
    (localAppData:EnvironmentVariable)
    (tTools:EnvironmentVariable)
    _ =
    writeTitle "INSTALL TOOLS"
    
    let defaultInstallDir = localAppData.GetOrNone() <?/> "tools"
    
    let installDir = out.Value      
                     |> Option.orElse (tTools.GetOrNone())
                     |> Option.defaultWith (fun () ->
                            match defaultInstallDir with
                            | Some path ->
                                if yes.Value then
                                    path
                                else
                                    promptUserOrDefault "Please provide the desired install directory" path
                            | None ->
                                promptUser "Please provide the desired install directory"
                         )
    
    let project = project.Value |> Option.bind projects.FindProjectByName 
                 
    writePending "Setting up environment variables"
    tTools.Set(installDir)        
    writeSuccess "Environment variables ready"
    
    writeInfo $"Installing to '{installDir}'"

    let installableProjects = projects |> Seq.filter (fun project -> project.install |> Option.isSome)
                                       |> Seq.filter (fun project -> force.Value || project.IsNotInstalledIn(installDir)) 
                                       |> Seq.toList
    
    let projectsToInstall = match all.Value,project with
                            | true, _ -> installableProjects 
                            | false, Some project -> [project]
                            | false, None ->
                                 choices "Select the project you want to install" installableProjects |> Seq.toList
                                 
    if projectsToInstall.Length = 0 then
        writeWarning "No projects to install"
    else
        let projectsToInstall = orderProjectsByDependencies projectsToInstall
        
        for project in projectsToInstall do
            try 
                writeBlankLine()
                match project.install with
                | Some install ->
                    install project installDir force.Value yes.Value
                | None ->
                    writeError $"Project {project} is not installable"
            with
            | ex -> 
                writeException ex
                readKey()