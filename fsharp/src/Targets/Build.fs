﻿module Targets.Build

open Helpers
open Definitions

let run (projects:ProjectList) _=
    projects
    |> Seq.fold
        (fun acc project ->
            match project.test with
            | Some test -> acc @ [ project; test ]
            | None -> acc @ [ project ])
        []
    |> List.map (fun project -> project.name, dotnet "build" project.path)
    |> runParallel
