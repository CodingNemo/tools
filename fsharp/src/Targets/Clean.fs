﻿module Targets.Clean

open Fake.IO

let run deployDir _=
    deployDir |> Shell.cleanDir
