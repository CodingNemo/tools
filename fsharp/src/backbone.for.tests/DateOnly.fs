﻿namespace backbone.for'.tests

open System

type DateOnlySetup = Bogus.Faker -> DateOnly
 
[<Measure>] type days

module DateOnly =
    open Bogus
    let yesterday (faker:Faker) : DateOnly =
        let clock = faker.Date.LocalSystemClock.Invoke()
        clock.AddDays(-1) |> DateOnly.FromDateTime
    
    let today (faker:Faker) : DateOnly =
        let clock = faker.Date.LocalSystemClock.Invoke()
        clock |> DateOnly.FromDateTime
    
    let tomorrow (faker:Faker) : DateOnly =
        let clock = faker.Date.LocalSystemClock.Invoke()
        clock.AddDays(1) |> DateOnly.FromDateTime
        
    let private daysToTimeSpan (days:int<days>) = days |> (int >> abs >> TimeSpan.FromDays)
        
    let past (from:TimeSpan) (to':TimeSpan) (faker:Faker) : DateOnly =
        let reference = faker.Date.LocalSystemClock.Invoke()
        let from = reference.Subtract(from)
        let to' = reference.Subtract(to')
        faker.Date.Between(from, to') |> DateOnly.FromDateTime

    let pastDayFromNow (days:int<days>) (faker:Faker) : DateOnly =
        let from = TimeSpan.Zero
        let to' = days |> daysToTimeSpan 
        past from to' faker      
        
    let pastDayBetween (from:int<days>) (to':int<days>) =
        let from = from |> daysToTimeSpan
        let to' = to' |> daysToTimeSpan
        past from to'
    
    let future (to':TimeSpan) (faker:Faker) : DateOnly =
        let reference = faker.Date.LocalSystemClock.Invoke()
        let to' = reference.Add(to')
        faker.Date.Between(reference, to') |> DateOnly.FromDateTime
        
    let futureDayFromNow (to':int<days>) =
        let to' = to' |> daysToTimeSpan
        future to'

    let thisDayOfCurrentYear (day:int) (month:int) (faker:Faker) : DateOnly =
        let localSystemClock = faker.Date.LocalSystemClock.Invoke()
        let year = localSystemClock.Year
        DateOnly(year, month, day)
        
    let aMonthOfCurrentYear (faker:Faker) : DateOnly =
        let month = faker.Date.Random.Int(1, 12)
        let localSystemClock = faker.Date.LocalSystemClock.Invoke()
        let year = localSystemClock.Year
        DateOnly(year, month, 1)
        
    let aQuarterOfCurrentYear (faker:Faker) : DateOnly =
        let month = faker.Date.Random.Int(1, 12) |> (fun m -> m - m % 3)
        let localSystemClock = faker.Date.LocalSystemClock.Invoke()
        let year = localSystemClock.Year
        DateOnly(year, month, 1)
    
    let nextYear (faker:Faker) : DateOnly =
        let localSystemClock = faker.Date.LocalSystemClock.Invoke()
        let year = localSystemClock.Year
        DateOnly(year + 1, 1, 1)