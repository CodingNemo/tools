﻿namespace backbone.for'.tests

open System.IO
open NHamcrest.Core


module Asserts =
    module Exe = 
        let successful =
            CustomMatcher<obj>(
                description = "be successful (code should be 0)",
                matches = fun run ->
                    match run with
                    | :? Run as r -> r.code = 0
                    | _ -> false
            ) :> NHamcrest.IMatcher<obj>
            
        let inError code =
            CustomMatcher<obj>(
                description = $"be in error with code {code}",
                matches = fun run ->
                    match run with
                    | :? Run as r -> r.code = code
                    | _ -> false
            ) :> NHamcrest.IMatcher<obj>
            
    module IO = 
        let anExistingFolder =
            CustomMatcher<obj>(
                description = "exist",
                matches = fun run ->
                    match run with
                    | :? string as path -> Directory.Exists(path)
                    | _ -> false
            ) :> NHamcrest.IMatcher<obj>
            
        let anExistingFile =
            CustomMatcher<obj>(
                description = "exist",
                matches = fun run ->
                    match run with
                    | :? string as path -> File.Exists(path)
                    | _ -> false
            ) :> NHamcrest.IMatcher<obj>        

