﻿namespace backbone.for'.tests

open System 
open System.IO
open System.Reflection
open System.Threading
open Spectre.Console
open Spectre.Console.Testing

type RunConsoleOutput = {
    StdOut: string
    StdErr: string
}
    
module Args =
    let defaulting (defaultedArgs: Map<string, unit -> string>) (args:string) =
        let cleanArgs (a:string) = a.Replace("$..$", "")
            
        let mutable additionalArgs : string list = []
        
        for defaultedArg in defaultedArgs do
            if not(args.Contains(defaultedArg.Key)) then
                let arg = defaultedArg.Value()
                additionalArgs <- $"{defaultedArg.Key} {arg}"::additionalArgs
    
        match additionalArgs with
        | [] -> cleanArgs args
        | _ ->
            let additionalArgs = String.concat " " additionalArgs
            if args.Contains("$..$") then
                args.Replace("$..$", additionalArgs)
            else
                $"{args} {additionalArgs}"
    
type Run = {
    code:int
    console:RunConsoleOutput
}

type RunException(exeName: string, args:string, innerException: exn) =
    inherit Exception($"Error running :{Environment.NewLine}---{Environment.NewLine}\t{exeName} {args}{Environment.NewLine}---", innerException)
type Scenario = interface end

type ExeContext = {
    runId: Guid
    workingFolder: string
}
    with
    
        static member Build (exeName:string) : ExeContext =
            let runId = Guid.NewGuid()
            let ctx = { runId = runId; workingFolder = Path.Combine(Path.GetTempPath(), "tests", exeName, runId.ToString()); }
            ctx 

type ISimulate = ExeContext -> Scenario -> unit


type Exe(exeName:string, scenario:Scenario, simulators: ISimulate list, ?size:(int*int)) =
    
    // mandatory because of the console
    static let mutex = new Mutex(false, "Global\\ExeRunMutex")

    let parseArgs args =
        System.Text.RegularExpressions.Regex.Split(args, @"\s(?=(?:[^'""\r\n]|'[^']*'|""[^""]*"")*$)")
    let invoke (args:string[]) =
        
        let previousConsole = AnsiConsole.Console
        let previousStdErr = Console.Error
        
        try
            let newStdErr = new StringWriter()
            Console.SetError(newStdErr)
            
            let console = new TestConsole()
            
            match size with
            | Some (width, height) ->
                console.Profile.Width <- width
                console.Profile.Height <- height
            | None -> ()
            
            AnsiConsole.Console <- console
           
            let assembly = Assembly.Load(exeName)
            
            let entryPoint = assembly.EntryPoint
            if entryPoint = null then
                failwith $"No entry point found on assembly {exeName}"
            let result = entryPoint.Invoke(null, [|args|])
            
            let stdOut = console.Output
            let stdErr = newStdErr.ToString()
            
            let code = result :?> int
            
            {
                code = code
                console = {
                    StdOut = stdOut
                    StdErr = stdErr
                }
            }
        finally
            AnsiConsole.Console <- previousConsole
            Console.SetError(previousStdErr)
                        
    let context = ExeContext.Build(exeName)
    
    let directory = Directory.CreateDirectory(context.workingFolder)
    
    do if not directory.Exists then failwith $"Could not create working folder {context.workingFolder}" 
   
    do simulators |> List.iter (fun simulate -> simulate context scenario)

    member val Context = context
    
    member this.Run(args: string) =
        mutex.WaitOne() |> ignore
        try
            try
                args |> parseArgs |> invoke
            with
            | :? TargetInvocationException as exn ->
                raise(RunException(exeName, args, exn.InnerException))
            | exn ->
                raise(RunException(exeName, args, exn)) 
        finally
            mutex.ReleaseMutex()

    interface IDisposable with
        member this.Dispose() =
            try
                if Directory.Exists context.workingFolder then
                    Directory.Delete(context.workingFolder, true)
            with exn ->
                ()

module Exe =
    let run (args: string) (exe:Exe) =
        exe.Run(args)