﻿namespace backbone.for'.tests 

module Faker = 
    open Bogus

    let create (seed: int option) =
        let faker = Faker()
        
        match seed with
        | Some seed -> faker.Random <- Randomizer(seed)
        | None -> ()
        
        let dateReference () = System.DateTime(2024, 6, 17) // maybe use a random date
        faker.Date.LocalSystemClock <- dateReference
        faker

    let from (faker:Faker) =
        let seed = faker.Random.Int()
        let childFaker = Faker()
        if faker.Date.LocalSystemClock <> null then
            childFaker.Date.LocalSystemClock <- faker.Date.LocalSystemClock
        childFaker.Random <- Randomizer(seed)
        childFaker
        
