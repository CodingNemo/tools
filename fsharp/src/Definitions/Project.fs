﻿namespace Definitions

open System
open Helpers

[<CustomEquality; NoComparison>]
type Project =
    { name: string
      aliases: string list
      path: string
      install: (Project -> string -> bool -> bool -> unit) option
      uninstall: (Project -> string -> unit) option
      dependsOn: Project list
      children: Project list
      test: Project option }

    static member Console(name: string, ?aliases: string list) =
        { name = name
          aliases = aliases |> Option.defaultValue []
          path = $"./%s{name}"
          install = None
          uninstall = None
          dependsOn = []
          children = []
          test = None }

    static member Library(name: string) =
        { name = name
          aliases = []
          path = $"./%s{name}"
          install = None
          uninstall = None
          dependsOn = []
          children = []
          test = None }

    override this.Equals(obj) =
        match obj with
        | :? Project as other -> this.name = other.name
        | _ -> false
    
    override this.GetHashCode() =
        hash this.name
            
    interface IEquatable<Project> with
        member this.Equals(other) =
            this.name = other.name
    
    member this.HasTests = 
        this.test |> Option.isSome

    member this.WithTests() =
        let testProject =
            { name = $"{this.name}.tests"
              aliases = []
              path = $"./%s{this.name}.tests"
              install = None
              uninstall = None
              dependsOn = []
              children = []
              test = None }

        { this with test = Some testProject }
    
    member this.IsInstalledIn(path:string) =
       let installPath = path </> this.path
       Directory.exists installPath
       
    member this.IsNotInstalledIn(path:string) =
       not(this.IsInstalledIn(path))
       
    member this.Install(install: Project -> string -> bool -> bool -> unit) =
        { this with install = Some install }
     
    member this.Uninstall(uninstall: Project -> string -> unit) =
        { this with uninstall = Some uninstall }
        
    member this.DependsOn(projects: Project list) =
        { this with dependsOn = projects }

    member this.WithChildren(projects: Project list) =
        { this with children = projects }
    
    override this.ToString() = this.name


type ProjectList(projects: Project list) =
    member this.FindProjectByName(name: string) =
        projects |> List.tryFind (fun project -> project.name = name)
        
    
    interface System.Collections.Generic.IEnumerable<Project> with
        member this.GetEnumerator() =
            (projects :> seq<Project>).GetEnumerator()

        member this.GetEnumerator(): System.Collections.IEnumerator =
            (projects :> seq<Project>).GetEnumerator()
    