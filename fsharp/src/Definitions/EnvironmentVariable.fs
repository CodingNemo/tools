﻿namespace Definitions

open System

type EnvironmentVariable(name:string, scope:EnvironmentVariableTarget) =
    member this.Set(value:string)=
        Environment.SetEnvironmentVariable(name, value, scope)
               
    member this.GetOrNone()=
        let value = Environment.GetEnvironmentVariable(name, scope)
        if String.IsNullOrWhiteSpace(value) then
            None
        else
            Some value

    member this.Get()=
        match this.GetOrNone() with
        | Some value -> value
        | None -> failwith $"Environment variable {name} is not defined"

    member this.Clear()=
        Environment.SetEnvironmentVariable(name, null, scope)



type PathEnvironmentVariable() =
    
    let Path = EnvironmentVariable("Path", EnvironmentVariableTarget.User)
    member this.Add(newPath:string)=
        let mutable path = Path.Get()

        if not(path.Contains(newPath)) then 
            path <- $"{path};{newPath}"
            Path.Set(path)
            true
        else
            false
               
    member this.Remove(newPath:string)=
        let mutable path = Path.Get()

        if path.Contains(newPath) then 
            path <- path.Replace($";{newPath}", "")
            Path.Set(path)
            true
        else
            false

    


