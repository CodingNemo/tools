namespace schedule.tests.ShowSchedule

open Xunit

open backbone.for'.tests


open VerifyXunit

open schedule.tests.Exe
open schedule.tests.ShowSchedule.Scenario
open schedule.tests.ShowSchedule.Simulators

type Should() =

    let scheduleExeFor = scheduleExe [simulateClientsSource;simulateScheduleSources]
    
    [<Fact>]
    member _.``Display a monthly Schedule`` () =
        task {
            let mutable reference : string = ""
            let scenario = ShowSchedule(1008)
                            .AddClients()
                            .AddGeneratedMonth(&reference, DateOnly.aMonthOfCurrentYear)
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let showScheduleRun = scheduleExe |> show reference
                                              |> withSuccess
            
            let! _ = Verifier.Verify showScheduleRun
            ()
        }

    [<Fact>]
    member _.``Display a monthly Schedule with weekends`` () =
        task {
            let mutable reference : string = ""
            let scenario = ShowSchedule(1020)
                            .AddClients()
                            .AddGeneratedMonth(&reference, DateOnly.aMonthOfCurrentYear)
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let showScheduleRun = scheduleExe |> showWithWeekends reference
                                              |> withSuccess
            
            let! _ = Verifier.Verify showScheduleRun
            ()
        }
        
    [<Fact>]
    member _.``Display default monthly Schedule`` () =
        task {
            let mutable reference : string = ""
            let scenario = ShowSchedule(1020)
                            .AddClients()
                            .AddGeneratedMonth(&reference)
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let showScheduleRun = scheduleExe |> showDefault scenario.AsOfDate
                                              |> withSuccess
            
            let! _ = Verifier.Verify showScheduleRun
            ()
        }
        
    [<Fact>]
    member _.``Display a not generated monthly Schedule`` () =
        task {
            let scenario = ShowSchedule(1020)
                            .AddClients()
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let showScheduleRun = scheduleExe |> showDefault scenario.AsOfDate
                                              |> withSuccess
            
            let! _ = Verifier.Verify showScheduleRun
            ()
        }

    
    [<Fact>]
    member _.``Display a quarterly Schedule`` () =
        task {
            let mutable reference : string = ""
            let scenario = ShowSchedule(1009)
                            .AddClients()
                            .AddGeneratedQuarter(&reference, DateOnly.aQuarterOfCurrentYear)
                            
            use scheduleExe = scheduleExeFor scenario
        
            let showScheduleRun =
                scheduleExe |> show reference
                            |> withSuccess  
            
            let! _ = Verifier.Verify showScheduleRun
            ()
        }
        
    [<Fact>]
    member _.``Display a yearly Schedule`` () =
        task {
            let mutable reference : string = ""
            let scenario = ShowSchedule(1010)
                            .AddClients()
                            .AddGeneratedYear(&reference, DateOnly.nextYear)
                            
            use scheduleExe = scheduleExeFor scenario
        
            let showScheduleRun = scheduleExe |> show reference
                                               |> withSuccess

            let! _ = Verifier.Verify showScheduleRun
            ()  
        }
