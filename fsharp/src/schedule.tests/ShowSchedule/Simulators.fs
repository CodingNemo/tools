﻿namespace schedule.tests.ShowSchedule

open backbone.for'.tests
open schedule.tests.Specs.Simulators


module Simulators =
    
    open clients.tests.specs.Simulators
    open schedule.tests.ShowSchedule.Scenario
    
    let simulateClientsSource (context:ExeContext) (scenario:Scenario)  =
        match scenario with
        | :? ShowSchedule as showSchedule -> 
            showSchedule.Clients |> createClientsFileFrom context.workingFolder showSchedule.ClientsSource
        | _ ->
            ()
    
    let simulateScheduleSources (context:ExeContext) (scenario:Scenario)  =
        match scenario with
        | :? ShowSchedule as showSchedule -> 
            showSchedule.Schedule.MonthlySchedules
            |> List.iter (fun schedule -> schedule |> createMonthlySchedule context.workingFolder showSchedule.ScheduleSource)
        | _ ->
            ()