﻿namespace schedule.tests.ShowSchedule

open clients.tests.Specs
open schedule.tests.Specs

module Scenario =
    open backbone.for'.tests
    
    type ShowSchedule(?seed:int) =
        let faker = Faker.create(seed)

        let mutable asOfDate = faker |> DateOnly.today
        let mutable clients = [] : ClientSpec list
        let mutable schedule = ScheduleSpec()
              
        let setupMonthlySchedule (monthlySchedule:MonthlyScheduleSpec) =
            let lastActiveMission client =
                let activeMissions = client.missions
                                                  |> List.sortBy _.startDate
                                                  |> List.filter (_.endDate.IsNone)
                match activeMissions with
                | [] -> failwith $"No active mission defined for client {client.slug}"
                | [activeMission] ->
                    activeMission
                | _ -> failwith $"Too many active missions defined for client {client.slug}"
            
            match clients with
            | [] -> monthlySchedule
            | [client] ->
                let activeMission = lastActiveMission client    
                monthlySchedule.AddFullMissionLine($"{activeMission.id}")
            | head::tail ->
                let headMission = lastActiveMission head
                let mutable monthlySchedule = monthlySchedule.AddFullMissionLine($"{headMission.id}")
                tail |> List.iter (fun client ->
                    let activeMission = lastActiveMission client
                    monthlySchedule <- monthlySchedule.AddEmptyMissionLine($"{activeMission.id}")
                )
                monthlySchedule

        member val ClientsSource : ClientsSourceSpec = ClientsSourceSpec.Default
        member val ScheduleSource : ScheduleSourceSpec = ScheduleSourceSpec.Default

        member val Schedule : ScheduleSpec = schedule
        
        member this.Clients = clients
        member this.AsOfDate = asOfDate
        
        member this.AddClient (?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            let builder = ClientSpec.Builder(faker)
            
            let builder = match setup with
                          | Some setup -> setup builder
                          | None -> builder
            
            let client = builder.Build()
            clients <- client :: clients
            this
        
        member this.AddClients() =
            let clientsCount = faker.Random.Int(2, 6)
            for _ in 0..clientsCount do
                this.AddClient(_.AddMission()) |> ignore
            this
        
        member this.AddGeneratedMonth (reference: byref<string>, ?setup:DateOnlySetup) =
            let date = match setup with
                       | Some setup -> setup(faker)
                       | None -> asOfDate
            reference <- schedule.AddMonthlySchedule(date,setupMonthlySchedule)   
            this
            
        member this.AddGeneratedQuarter (reference: byref<string>,?setup:DateOnlySetup) =
            let date = match setup with
                       | Some setup -> setup(faker)
                       | None -> asOfDate
            reference <- schedule.AddQuarterlySchedule(date,setupMonthlySchedule)   
            this
                
        member this.AddGeneratedYear (reference: byref<string>, ?setup:DateOnlySetup) =
            let date = match setup with
                       | Some setup -> setup(faker)
                       | None -> asOfDate
            reference <- schedule.AddYearlySchedule(date,setupMonthlySchedule)   
            this
                
        interface Scenario

