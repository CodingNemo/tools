﻿namespace schedule.tests.SetActivity

open clients.tests.Specs
open schedule.tests.Specs

module Scenario = 
    open backbone.for'.tests
            
    type SetActivity(?seed:int) =
        let faker = Faker.create(seed)
        let mutable targetDate = faker |> DateOnly.tomorrow
        let mutable schedule = MonthlyScheduleSpec.InitFromDate(targetDate)
        let mutable clients = [] : ClientSpec list
        
        member val ClientsSource : ClientsSourceSpec = ClientsSourceSpec.Default
        member val ScheduleSource : ScheduleSourceSpec = ScheduleSourceSpec.Default

        member this.Clients = clients
        member this.Schedule = schedule
        member this.TargetDate = targetDate

        member private this.AddClient (?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            let builder = ClientSpec.Builder(faker)
            
            let builder = match setup with
                          | Some setup -> setup builder
                          | None -> builder
            
            let client = builder.Build()
            clients <- client :: this.Clients
            this
        
        member this.AddFullTimeClient(?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            this.AddClient(fun client ->
                let client = client.AddMission(_.StartedOn(DateOnly.yesterday))
                match setup with
                | Some setup -> setup client
                | None -> client
            ) |> ignore
            
            let lastClient = this.Clients |> List.last
            let lastMissionIndex,lastMission = lastClient.missions
                                              |> List.sortBy _.startDate
                                              |> List.indexed
                                              |> List.last
            
            if lastMission.endDate.IsNone then
                schedule <- schedule.AddFullMissionLine $"{lastClient.slug}#{lastMissionIndex}"
                ()
            
            this
            
        member this.AddEmptyClient(?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            this.AddClient(fun client ->
                let client = client.AddMission(_.StartedOn(DateOnly.yesterday))
                match setup with
                | Some setup -> setup client
                | None -> client
            ) |> ignore
            
            let lastClient = this.Clients |> List.last
            let lastMissionIndex,lastMission = lastClient.missions
                                              |> List.sortBy _.startDate
                                              |> List.indexed
                                              |> List.last
            
            if lastMission.endDate.IsNone then
                schedule <- schedule.AddEmptyMissionLine $"{lastClient.slug}#{lastMissionIndex}"
                ()
            
            this
            
        member this.WithOffDate (span:string, ?date:DateOnlySetup) =
            let date = match date with
                       | Some date -> date(faker)
                       | None -> targetDate
            
            schedule <- schedule.AddOffDate(date, span)
            this
            
        member this.WithHoliday (holidayName:string, date:DateOnlySetup) =
            schedule <- schedule.AddHoliday(date(faker), holidayName)
            this

        member this.WithTargetDate (date:DateOnlySetup) =
            targetDate <- date(faker)
            schedule <- MonthlyScheduleSpec.InitFromDate(targetDate)
            this
        
 
        interface Scenario
