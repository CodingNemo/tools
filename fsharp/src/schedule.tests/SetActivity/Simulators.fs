﻿namespace schedule.tests.SetActivity

open backbone.for'.tests
open schedule.tests.Specs.Simulators
open clients.tests.specs.Simulators

module Simulators = 
    open schedule.tests.SetActivity.Scenario
    
    let simulateClientsSource (context:ExeContext) (scenario:Scenario)  =
        match scenario with
        | :? SetActivity as listClients -> 
            listClients.Clients |> createClientsFileFrom context.workingFolder listClients.ClientsSource
        | _ ->
            ()

    let simulateMonthlySchedule (context:ExeContext) (scenario:Scenario)  =
        match scenario with
        | :? SetActivity as listClients -> 
            listClients.Schedule |> createMonthlySchedule context.workingFolder listClients.ScheduleSource
        | _ ->
            ()