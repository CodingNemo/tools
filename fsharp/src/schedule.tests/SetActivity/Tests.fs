﻿namespace schedule.tests.SetActivity

open Xunit

open VerifyXunit

open backbone.for'.tests
open schedule
open schedule.tests.Exe
open schedule.tests.SetActivity.Scenario
open schedule.tests.SetActivity.Simulators

type Should() =
    let scheduleExeFor = scheduleExe [simulateClientsSource; simulateMonthlySchedule]

    [<Fact>]
    member _.``Set a full day off activity for a specific date`` () =
        task {
            let scenario = SetActivity(7134)
                               .AddFullTimeClient()
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let setActivityRun = scheduleExe |> set "off" scenario.TargetDate "F"
                                 |> withSuccess
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }
        
    [<Fact>]
    member _.``Set a morning day off activity for a specific date`` () =
        task {
            let scenario = SetActivity(8134)
                               .AddFullTimeClient()
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let setActivityRun = scheduleExe |> set "off" scenario.TargetDate "M"
                                 |> withSuccess            
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }
        
    [<Fact>]
    member _.``Set an afternoon day off activity for a specific date`` () =
        task {
            let scenario = SetActivity(9134)
                               .AddFullTimeClient()
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let setActivityRun = scheduleExe |> set "off" scenario.TargetDate "A"
                                 |> withSuccess
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }
        
    [<Fact>]
    member _.``Set an afternoon day off activity on a already off day in the morning`` () =
        task {
            let scenario = SetActivity(5134)
                               .AddFullTimeClient()
                               .WithOffDate("AM")
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let setActivityRun = scheduleExe |> set "off" scenario.TargetDate "PM"
                                             |> withSuccess
            
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }    

    [<Fact>]
    member _.``Set a full client day activity for a specific date`` () =
        task {
            let mutable emptyClientSlug = "empty-client"
            
            let scenario = SetActivity(6134)
                               .AddFullTimeClient()   
                               .AddEmptyClient _.WithSlug(&emptyClientSlug)
                            
            use scheduleExe = scheduleExeFor scenario 
                    
            let setActivityRun = scheduleExe |> set emptyClientSlug scenario.TargetDate "F"
                                             |> withSuccess            
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }
        
    [<Fact>]
    member _.``Set a morning mission day activity for a specific date`` () =
        task {
            let mutable emptyClientSlug = "empty-client"
            
            let scenario = SetActivity(6134)
                               .AddFullTimeClient()
                               .AddEmptyClient _.WithSlug(&emptyClientSlug)
                                                .ClearMissions()
                                                .AddMission(_.StartedOn(DateOnly.pastDayFromNow(-10<days>))
                                                             .EndedOn(DateOnly.pastDayFromNow(-5<days>))
                                                             )
                                                .AddMission(_.WithoutTitle()
                                                             .StartedOn(DateOnly.pastDayFromNow(-3<days>)))
                            
            use scheduleExe = scheduleExeFor scenario 
                    
            let setActivityRun = scheduleExe |> set $"{emptyClientSlug}#1" scenario.TargetDate "MORNING"
                                             |> withSuccess            
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }
        
    [<Fact>]
    member _.``Fail to set an afternoon client day activity on a holiday date`` () =
        task {
            let mutable emptyClientSlug = "empty-client"
            
            let christmasDay = DateOnly.thisDayOfCurrentYear 25 12
            
            let scenario = SetActivity(4134)
                               .WithTargetDate(christmasDay)
                               .WithHoliday("Christmas", christmasDay)
                               .AddFullTimeClient()
                               .AddEmptyClient _.WithSlug(&emptyClientSlug)
                            
            use scheduleExe = scheduleExeFor scenario 
                    
            let setActivityRun = scheduleExe |> set emptyClientSlug scenario.TargetDate "AFTERNOON"
                                             |> withError ErrorCodes.Schedule
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }

    [<Fact>]
    member _.``Fail to set an off day activity on a holiday date`` () =
        task {
            let christmasDay = DateOnly.thisDayOfCurrentYear 25 12
            
            let scenario = SetActivity(4134)
                               .WithTargetDate(christmasDay)
                               .WithHoliday("Christmas", christmasDay)
                               .AddFullTimeClient()
                               .AddEmptyClient()
                            
            use scheduleExe = scheduleExeFor scenario 
                    
            let setActivityRun = scheduleExe |> set "off" scenario.TargetDate "FULL"
                                             |> withError ErrorCodes.Schedule
            
            let! _ = Verifier.Verify setActivityRun
            ()
        }