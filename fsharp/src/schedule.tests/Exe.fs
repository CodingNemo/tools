﻿namespace schedule.tests

open backbone.Dates
open clients.tests.Specs
open schedule
open schedule.Interpreter
open schedule.tests.Specs

module Exe =
    
    open backbone.for'.tests
    open backbone.for'.tests.Exe
    open backbone.for'.tests.Asserts.Exe

    open FsUnit.Xunit

    type ScheduleExe = {
        Exe:Exe
        ClientsSource:ClientsSourceSpec
        ScheduleSource:ScheduleSourceSpec
    }
        with
            interface System.IDisposable with
                member this.Dispose() =
                    (this.Exe :> System.IDisposable).Dispose()
    
    module ScheduleExe =
        let run (args:string) (exe:ScheduleExe) =
            let args = args |> Args.defaulting (Map.empty
                |> Map.add "--clients-location" (fun _ -> $"folder:{System.IO.Path.Combine(exe.Exe.Context.workingFolder, exe.ClientsSource.folder)}")
                |> Map.add "--schedule-location" (fun _ -> $"folder:{System.IO.Path.Combine(exe.Exe.Context.workingFolder, exe.ScheduleSource.folder)}")
            )
            
            exe.Exe |> run args
            

    let inline scheduleExe<'scenario
                        when 'scenario :> Scenario
                        and ^scenario : (member ClientsSource:ClientsSourceSpec)
                        and ^scenario : (member ScheduleSource:ScheduleSourceSpec)
    > simulators (scenario:'scenario) =
        {
            Exe = new Exe("schedule", scenario, simulators, (130, 40))
            ClientsSource = scenario.ClientsSource
            ScheduleSource = scenario.ScheduleSource
        }
    
    open ScheduleExe
    
    let showDefault (asOfDate:System.DateOnly) (exe:ScheduleExe) =
        let asOfDate = asOfDate.ToString("yyyy-MM-dd")
        exe |> run $"show --as-of {asOfDate} --no-wait"
    
    let show (reference:string) (exe:ScheduleExe) =
        exe |> run $"show {reference} --no-wait"
   
    let showWithWeekends (reference:string) (exe:ScheduleExe) =
        exe |> run $"show {reference} --weekends --no-wait"
    
    let generate (reference:string) (asOfDate:System.DateOnly) (exe:ScheduleExe) =
        let asOfDate = asOfDate.ToString("yyyy-MM-dd")
        exe |> run $"generate {reference} --as-of {asOfDate}"
        
    let set (activity:string) (date:System.DateOnly) (span:string) (exe:ScheduleExe) =
        let date = date.ToString("yyyy-MM-dd")
        exe |> run $"set {activity} {date} {span}" 
        
    let withSuccess (run:Run) =
        run |> should be successful
        run
        
    let withError (errorCode:ErrorCodes) (run:Run) =
        run |> should be (inError (int errorCode))
        run