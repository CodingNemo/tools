﻿namespace schedule.tests

open Xunit
open schedule.lib
open FsUnit.Xunit

open backbone.Result
open backbone.Dates
open backbone.Dates.Year

module ReferenceShould =
    open Schedule
    
    let monthRef month y = Reference.ForMonth {
        month = month
        year = year y
    }
    
    [<Fact>]
    let ``parse a month with the format MMMyy``() =
        Reference.Parse("Jan22")
        |> value
        |> should equal (monthRef January  2022)
        Reference.Parse("Feb23")
        |> value
        |> should equal (monthRef February 2023)
        Reference.Parse("Mar24")
        |> value
        |> should equal (monthRef March 2024)
        Reference.Parse("Apr25")
        |> value
        |> should equal (monthRef April 2025)
        Reference.Parse("May26")
        |> value
        |> should equal (monthRef May 2026)
        Reference.Parse("Jun27")
        |> value
        |> should equal (monthRef June 2027)
        Reference.Parse("Jul28")
        |> value        
        |> should equal (monthRef July 2028)
        Reference.Parse("Aug29")
        |> value                
        |> should equal (monthRef August 2029)
        Reference.Parse("Sep30")
        |> value
        |> should equal (monthRef September 2030)
        Reference.Parse("Oct31")
        |> value
        |> should equal (monthRef October 2031)
        Reference.Parse("Nov32")
        |> value
        |> should equal (monthRef November 2032)
        Reference.Parse("Dec33")
        |> value
        |> should equal (monthRef December 2033)

    [<Fact>]
    let ``parse a quarter with the format Qnyy``() =
        Reference.Parse("Q118")
        |> value
        |> should equal (Reference.ForQuarter (First, year 2018))
        Reference.Parse("Q219")
        |> value
        |> should equal (Reference.ForQuarter (Second, year 2019))
        Reference.Parse("Q320")
        |> value
        |> should equal (Reference.ForQuarter (Third, year 2020))
        Reference.Parse("Q421")
        |> value
        |> should equal (Reference.ForQuarter (Last, year 2021))
    
    [<Fact>]
    let ``parse a year with the format Yyy``() =
        Reference.Parse("Y22")
        |> value
        |> should equal (Reference.ForYear (year 2022))
        Reference.Parse("Y25")
        |> value
        |> should equal (Reference.ForYear (year 2025))