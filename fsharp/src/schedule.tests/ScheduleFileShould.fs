﻿namespace schedule.tests

open Xunit
open FsUnit.Xunit

open schedule.lib.Interpreters

module ScheduleFileShould =            
    [<Fact>]
    let ``parse a table``() =
        let serializedLines = [
            "SomeMonth        | 1  | 2  | 3  | 4  | 5"
            "activityId:1234  | X  |    |    | X  |  "
            "activityId:4567  |    | X  |    |    | X"
            ]

        let rowParseResult = MonthlyScheduleFile.Parser.parseLines serializedLines
             
        match rowParseResult with
        | Result.Ok table ->
            let month, (days, rows) = table
            month |> should equal "SomeMonth"
            days |> should equal [1..5]
            rows |> should equal [
                ("activityId:1234", ["X"; ""; ""; "X"; ""])
                ("activityId:4567", [""; "X"; ""; ""; "X"])
            ]
            
        | Result.Error error -> 
            failwith error