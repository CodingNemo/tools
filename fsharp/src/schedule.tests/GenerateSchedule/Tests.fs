namespace schedule.tests.GenerateSchedule

open Xunit

open backbone.for'.tests.Asserts.Exe

open FsUnit.Xunit
open VerifyXunit

open schedule.tests.Exe
open schedule.tests.GenerateSchedule.Scenario
open schedule.tests.GenerateSchedule.Simulators

type Should() =

    let scheduleExeFor = scheduleExe [simulateClientsSource]
    
    [<Fact>]
    member _.``Generate a monthly Schedule`` () =
        task {
            let scenario = GenerateSchedule(7134)
                            .AddClient(_.AddMission())
                            
            use scheduleExe = scheduleExeFor scenario 
        
            let generateScheduleRun = scheduleExe |> generate "Feb26" scenario.AsOfDate

            generateScheduleRun
            |> should be successful
            
            let! _ = Verifier.Verify generateScheduleRun
            ()
        }

    [<Fact>]
    member _.``Generate a quarterly Schedule`` () =
        task {
            let scenario = GenerateSchedule(8134)
                            .AddClient(_.AddMission())
                            
            use scheduleExe = scheduleExeFor scenario
        
            let generateScheduleRun =
                scheduleExe |> generate "Q325" scenario.AsOfDate
    
            generateScheduleRun
            |> should be successful
            
            let! _ = Verifier.Verify generateScheduleRun
            ()
        }
        
    [<Fact>]
    member _.``Generate a yearly Schedule`` () =
        task {
            let scenario = GenerateSchedule(9134)
                            .AddClient(_.AddMission())
                            
            use scheduleExe = scheduleExeFor scenario
        
            let generateScheduleRun =
                scheduleExe |> generate "Y27" scenario.AsOfDate
    
            generateScheduleRun
            |> should be successful
            
            let! _ = Verifier.Verify generateScheduleRun
            ()  
        }
