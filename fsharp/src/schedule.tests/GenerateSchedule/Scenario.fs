﻿namespace schedule.tests.GenerateSchedule

open clients.tests.Specs
open schedule.tests.Specs

module Scenario =
    open backbone.for'.tests
    
    type GenerateSchedule(?seed:int) =
        let faker = Faker.create(seed)

        let mutable asOfDate = faker |> DateOnly.today
        let mutable clients = [] : ClientSpec list
                
        member val ClientsSource : ClientsSourceSpec = ClientsSourceSpec.Default
        member val ScheduleSource : ScheduleSourceSpec = ScheduleSourceSpec.Default

        member this.Clients = clients
        member this.AsOfDate = asOfDate
        
        member this.AddClient (?setup:ClientSpec.Builder -> ClientSpec.Builder) =
            let builder = ClientSpec.Builder(faker)
            
            let builder = match setup with
                          | Some setup -> setup builder
                          | None -> builder
            
            let client = builder.Build()
            clients <- client :: this.Clients
            this
                
                
        interface Scenario


