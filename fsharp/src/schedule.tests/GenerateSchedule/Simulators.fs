﻿namespace schedule.tests.GenerateSchedule

open backbone.for'.tests


module Simulators =
    
    open clients.tests.specs.Simulators
    open schedule.tests.GenerateSchedule.Scenario
    
    let simulateClientsSource (context:ExeContext) (scenario:Scenario)  =
        match scenario with
        | :? GenerateSchedule as listClients -> 
            listClients.Clients |> createClientsFileFrom context.workingFolder listClients.ClientsSource
        | _ ->
            ()
