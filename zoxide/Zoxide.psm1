function Add-ZoxideInitToProfile {
    Write-Pending "Adding Zoxide initialization to profile"
    $profileContent = Get-Content $PROFILE -Raw

    $initCommand = "Invoke-Expression (& { (zoxide init powershell | Out-String) })"
    
    if ($profileContent.Contains("# Init Zoxide")) {
        Write-Info "Profile already contains Zoxide initialization"
    }
    else {
        if(-not(Test-Path $PROFILE)) {
            New-Item -Type File $PROFILE
        }
        Add-Content $PROFILE "# Init Zoxide"
        Add-Content $PROFILE $initCommand
        Add-Content $PROFILE "# End Zoxide init"
        Add-Content $PROFILE ""
        Write-Host ""
        Write-Host ""
        
    }
}

function Test-Fzf {
    $fzf = Get-Command fzf -ErrorAction SilentlyContinue

    return $null -ne $fzf
}

function Install-Fzf {
    if (-not(Test-Fzf)) {
        Write-Pending "Installing fzf"
        winget install fzf
        Write-Success "Fzf installed"        
    }
    else {
        Write-Info "Fzf already installed"
    }

}

function Test-Zoxide {
    $zoxide = Get-Command zoxide -ErrorAction SilentlyContinue

    return $null -ne $zoxide
}

function Install-Zoxide {
    if (-not(Test-Zoxide)) {
        Write-Pending "Installing zoxide"
        winget install ajeetdsouza.zoxide -s winget
        Write-Success "Zoxide installed"        
    }
    else {
        Write-Info "Zoxide already installed"
    }

    Install-Fzf

    Write-Pending "Setuping Zoxide"

    Add-ZoxideInitToProfile

    Write-Success "Zoxide ready"
}

Export-ModuleMember -Function Install-Zoxide