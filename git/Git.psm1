function Test-Git {
    $git = Get-Command git -ErrorAction SilentlyContinue

    return $null -ne $git
}

function Install-Git {

    if (-not(Test-Git)) {
        Write-Pending "Installing Git"
        winget install Git.Git -s winget
        Write-Success "Git installed"
    }
    else {
        Write-Info "Git already installed"
    }

    $includePath = Join-Path $PSScriptRoot ".gitconfig_include"

    Write-Pending "Adding '$includePath' to global gitconfig"
    # Check already included    
    $actualIncludePath = git config --global include.path
    if ($actualIncludePath -ne $includePath) {
        git config --global include.path $includePath
        Write-Success "Include path: '$includePath'"
    }
    else {
        Write-Info "Include path already set with '$actualIncludePath'"
    }
    
    Write-Pending "Adding user info to global gitconfig"
    
    $userName = git config --global user.name
    if ($null -eq $userName) {
        $userName = Read-UserInput "Name"
        git config --global user.name $userName
    }
    else {
        Write-Info "Name: '$userName'"
    }

    $userEmail = git config --global user.email
    if ($null -eq $userEmail) {
        $userEmail = Read-UserInput "Email"
        git config --global user.email $userEmail

    }
    else {
        Write-Info "Email: '$userEmail'"
    }
    
    Write-Success "Git user info setup done"
}


Export-ModuleMember -Function Install-Git