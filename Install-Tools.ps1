if (-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    # Relaunch the script with elevated privileges
    Start-Process pwsh -Verb runAs -ArgumentList "-File `"$PSCommandPath`" -NoExit"
    exit
}

Join-Path $PSScriptRoot "Powershell" "Console.psm1" | Import-Module

Write-Title "INSTALLING ENVIRONMENT"

Write-Section "SETUP GIT"
Join-Path $PSScriptRoot "git" "Git.psm1" | Import-Module
Install-Git

Write-Section "SETUP WINDOWS TERMINAL"
Join-Path $PSScriptRoot "WindowsTerminal" "WindowsTerminal.psm1" | Import-Module
Install-WindowsTerminal

Write-Section "SETUP POWERSHELL"

Write-Step 1 "Zoxyide"
Join-Path $PSScriptRoot "zoxide" "Zoxide.psm1" | Import-Module
Install-Zoxide

Write-Step 2 "Oh-My-Posh"
Join-Path $PSScriptRoot "omp" "OhMyPosh.psm1" | Import-Module
Install-OhMyPosh

Write-Success "Powershell ready"

Write-Title "INSTALL FSHARP TOOLS"

Write-Section "Installing dotnet sdk 9"
Join-Path $PSScriptRoot "Dotnet" "Dotnet9.psm1" | Import-Module
Install-Dotnet9

Write-Section "Installing fsharp tools"

Join-Path $PSScriptRoot "fsharp" "src" | Push-Location
dotnet run Install
Pop-Location

Write-Congratulations "Yeah ! Everything is installed"

Wait-BreakPoint