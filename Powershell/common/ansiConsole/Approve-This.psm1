@(
    "../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Approve-This {
    param(
        [Parameter(Mandatory = $true)]
        [string] $Message
    )

    return [Spectre.Console.AnsiConsole]::Confirm($Message)
}

Export-ModuleMember -Function Approve-This