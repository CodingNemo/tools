
@(
    "../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Write-Markup {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$Markup
    )

    [Spectre.Console.AnsiConsole]::Markup($Markup)
}

Export-ModuleMember -Function Write-Markup