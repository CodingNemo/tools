
@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Use-Status {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $StatusText,
        [Parameter(Mandatory = $true, Position = 1)]
        [scriptblock] $Action
    )

    $Status = [Spectre.Console.AnsiConsole]::Status()

    $Status.Start($StatusText, {
            param($ctx)
            # Invoke the action with one parameter
            & $Action $ctx
        })
}

Export-ModuleMember -Function Use-Status