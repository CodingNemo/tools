
@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Update-Status {
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        $Context
    )

    if (-not $Context) {
        throw "Context is required"
    }

    $Context.Refresh()
}

Export-ModuleMember -Function Update-Status