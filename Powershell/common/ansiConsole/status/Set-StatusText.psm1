
@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Set-StatusText {
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        $Context,
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $StatusText
    )

    if (-not $Context) {
        throw "Context is required"
    }

    $Context.Status = $StatusText
}

Export-ModuleMember -Function Set-StatusText