@(
    "../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole
Use-SpectreConsoleJson

function Write-JsonText {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$JsonContent
    )

    $Json = [Spectre.Console.Json.JsonText]::new($JsonContent);
    $Panel = [Spectre.Console.Panel]::new($Json)

    [Spectre.Console.AnsiConsole]::Write($Panel) | Out-Null
}

Export-ModuleMember -Function Write-JsonText