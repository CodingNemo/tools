@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function New-Table {
    $Table = [Spectre.Console.Table]::new()
    return $Table
}

Export-ModuleMember -Function New-Table