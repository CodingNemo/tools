@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Add-Cell {
    param(
        [Parameter(Mandatory = $true)] 
        $Row,
        [Parameter(Mandatory = $true, Position = 0)]
        $Cell
    )

    if ($Cell -is [Spectre.Console.Rendering.IRenderable]) {
        $Row.Add($Cell) | Out-Null
    }
    else {
        if ($Cell -is [string]) {
            $Row.Add([Spectre.Console.Markup]::new($Cell)) | Out-Null
        }
        else {
            throw "Cell must be a string or implement IRenderable"
        }
    }
}

Export-ModuleMember -Function Add-Cell