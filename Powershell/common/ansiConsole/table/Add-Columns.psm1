@(
    "./Add-Column.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

function Add-Columns {
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline)] 
        $Table,
        [Parameter(Mandatory = $true, Position = 0)]
        [string[]] $Headers

    )
    foreach ($Header in $Headers) {
        $Table | Add-Column $Header
    }
}

Export-ModuleMember -Function Add-Columns