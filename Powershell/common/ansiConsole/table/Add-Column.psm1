@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Add-Column {
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline)] 
        $Table,
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Header
    )
    $Table.AddColumn($Header) | Out-Null
}

Export-ModuleMember -Function Add-Column