@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Write-Table {
    param(
        [Parameter(Mandatory = $true, Position = 0)] 
        $Table
    )

    [Spectre.Console.AnsiConsole]::Write($Table) | Out-Null
}

Export-ModuleMember -Function Write-Table