@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Add-Row {
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline)] 
        $Table,
        [Parameter(Mandatory = $true, Position = 0)] 
        [scriptblock] $BuildRow
    )
    $NewRow = New-Object 'System.Collections.Generic.List[Spectre.Console.Rendering.IRenderable]'

    & $BuildRow $NewRow

    $Table.Rows.Add($NewRow) | Out-Null
}

Export-ModuleMember -Function Add-Row