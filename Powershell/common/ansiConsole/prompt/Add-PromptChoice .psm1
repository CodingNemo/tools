
@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Add-PromptChoice {
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        $Prompt,
        [Parameter(Mandatory = $true, Position = 0)]
        $Item
    )

    $Prompt.AddChoice($Item) | Out-Null
}


Export-ModuleMember -Function Add-PromptChoice