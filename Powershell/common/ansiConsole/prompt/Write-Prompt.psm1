
@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function Write-Prompt {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        $Prompt    
    )

    return [Spectre.Console.AnsiConsole]::Prompt($Prompt)
}


Export-ModuleMember -Function Write-Prompt