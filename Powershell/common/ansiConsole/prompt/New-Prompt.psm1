
@(
    "../../Use-SpectreConsole.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

Use-SpectreConsole

function New-Prompt {
    param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$PromptText
    )

    $Prompt = [Spectre.Console.SelectionPrompt[object]]::new()
    $Prompt.Title = $PromptText

    return $Prompt
}


Export-ModuleMember -Function New-Prompt