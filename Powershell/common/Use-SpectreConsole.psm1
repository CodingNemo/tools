@(
    "Install-NugetCLI.psm1"
    "Lock.psm1"
) 
| ForEach-Object { $PSScriptRoot | Join-Path -ChildPath $_ }
| ForEach-Object { $_ | Import-Module }

function Install-Nuget {
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [string]$Name,
        [Parameter(Mandatory = $true, Position = 1)]
        [string]$Version,
        [Parameter(Mandatory = $false)]
        [string]$Target = "netstandard2.0"
    )
        
    $PackagesDir = Join-Path -Path $HOME -ChildPath ".ps1-tools/packages"
    
    $NugetPackageDir = Join-Path -Path $PackagesDir -ChildPath "$Name.$Version"

    $NugetPackageTargetRuntimeDir = Join-Path -Path $NugetPackageDir -ChildPath "lib\$Target"
    $NugetPackageAssemblyPath = Join-Path -Path $NugetPackageTargetRuntimeDir -ChildPath "$Name.dll"

    if (-Not (Test-Path -Path $NugetPackageAssemblyPath)) {
        Invoke-WithLock {
            if (Test-Path -Path $NugetPackageAssemblyPath) {
                return
            }
            
            # Create the packages directory if it doesn't exist
            if (-Not (Test-Path -Path $PackagesDir)) {
                New-Item -Type Directory -Path $PackagesDir
            }
    
            if (-not(Install-NuGetCLI)) {
                Write-Error "Nuget cli is missing" -ErrorAction Stop
            }
            
            # Use the NuGet CLI to install Spectre.Console
            nuget install $Name -Version $Version -OutputDirectory $PackagesDir -Source https://api.nuget.org/v3/index.json
        } -LockName "Install-Nuget $Name $Version $Target"
    } 

    if (-not([System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object { $_.Location -eq $NugetPackageAssemblyPath })) {
        Add-Type -Path $NugetPackageAssemblyPath
    }
}
function Use-SpectreConsoleJson {  
    Install-Nuget "Spectre.Console.Json" "0.48.0"
}

Export-ModuleMember -Function Use-SpectreConsoleJson

function Use-SpectreConsole {
    Install-Nuget "Spectre.Console" "0.48.0"
}

Export-ModuleMember -Function Use-SpectreConsole