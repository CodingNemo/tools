#############################
#      Utility Functions    #
#############################

function Test-IsDebug {
    return $PSBoundParameters.ContainsKey("Debug") -or $DebugPreference -eq 'Continue'
}

function Test-SupportUnicode {
    return [Console]::OutputEncoding.CodePage -eq 65001
}

function Wait-BreakPoint {

    $pauseEmoji = if (Test-SupportUnicode) { "⏸️" } else { "||" }

    Write-Host ""
    Write-Host "$pauseEmoji  Press any key to proceed ...  $pauseEmoji" -ForegroundColor DarkGray
    Write-Host ""

    [System.Console]::ReadKey() | Out-Null
}

#############################
#       Write- Functions    #
#############################


function Write-Warning {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    if (-not($text.EndsWith("."))) {
        $text += "."
    }

    $warningEmoji = if (Test-SupportUnicode) { "⚠️" } else { "/!\" }

    Write-Host " $warningEmoji $text" -ForegroundColor DarkYellow
}

Export-ModuleMember -Function Write-Warning

function Write-Success {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    if (-not($text.EndsWith("."))) {
        $text += "."
    }

    $checkEmoji = if (Test-SupportUnicode) { "✅" } else { "[v]" }

    Write-Host " $checkEmoji $text" -ForegroundColor Green
}

Export-ModuleMember -Function Write-Success

function Write-Issue {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    $crossEmoji = if (Test-SupportUnicode) { "❌" } else { "[x]" }

    Write-Host " $crossEmoji $text" -ForegroundColor Red
}

Export-ModuleMember -Function Write-Issue

function Write-Pending {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    if (-not($text.EndsWith("..."))) {
        $text += "..."
    }

    $pendingEmoji = if (Test-SupportUnicode) { "⏳" } else { "(↺)" }

    Write-Host " $pendingEmoji $text" -ForegroundColor Cyan
}

Export-ModuleMember -Function Write-Pending

function Write-Step {
    param(
        [Parameter(Mandatory, Position = 0)]
        [int]$step,
        [Parameter(Mandatory, Position = 1)]
        [string]$text
    )

    if (-not($text.EndsWith("."))) {
        $text += "."
    }

    $rocketEmoji = if (Test-SupportUnicode) { "🚀" } else { "[>]" }

    Write-Host ""
    Write-Host "$rocketEmoji STEP ($step): $text" -ForegroundColor Blue
    Write-Host ""
}

Export-ModuleMember -Function Write-Step

function Write-Info {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    $infoEmoji = if (Test-SupportUnicode) { "ℹ️" } else { "(i)" }

    Write-Host " $infoEmoji $text"
}

Export-ModuleMember -Function Write-Info

function Write-DebugHighlight {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$message,
        [Parameter(Mandatory, Position = 1)]
        [string]$highlighted
    )

    if (-not(Test-IsDebug)) {
        return
    }

    $index = $message.IndexOf($highlighted)

    $left = if (0 -eq $index) {
        ""
    }
    else {
        $message.Substring(0, $index)
    }

    $right = if ($index -gt $message.Length) {
        ""
    }
    else {
        $message.Substring($index + $highlighted.Length)
    }

    Write-Host "DEBUG: " -ForegroundColor Yellow -NoNewLine
    Write-Host $left -ForegroundColor Yellow -NoNewLine
    Write-Host $highlighted -ForegroundColor Yellow -BackgroundColor Red -NoNewLine
    Write-Host $right -ForegroundColor Yellow
}

Export-ModuleMember -Function Write-DebugHighlight

function Write-Title {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$title
    )

    $formattedTitle = if ($title.Length -gt 43) { $title.Substring(0, 40) + "..." } else { $title }

    $supportsUnicode = Test-SupportUnicode

    $topLeft = if ($supportsUnicode) { "╔" } else { "#" }
    $topRight = if ($supportsUnicode) { "╗" } else { "#" }
    $bottomLeft = if ($supportsUnicode) { "╚" } else { "#" }
    $bottomRight = if ($supportsUnicode) { "╝" } else { "#" }
    $horizontal = if ($supportsUnicode) { "═" * 47 } else { "#" * 47 }
    $vertical = if ($supportsUnicode) { "║" } else { "#" }

    Write-Host "$topleft$horizontal$topRight" -ForegroundColor DarkBlue
    Write-Host "$vertical $($formattedTitle.PadRight(45)) $vertical" -ForegroundColor DarkBlue
    Write-Host "$bottomleft$horizontal$bottomRight" -ForegroundColor DarkBlue
    Write-Host ""

    Wait-BreakPoint
}

Export-ModuleMember -Function Write-Title

function Write-Section {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    $supportsUnicode = Test-SupportUnicode

    $topLeft = if ($supportsUnicode) { "┏" } else { "-" }
    $topRight = if ($supportsUnicode) { "┓" } else { "-" }
    $bottomLeft = if ($supportsUnicode) { "┗" } else { "-" }
    $bottomRight = if ($supportsUnicode) { "┛" } else { "-" }
    $horizontal = if ($supportsUnicode) { "━" * ($text.Length + 2) } else { "-" * ($text.Length + 2) } 
    $vertical = if ($supportsUnicode) { "┃" } else { "|" }


    Write-Host "$topLeft$horizontal$topRight" -ForegroundColor DarkCyan
    Write-Host "$vertical $($text.ToUpperInvariant()) $vertical" -ForegroundColor DarkCyan
    Write-Host "$bottomLeft$horizontal$bottomRight" -ForegroundColor DarkCyan
    Write-Host ""
}

Export-ModuleMember -Function Write-Section

function Write-Congratulations {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    $supportsUnicode = Test-SupportUnicode

    $emojiCup = if ($supportsUnicode) { "🏆" } else { "¥" }
    $emojiSuccess = if ($supportsUnicode) { "🎉" } else { "<;" }
    $emojiNote = if ($supportsUnicode) { "🎵" } else { "oo" }
    $emojiParty = if ($supportsUnicode) { "🥳" } else { "٩(˘◡˘)۶" }

    $congratulationsText = "$emojiSuccess $emojiNote  $emojiParty  CONGRATULATIONS  $emojiParty  $emojiNote $emojiSuccess"

    if ($text.Length -lt $congratulationsText.Length) {
        $padding = [Math]::Floor(($congratulationsText.Length - $text.Length) / 2)
        Write-Debug "PADDING: $padding"
        $text = "$(' ' * $padding)$text"
        Write-Debug "LEFT PADDED TEXT: '$text'"
        $text = "$text$(' ' * $padding)"
        Write-Debug "RIGHT PADDED TEXT: '$text'"
    }
    elseif ($text.Length -gt $congratulationsText.Length) {
        $padding = [Math]::Floor(($text.Length - $congratulationsText.Length) / 2)
        Write-Debug "PADDING: $padding"
        $congratulationsText = "$(' ' * $padding)$congratulationsText"
        Write-Debug "LEFT PADDED TEXT: '$congratulationsText'"
        $congratulationsText = "$congratulationsText$(' ' * $padding)"
        Write-Debug "RIGHT PADDED TEXT: '$congratulationsText'"
    }

    Write-Host "$emojiCup $text $emojiCup" -ForegroundColor Yellow -BackgroundColor DarkGreen
    Write-Host "$emojiCup $congratulationsText $emojiCup" -ForegroundColor Yellow -BackgroundColor DarkGreen
    Write-Host ""

    Wait-BreakPoint
}

Export-ModuleMember -Function Write-Congratulations

#############################
#     User Input Helpers    #
#############################

function Read-Confirmation {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$text
    )

    $supportsUnicode = Test-SupportUnicode

    $questionMarkEmoji = if ($supportsUnicode) { "❓" } else { "(?)" }

    while ($true) {
        Write-Host ""
        Write-Host " $questionMarkEmoji $text ([y]es,[n]o)" -ForegroundColor DarkGray
        $response = [System.Console]::ReadKey().KeyChar.ToString().ToLower()

        Write-Host ""

        if ($response -eq 'y') {
            return $true
        }
        elseif ($response -eq 'n') {
            return $false
        }

        Write-Warning "Sorry, I didn’t understand. Please respond with y, yes, n or no."
    }
}

Export-ModuleMember -Function Read-Confirmation

function Read-UserInput {
    param(
        [Parameter(Mandatory, Position = 0)]
        [string]$question
    )

    Write-Host ""
    if ($question.EndsWith(":")) {
        $question = $question.TrimEnd(":")
    }

    $promptEmoji = if ($supportsUnicode) { "⌨️" } else { ">=>" }

    $prompt = " $promptEmoji $question"

    $response = Read-Host $prompt
    Write-Host ""

    return $response
}

Export-ModuleMember -Function Read-UserInput
