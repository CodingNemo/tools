function Test-Hash(
    [Parameter( Mandatory )]
    [string] $ExpectedHash,
    [Parameter( ValueFromPipeline, Mandatory )]
    [string] $File ) {

    function Write-DiffLog([string] $Actual, [string] $Expected) {
        Write-Host "Expected : " -NoNewline -ForegroundColor Red
        for ($Index = 0; $Index -lt $Expected.Length; $Index++) {
            $ActualChar = $Actual[$Index]
            $ExpectedChar = $Expected[$Index]

            if ($ActualChar -ne $ExpectedChar) {
                Write-Host $ExpectedChar -ForegroundColor Green -NoNewline
            }
            else {
                Write-Host $ExpectedChar -ForegroundColor DarkGray -NoNewline
            }
        }

        Write-Host ""

        Write-Host "Actual   : " -NoNewline  -ForegroundColor Red
        for ($Index = 0; $Index -lt $Expected.Length; $Index++) {
            $ActualChar = $Actual[$Index]
            $ExpectedChar = $Expected[$Index]

            if ($ActualChar -ne $ExpectedChar) {
                Write-Host $ActualChar -ForegroundColor Red -NoNewline
            }
            else {
                Write-Host $ActualChar -ForegroundColor DarkGray -NoNewline
            }
        }

        if ($Actual.Length -gt $Expected.Length) {
            $Overflow = $Actual.Substring($Expected.Length)
            Write-Host $Overflow -ForegroundColor Red
        }
    }

    if ((Test-Path $File) -eq $False) {
        Write-Error "$File does not exist"
        return
    }

    $HashLog = certutil -hashfile $File SHA256

    $SplittedLog = $HashLog -split [System.Environment]::NewLine

    if ($SplittedLog.Length -lt 2) {
        Write-Error "Invalid Log"
        Write-Error $HashLog
        return
    }

    $Hash = $SplittedLog[1]

    if ($Hash -ne $ExpectedHash) {
        Write-Error "Hash does not match "
        Write-DiffLog -Actual $Hash -Expected $ExpectedHash
    }
    else {
        Write-Host "Hash matches !! " -ForegroundColor Green
    }
}


Export-ModuleMember -Function Test-Hash