# File: YamlModule.psm1

function Flatten-Object {
    param(
        [Parameter(Mandatory = $true)]
        $Object
    )
    if ($Object -is [System.Collections.IDictionary]) {
        $result = @{}
        foreach ($key in $Object.Keys) {
            $result[$key] = Flatten-Object $Object[$key]
        }
        return $result
    }
    elseif ($Object -is [System.Collections.IEnumerable] -and -not ($Object -is [string])) {
        $result = @()
        foreach ($item in $Object) {
            $result += Flatten-Object $item
        }
        return $result
    }
    elseif ($Object -is [psobject]) {
        $result = @{}
        foreach ($prop in $Object.PSObject.Properties) {
            # Skip metadata properties that can cause recursion issues.
            if ($prop.Name -in @("PSStandardMembers", "PSComputerName", "RunspaceId", "PSShowComputerName")) {
                continue
            }
            $result[$prop.Name] = Flatten-Object $prop.Value
        }
        return $result
    }
    else {
        return $Object
    }
}

function ConvertTo-Yaml {
    param(
        [Parameter(Mandatory, ValueFromPipeline)]
        $Input
    )
    process {
        # Flatten the object to remove extra PowerShell metadata.
        $flatInput = Flatten-Object $Input
        $serializerBuilder = [YamlDotNet.Serialization.SerializerBuilder]::new().DisableAliases()
        $serializer = $serializerBuilder.Build()
        Write-Output $serializer.Serialize($flatInput)
    }
}

# Example usage:
# @{ property = "plop" } | ConvertTo-Yaml

function ConvertFrom-Yaml {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string]$Yaml
    )
    begin {
        $yamlLines = @()
        Ensure-YamlDotNet
    }
    process {
        $yamlLines += $_
    }
    end {
        $yamlContent = $yamlLines -join "`n"
        $deserializer = New-Object YamlDotNet.Serialization.Deserializer
        $reader = [System.IO.StringReader]::new($yamlContent)
        $result = $deserializer.Deserialize($reader)
        Write-Output $result
    }
}

Export-ModuleMember -Function ConvertTo-Yaml, ConvertFrom-Yaml
